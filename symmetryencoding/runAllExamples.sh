#!/bin/bash
java -cp symmetryencoding.jar symmetryencoding.Main -o -f paper_examples/CoffeeCan.txt
java -cp symmetryencoding.jar symmetryencoding.Main -o -f paper_examples/DiningPhilosopher.txt
java -cp symmetryencoding.jar symmetryencoding.Main -o -f paper_examples/Herman.txt
java -cp symmetryencoding.jar symmetryencoding.Main -o -f paper_examples/Israeli.txt
java -cp symmetryencoding.jar symmetryencoding.Main -o -f paper_examples/ResourceAllocator.txt
java -cp symmetryencoding.jar symmetryencoding.Main -o -f paper_examples/LeaderElection.txt
java -cp symmetryencoding.jar symmetryencoding.Main -o -f paper_examples/ReaderWriter.txt
