# README #

Project symmetryencoding is the implementation of the paper "Parameterised Symmetries as Transducers" to generate parameterised data/process symmetries.

Note that in this guide, the current folder is "symmetryencoding".

#build project
mvn package assembly:single

#run the tool
java -cp symmetryencoding.jar symmetryencoding.Main -o -f fileName

- option -o, optional, is to generate the output in dot file. The dot file is saved with the same name and in the same folder as fileName.

- option -f, mandatory, is the input file name.

Please refer to grammar.cf for the input file format and modelinputsample.txt for a sample input file. All the input files used in our paper can be found at the folder "paper_examples".