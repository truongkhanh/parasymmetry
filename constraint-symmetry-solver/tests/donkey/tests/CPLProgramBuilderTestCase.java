package donkey.tests;

import static org.junit.Assert.fail;


import org.junit.Test;

import css.symmetrygraph.color.DefaultColorPartitioningStrategy;


public class CPLProgramBuilderTestCase {

  private static final DefaultColorPartitioningStrategy COLORING_STRATEGY = new DefaultColorPartitioningStrategy();

  @Test
  public void testBuild() {
    try {
      test("test-cpl/p1.txt");
      test("test-cpl/p2.txt");

    } catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void testP3() {
    try {
      test("test-cpl/p3.txt");
    } catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void testP4() {
    try {
      test("test-cpl/p4.txt");
      // System.out.println(new SymmetryConstraintGraph(CPLProgramBuilder
      // .build(new File("test-cpl/p-exc.txt"))));
      test("test-cpl/p-exc2.txt");
      test("test-cpl/p-var.txt");
      test("test-cpl/p-imply.txt");
    } catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  
  @Test 
  public void testP5() {
	  try {
	      test("test-cpl/p5.txt");
	  }catch(Exception e) {
		  e.printStackTrace();
	      fail();
	  }
	  
  }
  
  @Test
  public void testForall() {
    test("test-cpl/p-forall.txt");
  }

  private void test(String file) {
    try {
//      System.out.println(new SymmetryConstraintGraph(CPLProgramBuilder
//          .build(new File(file)), COLORING_STRATEGY));

    } catch (Exception e) {
      e.printStackTrace();
      fail();
    }
  }

  @Test
  public void testArray() {
    // XXX shaojie, look at here. 
    test("test-cpl/p-array.txt");
  }
}
