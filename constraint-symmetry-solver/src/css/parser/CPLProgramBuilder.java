package css.parser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.RecognitionException;

import output.CPLLexer;
import output.CPLParser;
import ast.ASTArrayDefinition;
import ast.ASTArrayIndexingExpression;
import ast.ASTBinaryExpression;
import ast.ASTBooleanExpression;
import ast.ASTConstraint;
import ast.ASTDomDefinition;
import ast.ASTExistentialExpression;
import ast.ASTForallExpression;
import ast.ASTNotOperator;
import ast.ASTNumberExpression;
import ast.ASTProgram;
import ast.ASTScalarVarDefinition;
import ast.ASTVariableExpression;
import ast.AbstractASTExpression;
import ast.AbstractASTQuantifierExpression;
import ast.AbstractASTVarDefinition;
import ast.AbstractTypedWithDomDefinition;
import css.constraint.Constraint;
import css.core.CPLProgram;
import css.exceptions.ParsingFailureException;
import css.exceptions.TypeCheckingFailureException;
import css.expr.AbstractQuantifierExpression;
import css.expr.ArrayIndexingExpression;
import css.expr.BinaryExpression;
import css.expr.ConstantExpression;
import css.expr.ExistExpression;
import css.expr.ForallExpression;
import css.expr.IExpression;
import css.expr.LocalVariableExpression;
import css.expr.NegationOperator;
import css.expr.VariableExpression;
import css.expr.operators.IBinaryOperator;
import css.expr.operators.OperatorFactory;
import css.value.BoolValue;
import css.value.IntValue;
import css.variable.BooleanVariable;
import css.variable.IArrayVariable;
import css.variable.IScalarLocalVariable;
import css.variable.IScalarVariable;
import css.variable.IntegerVariable;
import css.variable.OneDArrayVariable;
import css.variable.ThreeDArrayVariable;
import css.variable.TwoDArrayVariable;

public class CPLProgramBuilder {

	/**
	 * including scalar variables and all elements in arrays.
	 */
	private final Map<String, IScalarVariable> scalarVariableMap;

	/**
	 * including array variables.
	 */
	private final Map<String, IArrayVariable> arrayVariableMap;

	private final Map<String, IScalarVariable> domainMap;

	private final List<Constraint> constraints;

	// private final String programInString;
	private final File file = null;

	// file content
	private final String content;

	// private CPLProgramBuilder(File file) throws FileNotFoundException,
	// IOException, RecognitionException, TypeCheckingFailureException {
	// scalarVariableMap = new HashMap<String, IScalarVariable>();
	// this.domainMap = new HashMap<String, IScalarVariable>();
	// this.constraints = new ArrayList<Constraint>();
	// this.arrayVariableMap = new HashMap<String, IArrayVariable>();
	// // this.programInString = programInString;
	// this.file = file;
	// this.parse();
	// }
	private CPLProgramBuilder(String content) throws FileNotFoundException,
			IOException, RecognitionException, TypeCheckingFailureException {
		scalarVariableMap = new HashMap<String, IScalarVariable>();
		this.domainMap = new HashMap<String, IScalarVariable>();
		this.constraints = new ArrayList<Constraint>();
		this.arrayVariableMap = new HashMap<String, IArrayVariable>();
		// this.programInString = programInString;
		this.content = content;
		this.parse();
	}

	// public static CPLProgram build(File file) throws FileNotFoundException,
	// IOException, RecognitionException, TypeCheckingFailureException {
	// CPLProgramBuilder builder = new CPLProgramBuilder(file);
	// return new CPLProgram(new ArrayList<IScalarVariable>(
	// builder.scalarVariableMap.values()), builder.constraints);
	// }
	public static CPLProgram build(String content)
			throws FileNotFoundException, IOException, RecognitionException,
			TypeCheckingFailureException {
		CPLProgramBuilder builder = new CPLProgramBuilder(content);
		return new CPLProgram(new ArrayList<IScalarVariable>(
				builder.scalarVariableMap.values()), builder.constraints);
	}

	private void parse() throws FileNotFoundException, IOException,
			RecognitionException, TypeCheckingFailureException {

		InputStream is = new ByteArrayInputStream(this.content.getBytes());

		ANTLRInputStream stream = new ANTLRInputStream(is);
		// ANTLRInputStream is = new ANTLRInputStream(new FileInputStream(
		// this.file));
		CPLLexer lexer = new CPLLexer(stream);
		CommonTokenStream ts = new CommonTokenStream(lexer);
		CPLParser parser = new CPLParser(ts);
		ASTProgram program = parser.program().value;

		buildDomainMap(program);

		buildVariableMap(program);

		buildConstraints(program);
	}

	/*
	 * //Recursive version private void TreeSearching() { List<IScalarVariable>
	 * variables = new ArrayList<IScalarVariable>();
	 * variables.addAll(scalarVariableMap.values()); Map<IScalarVariable,
	 * IValue> solution = new HashMap<IScalarVariable, IValue>();
	 * Search_BT(variables, 1, solution) ;
	 * 
	 * }
	 * 
	 * private void Search_BT(List<IScalarVariable> variables, int level,
	 * Map<IScalarVariable, IValue> solution) { for(IScalarVariable var:
	 * variables) { for (int i = 0; i < var.sizeOfDomain(); ++i) { IValue value
	 * = var.getValue(i); if (Consistent(var, value, solution)) {
	 * solution.put(var, value); if (variables.size() == 1) {
	 * 
	 * }
	 * 
	 * } }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * private boolean Consistent(IScalarVariable var, IValue value,
	 * Map<IScalarVariable, IValue> solution) {
	 * 
	 * boolean isConsistent = true;
	 * 
	 * for(Constraint cons: constraints) { List<IScalarVariable>
	 * currentVariables = new ArrayList<IScalarVariable>();
	 * currentVariables.addAll(solution.keySet()); currentVariables.add(var);
	 * 
	 * if (cons.getVariables().contains(var) &&
	 * currentVariables.containsAll(cons.getVariables())) { Map<IScalarVariable,
	 * IValue> currentSolution = new HashMap<IScalarVariable, IValue>();
	 * currentSolution.putAll(solution); currentSolution.put(var, value); if
	 * (!cons.testSatisfaction(new Assignment(currentSolution))) { isConsistent
	 * = false; break; }
	 * 
	 * }
	 * 
	 * }
	 * 
	 * return isConsistent;
	 * 
	 * }
	 */

	private void buildConstraints(ASTProgram program)
			throws TypeCheckingFailureException {
		for (ASTConstraint astConstraint : program.getConstraints()) {
			Constraint cons = new Constraint(astConstraint.getAnnotations(),
					convert(astConstraint.getExpression(),
							Collections
									.<String, IScalarLocalVariable> emptyMap()));
			// cons.typeCheck();
			this.constraints.add(cons);
		}
	}

	private void buildVariableMap(ASTProgram program) {
		for (AbstractASTVarDefinition def : program.getVarDefinitions()) {
			// variable is a plain variable.
			if (def instanceof ASTScalarVarDefinition) {
				buildScalarVarDefinition((ASTScalarVarDefinition) def);
			} else if (def instanceof ASTArrayDefinition) {
				// variable is an array.
				ASTArrayDefinition array = (ASTArrayDefinition) def;
				IArrayVariable arrayVar = null;
				switch (array.numberOfDimensions()) {
				case 1: {
					arrayVar = buildOneDArrayVariable(array);
					break;
				}
				case 2: {
					arrayVar = buildTwoDArrayVariable(array);
					break;
				}
				case 3: {
					arrayVar = buildThreeDArrayVariable(array);
					break;
				}
				default:
					throw new ParsingFailureException(
							"So far we only accept 2-d array at most.");
				}
				if (this.arrayVariableMap.containsKey(arrayVar.getName())) {
					throw new ParsingFailureException("Array variable "
							+ arrayVar.getName() + " has been defined. ");
				} else {
					this.arrayVariableMap.put(arrayVar.getName(), arrayVar);
					// add all variables in the scalar variable map.
					for (IScalarVariable var : arrayVar.getAllElements()) {
						assert !this.scalarVariableMap.containsKey(var
								.getName());
						this.scalarVariableMap.put(var.getName(), var);
					}
				}
			} else {
				throw new RuntimeException(
						"Cannot reach here. The class of def is "
								+ def.getClass());
			}

		}
	}

	private IArrayVariable buildThreeDArrayVariable(ASTArrayDefinition array) {
		assert (array.numberOfDimensions() == 3);
		String name = array.getName();
		String type = array.getType();
		if (type.equals("int")) {
			return ThreeDArrayVariable.createIntegerArray(name,
					array.getDimension(0), array.getDimension(1),
					array.getDimension(2), parseIntegerDomain(array),
					array.getAnnotations());
		} else if (type.equals("bool")) {
			return ThreeDArrayVariable.createBooleanArray(name,
					array.getDimension(0), array.getDimension(1),
					array.getDimension(3), parseBooleanDomain(array),
					array.getAnnotations());
		} else {
			throw new ParsingFailureException("Unhandled type: " + type
					+ ". Only int or bool is accepted.");
		}
	}

	private IArrayVariable buildTwoDArrayVariable(ASTArrayDefinition array) {
		assert (array.numberOfDimensions() == 2);
		String name = array.getName();
		String type = array.getType();
		if (type.equals("int")) {
			return TwoDArrayVariable.createIntegerArray(name,
					array.getDimension(0), array.getDimension(1),
					parseIntegerDomain(array), array.getAnnotations());
		} else if (type.equals("bool")) {
			return TwoDArrayVariable.createBooleanArray(name,
					array.getDimension(0), array.getDimension(1),
					parseBooleanDomain(array), array.getAnnotations());
		} else {
			throw new ParsingFailureException("Unhandled type: " + type
					+ ". Only int or bool is accepted.");
		}
	}

	private OneDArrayVariable buildOneDArrayVariable(ASTArrayDefinition array) {
		assert (array.numberOfDimensions() == 1);
		String name = array.getName();
		String type = array.getType();
		if (type.equals("int")) {
			return OneDArrayVariable.createIntegerArray(name,
					array.getDimension(0), parseIntegerDomain(array),
					array.getAnnotations());
		} else if (type.equals("bool")) {
			return OneDArrayVariable.createBooleanArray(name,
					array.getDimension(0), parseBooleanDomain(array),
					array.getAnnotations());
		} else {
			throw new ParsingFailureException("Unhandled type: " + type
					+ ". Only int or bool is accepted.");
		}
	}

	/**
	 * parse text to variable
	 * 
	 * @param def
	 * @return
	 */
	private IScalarVariable convertToVariable(AbstractASTVarDefinition def) {
		String name = def.getName();
		String type = def.getType();
		if (type.equals("int")) {
			// FIXME: maybe use existing domain value by name, no need to parse
			// again?!
			return new IntegerVariable(name, parseIntegerDomain(def),
					def.getAnnotations());
		} else if (type.equals("bool")) {
			return new BooleanVariable(name, parseBooleanDomain(def),
					def.getAnnotations());
		} else {
			throw new ParsingFailureException("Unhandled type: " + type
					+ ". Only int or bool is accepted.");
		}
	}

	/**
	 * parse text to boolean value.
	 * 
	 * @param def
	 * @return
	 */
	private Set<Boolean> parseBooleanDomain(AbstractTypedWithDomDefinition def) {
		assert def.getType().equals("bool");
		Set<Boolean> domain = new HashSet<Boolean>();
		for (String e : def.getDomain()) {
			domain.add(Boolean.parseBoolean(e));
		}
		return domain;
	}

	/**
	 * parse text to int value in domain.
	 * 
	 * @param def
	 * @return
	 */
	private Set<Integer> parseIntegerDomain(AbstractTypedWithDomDefinition def) {
		assert def.getType().equals("int");
		Set<Integer> domain = new HashSet<Integer>();
		for (String e : def.getDomain()) {
			domain.add(Integer.parseInt(e));
		}
		return domain;
	}

	private void buildScalarVarDefinition(ASTScalarVarDefinition scalarVar) {
		IScalarVariable var = convertToVariable(scalarVar);
		if (scalarVariableMap.containsKey(var.getName())) {
			throw new ParsingFailureException("Variable " + var.getName()
					+ " has been defined.");
		} else {
			scalarVariableMap.put(var.getName(), var);
		}
	}

	private void buildDomainMap(ASTProgram program) {
		for (ASTDomDefinition domain : program.getDomDefintions()) {
			IScalarVariable dom = convertDomain(domain);
			if (this.domainMap.containsKey(dom.getName())) {
				throw new ParsingFailureException("Domain " + dom.getName()
						+ " has been defined.");
			} else {
				this.domainMap.put(dom.getName(), dom);
			}
		}
	}

	/**
	 * convert domain values from text content.
	 * 
	 * @param def
	 * @return
	 */
	private IScalarVariable convertDomain(ASTDomDefinition def) {
		String name = def.getName();
		String type = def.getType();
		if (type.equals("int")) {
			return new IntegerVariable(name, parseIntegerDomain(def));
		} else if (type.equals("bool")) {
			return new BooleanVariable(name, parseBooleanDomain(def));
		} else {
			throw new ParsingFailureException("Unhandled type: " + type
					+ ". Only int or bool is accepted.");
		}
	}

	/**
	 * convert plain text to constraint expression.
	 * 
	 * @param exp
	 * @param localVariableMap
	 * @return
	 */
	private IExpression convert(AbstractASTExpression exp,
			Map<String, IScalarLocalVariable> localVariableMap) {
		if (exp.getClass().equals(ASTBinaryExpression.class)) {
			return convertBinary((ASTBinaryExpression) exp, localVariableMap);

		} else if (exp.getClass().equals(ASTBooleanExpression.class)) {
			ASTBooleanExpression bool = (ASTBooleanExpression) exp;
			return new ConstantExpression(new BoolValue(
					bool.equals(ASTBooleanExpression.TRUE)));

		} else if (exp.getClass().equals(ASTNotOperator.class)) {
			ASTNotOperator not = (ASTNotOperator) exp;
			return new NegationOperator(convert(not.getChild(),
					localVariableMap));

		} else if (exp.getClass().equals(ASTNumberExpression.class)) {
			ASTNumberExpression num = (ASTNumberExpression) exp;
			return new ConstantExpression(new IntValue(num.getValue()));

		} else if (exp.getClass().equals(ASTVariableExpression.class)) {
			ASTVariableExpression var = (ASTVariableExpression) exp;
			String varName = var.getName();

			if (localVariableMap.containsKey(varName)) {
				IScalarLocalVariable localVariable = localVariableMap
						.get(varName);
				if (localVariable == null) {
					throw new ParsingFailureException("Local variable "
							+ varName + " has not been defined.");
				}
				return new LocalVariableExpression(localVariable);
			} else {
				IScalarVariable cplVariable = this.scalarVariableMap
						.get(varName);
				if (cplVariable == null){
					throw new ParsingFailureException("Variable " + varName
							+ " has not been defined.");
				}
				return new VariableExpression(cplVariable);
			}

		} else if (exp.getClass().equals(ASTForallExpression.class)) {
			return convertQuantifierExpression(
					(AbstractASTQuantifierExpression) exp, true,
					localVariableMap);

		} else if (exp.getClass().equals(ASTExistentialExpression.class)) {
			return convertQuantifierExpression(
					(AbstractASTQuantifierExpression) exp, false,
					localVariableMap);

		} else if (exp.getClass().equals(ASTArrayIndexingExpression.class)) {
			return arrayIndexingExpression((ASTArrayIndexingExpression) exp,
					localVariableMap);
			// } else if (exp.getClass().equals(ASTWriteExpression.class)) {
			// //return arrayWriteExpression((ASTWriteExpression)exp);
			//
		}

		else
			throw new RuntimeException(
					"Cannot reach here. The class of exp is " + exp.getClass());
	}

	private IExpression arrayIndexingExpression(ASTArrayIndexingExpression exp,
			Map<String, IScalarLocalVariable> localVariableMap) {
		final int numberOfDimensions = exp.numberOfIndices();
		List<IExpression> indices = new ArrayList<IExpression>();
		for (int i = 0; i < numberOfDimensions; ++i) {
			indices.add(this.convert(exp.getIndex(i), localVariableMap));
		}
		IArrayVariable array = this.arrayVariableMap.get(exp.getArrayName());
		if (array == null)
			throw new ParsingFailureException("Array " + exp.getArrayName()
					+ " is not defined.");
		return new ArrayIndexingExpression(array, indices);
	}

	private AbstractQuantifierExpression convertQuantifierExpression(
			AbstractASTQuantifierExpression exp, boolean isForall,
			Map<String, IScalarLocalVariable> localVariableMap) {
		AbstractASTQuantifierExpression quantifier = (AbstractASTQuantifierExpression) exp;
		String localVariableName = quantifier.getLocalVariableName();
		String domainName = quantifier.getDomainName();

		IScalarVariable domain = this.domainMap.get(domainName);
		if (domain == null) {
			throw new ParsingFailureException("No domain named " + domainName
					+ " in expression " + exp.toString());
		}

		AbstractQuantifierExpression resultExpression;
		if (isForall)
			resultExpression = new ForallExpression(localVariableName, domain);
		else
			resultExpression = new ExistExpression(localVariableName, domain);

		Map<String, IScalarLocalVariable> localMap = new HashMap<String, IScalarLocalVariable>(
				localVariableMap);
		localMap.put(localVariableName, resultExpression.getLocalVariable());
		IExpression childExpression = this.convert(quantifier.getChild(),
				localMap);
		resultExpression.setEnclosedExpression(childExpression);
		return resultExpression;

	}

	/**
	 * convert to binary expression, e.g: a+b; (left expression) operator (right
	 * expression)
	 * 
	 * @param binary
	 * @param localVariableMap
	 * @return
	 */
	private IExpression convertBinary(ASTBinaryExpression binary,
			Map<String, IScalarLocalVariable> localVariableMap) {
		IExpression left = this.convert(binary.getLeft(), localVariableMap);
		IExpression right = this.convert(binary.getRight(), localVariableMap);
		String op = binary.getOperator();
		IBinaryOperator bop = null;
		if (op.equals("+")) {
			bop = OperatorFactory.ADD;
		} else if (op.equals("-")) {
			bop = OperatorFactory.MINUS;
		} else if (op.equals("*")) {
			bop = OperatorFactory.TIME;
		} else if (op.equals("/")) {
			bop = OperatorFactory.DIVIDE;
		} else if (op.equals("%")) {
			bop = OperatorFactory.MODULO;
		} else if (op.equals("==")) {
			bop = OperatorFactory.EQUAL;
		} else if (op.equals("!=")) {
			bop = OperatorFactory.NOT_EQUAL;
		} else if (op.equals("&&")) {
			bop = OperatorFactory.AND;
		} else if (op.equals("||")) {
			bop = OperatorFactory.OR;
		} else if (op.equals(">")) {
			bop = OperatorFactory.GREATER;
		} else if (op.equals("<")) {
			bop = OperatorFactory.LESS;
		} else if (op.equals(">=")) {
			bop = OperatorFactory.GREATER_EQ;
		} else if (op.equals("<=")) {
			bop = OperatorFactory.LESS_EQ;
		} else if (op.equals("^^")) {
			bop = OperatorFactory.POWER;
		} else if (op.equals("^")) {
			bop = OperatorFactory.XOR;
		} else if (op.equals("->")) {
			bop = OperatorFactory.IMPLY;
		} else {
			throw new RuntimeException("Unhandled operator " + op);
		}
		return new BinaryExpression(left, right, bop);
	}

}
