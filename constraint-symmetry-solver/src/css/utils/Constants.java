package css.utils;

public class Constants {

	public final static String GLOBAL = "global";

	public final static String SUMNUM = "sumnum";
	
	public final static String LASTVERSION = "_last";
}
