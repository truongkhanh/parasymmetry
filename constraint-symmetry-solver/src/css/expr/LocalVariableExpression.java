package css.expr;

import java.util.List;
import java.util.Map;
import java.util.Set;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.value.IValue;
import css.value.IValue.ValueType;
import css.variable.IScalarLocalVariable;
import css.variable.IScalarVariable;


public  class LocalVariableExpression implements IExpression,
    IExpressionWithDomain {

  private final IScalarLocalVariable localVariable;

  public LocalVariableExpression(IScalarLocalVariable localVariable) {
    super();
    this.localVariable = localVariable;
  }

  public IScalarLocalVariable getLocalVariable() {
    return localVariable;
  }

  @Override
  public IValue evaluate(Assignment assignment) throws EvaluationException {
    return this.localVariable.getCurrentValue();
  }

  @Override
  public ValueType getType() {
    return this.localVariable.getType();
  }

  @Override
  public void typeCheck() throws TypeCheckingFailureException {
  }

  @Override
  public void collectVariables(Map<IScalarVariable, List<IExpression>>  variableCollector) {
  }

  @Override
  public String toString() {
    return this.localVariable.getName();
  }

  @Override
  public Set<Object> getDomain() {
    return this.localVariable.getDomain();
  }

@Override
public void calculateUsefulVariables(Assignment assignment) {
	// TODO Auto-generated method stub
	
}

@Override
public boolean shaojieEquality(IExpression other) {
    if (!other.getClass().equals(this.getClass())) {
      return false;
    }
    
    LocalVariableExpression exp = (LocalVariableExpression)other;
    return this.getDomain().equals(exp.getDomain());
    
}

}
