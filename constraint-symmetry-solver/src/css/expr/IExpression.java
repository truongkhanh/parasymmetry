package css.expr;


import java.util.List;
import java.util.Map;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.value.IValue;
import css.variable.IScalarVariable;


public interface IExpression {

  IValue evaluate(Assignment assignment) throws EvaluationException;

  // IValue evaluate(Set<Assignment> allFeasibleAssignments)
  // throws EvaluationException;

  void calculateUsefulVariables(Assignment assignment)
      throws EvaluationException;

  IValue.ValueType getType();

  void typeCheck() throws TypeCheckingFailureException;

  void collectVariables(Map<IScalarVariable, List<IExpression>> variableCollector);

  @Override
  public String toString();

  boolean shaojieEquality(IExpression other);

}
