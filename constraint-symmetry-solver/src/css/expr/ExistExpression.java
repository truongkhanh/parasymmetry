package css.expr;

import css.assignment.Assignment;
import css.variable.IScalarVariable;

public class ExistExpression extends AbstractQuantifierExpression {

  public ExistExpression(String variableName, IScalarVariable domain) {
    super(variableName, domain);
  }

  @Override
  protected boolean finalReturnValue() {
    return false;
  }

  @Override
  protected boolean earlyReturnValue() {
    return true;
  }

  @Override
  protected boolean earlyReturn(boolean asBoolean) {
    return asBoolean;
  }

  @Override
  protected String getQuantifier() {
    return "exists";
  }

  @Override
  public void calculateUsefulVariables(Assignment assignment) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean shaojieEquality(IExpression other) {
    if (!this.getClass().equals(other.getClass())) {
      return false;
    }
    
    ExistExpression exp = (ExistExpression)other;
    
    return this.getLocalVariable().getDomain().equals(exp.getLocalVariable().getDomain()) 
            && this.getEclosedExpression().equals(exp.getEclosedExpression());
  }

}
