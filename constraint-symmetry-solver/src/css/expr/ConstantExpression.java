package css.expr;


import java.util.List;
import java.util.Map;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.value.IValue;
import css.value.IValue.ValueType;
import css.variable.IScalarVariable;


public class ConstantExpression implements IExpression {

  private final IValue value;

  public ConstantExpression(IValue value) {
    super();
    this.value = value;
  }

  @Override
  public boolean shaojieEquality(IExpression other) {
    if (!other.getClass().equals(this.getClass())) {
      return false;
    }
    final ConstantExpression exp = (ConstantExpression) other;
    if (!this.value.getType().equals(exp.value.getType()))
      return false;
    switch (this.value.getType()) {
    case Bool:
      return this.value.asBoolean() == exp.value.asBoolean();
    case Int:
      return this.value.asInt() == exp.value.asInt();
    default:
      throw new RuntimeException("cannot reach here." + this.value.getType());
    }
  }

  public IValue getValue() {
    return value;
  }

  @Override
  public IValue evaluate(Assignment assignment) throws EvaluationException {
    return this.value;
  }

  @Override
  public ValueType getType() {
    return this.value.getType();
  }

  @Override
  public void typeCheck() throws TypeCheckingFailureException {
  }

  @Override
  public String toString() {
    return this.value.toString();
  }

  @Override
  public void collectVariables(Map<IScalarVariable, List<IExpression>> variableCollector) {
  }

  @Override
  public void calculateUsefulVariables(Assignment assignment) {
    // TODO Auto-generated method stub

  }
}
