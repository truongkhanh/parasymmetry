package css.expr;

import java.util.List;
import java.util.Map;
import java.util.Set;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.value.BoolValue;
import css.value.IValue;
import css.value.IValue.ValueType;
import css.variable.IScalarLocalVariable;
import css.variable.IScalarVariable;


public abstract class AbstractQuantifierExpression implements IExpression {

  protected static class LocalVariable implements IScalarLocalVariable {

    private IValue currentValue;

    private final IScalarVariable domain;

    private final String name;

    public LocalVariable(String name, IScalarVariable domain) {
      this.name = name;
      this.domain = domain;
    }

    @Override
    public String getName() {
      return this.name;
    }

    public String getDomainName() {
      return this.domain.getName();
    }

    @Override
    public IValue getCurrentValue() {
      return this.currentValue;
    }

    public void setCurrentValue(IValue value) {
      this.currentValue = value;
    }

    @Override
    public ValueType getType() {
      return this.domain.getType();
    }

    @Override
    public String toString() {
      StringBuilder builder = new StringBuilder();
      builder.append(this.name).append(" in ").append(this.domain.getName());
      return builder.toString();
    }

    @Override
    public Set<Object> getDomain() {
      return this.domain.getDomain();
    }
  }

  private IExpression enclosed;

  private final LocalVariable variable;

  public AbstractQuantifierExpression(String variableName, IScalarVariable domain) {
    // this.enclosed = expression;
    this.variable = new LocalVariable(variableName, domain);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.getQuantifier()).append(' ')
        .append(this.variable.toString()).append(".").append(this.enclosed);
    return builder.toString();
  }

  protected abstract String getQuantifier();

  public void setEnclosedExpression(IExpression enclosed) {
    if (this.enclosed != null)
      throw new RuntimeException();
    this.enclosed = enclosed;
  }
  
  public IExpression getEclosedExpression() {
    return this.enclosed;
  }
  

  @Override
  public IValue evaluate(Assignment assignment) throws EvaluationException {
    final IScalarVariable domain = this.variable.domain;
    final int sizeOfDomain = domain.sizeOfDomain();
    for (int i = 0; i < sizeOfDomain; ++i) {
      IValue value = domain.getValue(i);
      this.variable.setCurrentValue(value);
      IValue result = this.enclosed.evaluate(assignment);
      if (earlyReturn(result.asBoolean())) {
        return new BoolValue(earlyReturnValue());
      }
    }
    return new BoolValue(finalReturnValue());
  }

  protected abstract boolean finalReturnValue();

  protected abstract boolean earlyReturnValue();

  protected abstract boolean earlyReturn(boolean asBoolean);

  public IScalarLocalVariable getLocalVariable() {
    return this.variable;
  }

  @Override
  public ValueType getType() {
    return ValueType.Bool;
  }

  @Override
  public void typeCheck() throws TypeCheckingFailureException {
    this.enclosed.typeCheck();

    if (this.enclosed.getType().equals(IValue.ValueType.Bool)) {
      return;
    } else
      throw new TypeCheckingFailureException(
          "The type of the enclosed expression is not BOOL. Enclosed = "
              + this.enclosed.toString());
  }

  @Override
  public void collectVariables(Map<IScalarVariable, List<IExpression>> variableCollector) {
    this.enclosed.collectVariables(variableCollector);
  }

}
