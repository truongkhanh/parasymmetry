package css.expr.operators;

import css.exceptions.EvaluationException;
import css.value.IValue;

public interface IBinaryOperator {

  IValue evalute(IValue left, IValue right) throws EvaluationException;

  String typeCheck(IValue.ValueType left, IValue.ValueType right);

  IValue.ValueType getType();

  IValue shortCircuit(IValue left);

//  IValue getShortCircuitValue(IValue left);

}
