package css.expr.operators;

import css.exceptions.EvaluationException;
import css.value.BoolValue;
import css.value.IValue;
import css.value.IntValue;
import css.value.IValue.ValueType;

public final class OperatorFactory {

  private static abstract class ArithmeticOperator implements IBinaryOperator {

    private final String name;

    public ArithmeticOperator(String name) {
      super();
      this.name = name;
    }

    @Override
    public String toString() {
      return this.name;
    }

    @Override
    public ValueType getType() {
      return ValueType.Int;
    }

    @Override
    public IValue evalute(IValue left, IValue right) throws EvaluationException {
      if (!left.getType().equals(IValue.ValueType.Int)
          || !right.getType().equals(IValue.ValueType.Int)) {
        throw new RuntimeException(
            "The types of left and right are not all Int. "
                + "The type of left is " + left.getType()
                + ", and the right type is " + right.getType());
      }
      return new IntValue(this.internalEvaluate(left.asInt(), right.asInt()));
    }

    @Override
    public String typeCheck(IValue.ValueType left, IValue.ValueType right) {
      if (!left.equals(IValue.ValueType.Int)
          || !right.equals(IValue.ValueType.Int)) {
        return "The types of left and right are not all Int. "
            + "The type of left is " + left + ", and the right type is "
            + right;
      } else
        return null;
    }

    @Override
    public final IValue shortCircuit(IValue left) {
      return null;
    }

    protected abstract int internalEvaluate(int left, int right)
        throws EvaluationException;
  }

  public static final IBinaryOperator ADD = new ArithmeticOperator("+") {
    @Override
    protected int internalEvaluate(int left, int right) {
      return left + right;
    }

  };

  public static final IBinaryOperator POWER = new ArithmeticOperator("^^") {

    @Override
    protected int internalEvaluate(int left, int right)
        throws EvaluationException {
      if (right < 0)
        throw new EvaluationException("The exponent " + right
            + " is not positive");
      int result = 1;
      for (int i = 0; i < right; ++i) {
        result *= left;
      }
      return result;
    }

  };

  public static final IBinaryOperator XOR = new ArithmeticOperator("^") {
    @Override
    protected int internalEvaluate(int left, int right)
        throws EvaluationException {
      return left ^ right;
    }

  };

  public static final IBinaryOperator MINUS = new ArithmeticOperator("-") {

    @Override
    protected int internalEvaluate(int left, int right) {
      return left - right;
    }

  };

  public static final IBinaryOperator TIME = new ArithmeticOperator("*") {

    @Override
    protected int internalEvaluate(int left, int right) {
      return left * right;
    }

  };

  public static final IBinaryOperator DIVIDE = new ArithmeticOperator("/") {

    @Override
    protected int internalEvaluate(int left, int right)
        throws EvaluationException {
      if (right == 0)
        throw new EvaluationException("zero divider");
      return left / right;
    }

  };

  public static final IBinaryOperator MODULO = new ArithmeticOperator("%") {

    @Override
    protected int internalEvaluate(int left, int right)
        throws EvaluationException {
      try {
        int result = left % right;
        if (result < 0)
          result += right;
        return result;
      } catch (Exception e) {
        throw new EvaluationException(e);
      }
    }

  };

  private static class EqualityOperator implements IBinaryOperator {

    private final boolean testingEqual;

    public EqualityOperator(boolean testingEqual) {
      super();
      this.testingEqual = testingEqual;
    }

    @Override
    public ValueType getType() {
      return ValueType.Bool;
    }

    @Override
    public String typeCheck(ValueType left, ValueType right) {
      if (left.equals(right))
        return null;
      else {
        return "The left type and the right type are not equal. " + "Left = "
            + left + ", right = " + right;
      }
    }

    @Override
    public String toString() {
      return this.testingEqual ? "==" : "!=";
    }

    @Override
    public IValue evalute(IValue left, IValue right) throws EvaluationException {
      boolean equal;
      if (left.getType().equals(IValue.ValueType.Int)
          && right.getType().equals(IValue.ValueType.Int)) {
        equal = left.asInt() == right.asInt();
      } else if (left.getType().equals(IValue.ValueType.Bool)
          && right.getType().equals(IValue.ValueType.Bool)) {
        equal = left.asBoolean() == right.asBoolean();
      } else
        throw new RuntimeException("Cannot reach here.");
      return new BoolValue(testingEqual ? equal : !equal);
    }

    @Override
    public IValue shortCircuit(IValue left) {
      return null;
    }
  }

  public static final IBinaryOperator EQUAL = new EqualityOperator(true);

  public static final IBinaryOperator NOT_EQUAL = new EqualityOperator(false);

  private static abstract class AbstractBinaryLogicOperator implements
      IBinaryOperator {

    // private final boolean and;
    //
    // public BinaryLogicOperator(boolean and) {
    // super();
    // this.and = and;
    // }

    @Override
    public ValueType getType() {
      return ValueType.Bool;
    }

    // @Override
    // public String toString() {
    // return this.and ? "&&" : "||";
    // }
    public abstract String toString();

    @Override
    public IValue evalute(IValue left, IValue right) throws EvaluationException {
      final boolean l = left.asBoolean();
      final boolean r = right.asBoolean();
      return new BoolValue(this.evalute(l, r));	
    }

    protected abstract boolean evalute(boolean l, boolean r);

    @Override
    public String typeCheck(ValueType left, ValueType right) {
      if (left.equals(right) && left.equals(IValue.ValueType.Bool))
        return null;
      else {
        return "The types of left and right are not all Bool. "
            + "The type of left is " + left + ", and the right type is "
            + right;
      }
    }

    @Override
    public final IValue shortCircuit(IValue left) {
      return this.shortCircuit(left.asBoolean());
    }

    protected abstract IValue shortCircuit(boolean left);
  }

  public static IBinaryOperator AND = new AbstractBinaryLogicOperator() {

    @Override
    public String toString() {
      return "&&";
    }

    @Override
    protected boolean evalute(boolean l, boolean r) {
      return l && r;
    }

    @Override
    public IValue shortCircuit(boolean left) {
      if (!left) {
        return new BoolValue(false);
      } else {
        return null;
      }
    }
  };

  public static IBinaryOperator OR = new AbstractBinaryLogicOperator() {

    @Override
    public String toString() {
      return "||";
    }

    @Override
    protected boolean evalute(boolean l, boolean r) {
      return l || r;
    }

    @Override
    protected IValue shortCircuit(boolean left) {
      return left ? new BoolValue(true):null;
    }
  };

  public static IBinaryOperator IMPLY = new AbstractBinaryLogicOperator() {

    @Override
    public String toString() {
      return "->";
    }

    @Override
    protected boolean evalute(boolean l, boolean r) {
      return !l || r;
    }

    @Override
    protected IValue shortCircuit(boolean left) {
      return left? null:new BoolValue(true);
    }
  };

  private static abstract class RelationOperator implements IBinaryOperator {

    private final String name;

    public RelationOperator(String name) {
      super();
      this.name = name;
    }

    @Override
    public ValueType getType() {
      return ValueType.Bool;
    }

    @Override
    public String toString() {
      return this.name;
    }

    @Override
    public String typeCheck(ValueType left, ValueType right) {
      if (left.equals(right) && left.equals(IValue.ValueType.Int))
        return null;
      else
        return "The types of left and right are not all Int. "
            + "The type of left is " + left + ", and the right type is "
            + right;
    }

    @Override
    public IValue evalute(IValue left, IValue right) throws EvaluationException {
      return new BoolValue(this.internalEvaluate(left.asInt(), right.asInt()));
    }

    
    
    
    @Override
    public IValue shortCircuit(IValue left) {
      return null;
    }

    protected abstract boolean internalEvaluate(int left, int right);

  }

  public static final IBinaryOperator LESS = new RelationOperator("<") {

    @Override
    protected boolean internalEvaluate(int left, int right) {
      return left < right;
    }

  };

  public static final IBinaryOperator LESS_EQ = new RelationOperator("<=") {

    @Override
    protected boolean internalEvaluate(int left, int right) {
      return left <= right;
    }
  };

  public static final IBinaryOperator GREATER = new RelationOperator(">") {

    @Override
    protected boolean internalEvaluate(int left, int right) {
      return left > right;
    }

  };

  public static final IBinaryOperator GREATER_EQ = new RelationOperator(">=") {

    @Override
    protected boolean internalEvaluate(int left, int right) {
      return left >= right;
    }
  };

}
