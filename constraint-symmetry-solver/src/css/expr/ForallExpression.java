package css.expr;

import css.assignment.Assignment;
import css.variable.IScalarVariable;

public class ForallExpression extends AbstractQuantifierExpression {

  public ForallExpression(String variableName, IScalarVariable domain) {
    super(variableName, domain);
  }

  @Override
  protected boolean finalReturnValue() {
    return true;
  }

  @Override
  protected boolean earlyReturnValue() {
    return false;
  }

  @Override
  protected boolean earlyReturn(boolean asBoolean) {
    return !asBoolean;
  }

  @Override
  protected String getQuantifier() {
    return "forall";
  }

  @Override
  public void calculateUsefulVariables(Assignment assignment) {
    // TODO Auto-generated method stub

  }
  
  @Override
  public boolean shaojieEquality(IExpression other) {
    if (!this.getClass().equals(other.getClass())) {
      return false;
    }
    
    ForallExpression exp = (ForallExpression)other;
    
    return this.getLocalVariable().getDomain().equals(exp.getLocalVariable().getDomain()) 
            && this.getEclosedExpression().equals(exp.getEclosedExpression());
  }

}
