package css.expr;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.value.IValue;
import css.value.IntValue;
import css.value.IValue.ValueType;
import css.variable.IArrayVariable;
import css.variable.IScalarVariable;
import css.variable.IWithNameAndDomain;
import css.variable.OneDArrayVariable;
import css.variable.ThreeDArrayVariable;
import css.variable.TwoDArrayVariable;


public class ArrayIndexingExpression implements IExpression {

  private final IArrayVariable array;

  private final IExpression[] indices;

  public ArrayIndexingExpression(IArrayVariable array, List<IExpression> indices) {
    super();
    assert indices.size() > 0;
    assert array.getDimension() == 1 && indices.size() == 1
        || array.getDimension() == 2 && indices.size() == 2;
    this.array = array;
    this.indices = indices.toArray(new IExpression[indices.size()]);
  }

  @Override
  public boolean shaojieEquality(IExpression other) {
    if (!other.getClass().equals(ArrayIndexingExpression.class)) {
      return false;
    }
    final ArrayIndexingExpression exp = (ArrayIndexingExpression) other;
    final int length = this.indices.length;
    if (length != exp.indices.length)
      return false;
    if (!this.array.shaojieEquality(exp.array)) {
      return false;
    }
    for (int i = 0; i < length; ++i) {
      if (!(this.indices[i] instanceof ConstantExpression && exp.indices[i] instanceof ConstantExpression)) {
        if (!this.indices[i].shaojieEquality(exp.indices[i])) {
          return false;
        }
      }

    }
    return true;
  }

  @Override
  public IValue evaluate(Assignment assignment) throws EvaluationException {
    switch (array.getDimension()) {
    case 1: {
      assert this.indices.length == 1;
      IValue value = this.indices[0].evaluate(assignment);
      assert value instanceof IntValue;
      OneDArrayVariable oneD = (OneDArrayVariable) this.array;
      return assignment.getValue(oneD.getElement(value.asInt()));
    }
    case 2: {
      assert this.indices.length == 2;
      IValue row = this.indices[0].evaluate(assignment);
      IValue column = this.indices[1].evaluate(assignment);
      assert row instanceof IntValue;
      assert column instanceof IntValue;
      TwoDArrayVariable twoD = (TwoDArrayVariable) this.array;
      return assignment.getValue(twoD.getElement(row.asInt(), column.asInt()));
    }
    case 3: {
      assert this.indices.length == 3;
      IValue dimension1 = this.indices[0].evaluate(assignment);
      IValue dimension2 = this.indices[1].evaluate(assignment);
      IValue dimension3 = this.indices[2].evaluate(assignment);
      assert dimension1 instanceof IntValue;
      assert dimension2 instanceof IntValue;
      assert dimension3 instanceof IntValue;

      ThreeDArrayVariable threeD = (ThreeDArrayVariable) this.array;

      return assignment.getValue(threeD.getElement(dimension1.asInt(),
          dimension2.asInt(), dimension3.asInt()));
    }
    default:
      throw new UnsupportedOperationException("Only support at most 2-d array.");
    }
  }

  @Override
  public ValueType getType() {
    return this.array.getElementType();
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.array.getName());
    for (IExpression index : this.indices) {
      builder.append('[').append(index.toString()).append(']');
    }
    return builder.toString();
  }

  @Override
  public void typeCheck() throws TypeCheckingFailureException {
    for (IExpression index : this.indices) {
      index.typeCheck();
      if (!index.getType().equals(IValue.ValueType.Int)) {
        throw new TypeCheckingFailureException("In array indexing expression "
            + this + ", the index " + index + " should be of type int.");
      }
    }
  }

  @Override
  public void collectVariables(
      Map<IScalarVariable, List<IExpression>> variableCollector) {
    for (IExpression index : this.indices) {
      index.collectVariables(variableCollector);
    }
    if (this.indices.length == 1) {
      collectVariablesForOneDArray(variableCollector);
      return;
    }

    if (this.indices.length == 2) {
      collectVariablesForTwoDArray(variableCollector);
      return;
    }

    if (this.indices.length == 3) {
      collectVariableForThreeDArray(variableCollector);
      return;
    }

    throw new RuntimeException(
        "Cannot reach here. Only support 3d array so far. ");
  }

  private void collectVariableForThreeDArray(
      Map<IScalarVariable, List<IExpression>> variableCollector) {
    assert this.array instanceof ThreeDArrayVariable;
    ThreeDArrayVariable threeDArray = (ThreeDArrayVariable) this.array;
    IExpression x = this.indices[0];
    IExpression y = this.indices[1];
    IExpression z = this.indices[2];

    if (x instanceof ConstantExpression && y instanceof ConstantExpression
        && z instanceof ConstantExpression) {
      final IValue value1 = ((ConstantExpression) x).getValue();
      assert value1 instanceof IntValue;
      final int xIndex = ((IntValue) value1).asInt();
      if (xIndex >= threeDArray.getX())
        return;
      
      final IValue value2 = ((ConstantExpression) y).getValue();
      assert value2 instanceof IntValue;
      final int yIndex = ((IntValue) value2).asInt();
      if (yIndex >= threeDArray.getY())
        return;
      
      final IValue value3 = ((ConstantExpression) z).getValue();
      assert value3 instanceof IntValue;
      final int zIndex = ((IntValue) value3).asInt();
      if (zIndex >= threeDArray.getZ())
        return;
      

      variableCollector.put(threeDArray.getElement(xIndex, yIndex, zIndex), null);
      return;

    }

    for (IScalarVariable var : this.array.getAllElements()) {
      List<IExpression> expressions = new ArrayList<IExpression>();
      expressions.add(x);
      expressions.add(y);
      expressions.add(z);
      variableCollector.put(var, expressions);
    }
    return;

  }

  private void collectVariablesForTwoDArray(
      Map<IScalarVariable, List<IExpression>> variableCollector) {
    assert this.array instanceof TwoDArrayVariable;
    TwoDArrayVariable twoDArray = (TwoDArrayVariable) this.array;
    IExpression row = this.indices[0];
    IExpression column = this.indices[1];

    if (row instanceof ConstantExpression
        && column instanceof ConstantExpression) {
      final IValue value = ((ConstantExpression) row).getValue();
      assert value instanceof IntValue;
      final int rowIndex = ((IntValue) value).asInt();
      if (rowIndex >= twoDArray.getRow())
        return;

      final IValue value1 = ((ConstantExpression) column).getValue();
      assert value1 instanceof IntValue;
      final int columnIndex = ((IntValue) value1).asInt();
      if (columnIndex >= twoDArray.getColumn())
        return;

      variableCollector.put(twoDArray.getElement(rowIndex, columnIndex), null);
      return;

    }

    for (IScalarVariable var : this.array.getAllElements()) {
      List<IExpression> expressions = new ArrayList<IExpression>();
      expressions.add(row);
      expressions.add(column);
      variableCollector.put(var, expressions);
    }
    return;

    // if (row instanceof IExpressionWithDomain) {
    // if (column instanceof IExpressionWithDomain) {
    // for (Object rowObject : ((IExpressionWithDomain) row).getDomain()) {
    // assert rowObject instanceof Integer;
    // final int rowIndex = ((Integer) rowObject).intValue();
    // if (rowIndex >= twoDArray.getRow())
    // continue;
    // for (Object columnObject : ((IExpressionWithDomain) column)
    // .getDomain()) {
    // assert columnObject instanceof Integer;
    // final int columnIndex = ((Integer) columnObject).intValue();
    // if (columnIndex >= twoDArray.getColumn())
    // continue;
    // List<IExpression> expressions = new ArrayList<IExpression>();
    // expressions.add(row);
    // expressions.add(column);
    // variableCollector.put(twoDArray.getElement(rowIndex, columnIndex),
    // expressions);
    // //variableCollector.add(twoDArray.getElement(rowIndex, columnIndex));
    // }
    //
    // }
    // return;
    // } else if (column instanceof ConstantExpression) {
    // final IValue value = ((ConstantExpression) column).getValue();
    // assert value instanceof IntValue;
    // final int columnIndex = ((IntValue) value).asInt();
    // if (columnIndex >= twoDArray.getColumn())
    // return;
    // for (Object rowObject : ((IWithNameAndDomain) row).getDomain()) {
    // assert rowObject instanceof Integer;
    // final int rowIndex = ((Integer) rowObject).intValue();
    // if (rowIndex >= twoDArray.getRow())
    // continue;
    // List<IExpression> expressions = new ArrayList<IExpression>();
    // expressions.add(row);
    // expressions.add(column);
    // variableCollector.put(twoDArray.getElement(rowIndex, columnIndex),
    // expressions);
    // }
    // return;
    // } else {
    // for(IScalarVariable var: this.array.getAllElements()) {
    // variableCollector.put(var, null);
    // }
    // return;
    // }
    // } else if (row instanceof ConstantExpression) {
    // final IValue value = ((ConstantExpression) row).getValue();
    // assert value instanceof IntValue;
    // final int rowIndex = ((IntValue) value).asInt();
    // if (rowIndex >= twoDArray.getRow())
    // return;
    // if (column instanceof IExpressionWithDomain) {
    // for (Object columnObject : ((IExpressionWithDomain) column).getDomain())
    // {
    // assert columnObject instanceof Integer;
    // final int columnIndex = ((Integer) columnObject).intValue();
    // if (columnIndex >= twoDArray.getColumn())
    // continue;
    // List<IExpression> expressions = new ArrayList<IExpression>();
    // expressions.add(row);
    // expressions.add(column);
    // variableCollector.put(twoDArray.getElement(rowIndex, columnIndex),
    // expressions);
    // }
    // } else if (column instanceof IntValue) {
    // final int columnIndex = ((IntValue) column).asInt();
    // if (columnIndex >= twoDArray.getColumn())
    // return;
    // variableCollector.put(twoDArray.getElement(rowIndex, columnIndex), null);
    // return;
    // } else {
    // for(IScalarVariable var: this.array.getAllElements()) {
    // variableCollector.put(var, null);
    // }
    // return;
    // }
    // } else {
    // for(IScalarVariable var: this.array.getAllElements()) {
    // variableCollector.put(var, null);
    // }
    // return;
    // }
  }

  private void collectVariablesForOneDArray(
      Map<IScalarVariable, List<IExpression>> variableCollector) {
    IExpression first = this.indices[0];
    assert this.array instanceof OneDArrayVariable;
    OneDArrayVariable oneDArray = (OneDArrayVariable) this.array;

    if (first instanceof IExpressionWithDomain) {
      for (Object value : ((IExpressionWithDomain) first).getDomain()) {
        assert value instanceof Integer;
        int index = ((Integer) value).intValue();
        if (index >= oneDArray.length())
          continue;

        List<IExpression> expressions = new ArrayList<IExpression>();
        expressions.add(first);

        if (first instanceof VariableExpression) {
          variableCollector.put(oneDArray.getElement(index), expressions);
        } else {
          variableCollector.put(oneDArray.getElement(index), null);
        }

      }
    } else if (first instanceof ConstantExpression) {
      IValue value = ((ConstantExpression) first).getValue();
      assert value instanceof IntValue;
      final int index = ((IntValue) value).asInt();
      if (index < oneDArray.length())
        variableCollector.put(oneDArray.getElement(index), null);
    } else {
      for (IScalarVariable var : this.array.getAllElements()) {
        variableCollector.put(var, null);
      }

    }
  }

  @Override
  public void calculateUsefulVariables(Assignment assignment)
      throws EvaluationException {
    switch (array.getDimension()) {
    case 1: {
      assert this.indices.length == 1;
      this.indices[0].calculateUsefulVariables(assignment);
      IValue value = this.indices[0].evaluate(assignment);
      assert value instanceof IntValue;
      OneDArrayVariable oneD = (OneDArrayVariable) this.array;
      assignment.SetUsefulVariable(oneD.getElement(value.asInt()));
      return;

    }
    case 2: {
      assert this.indices.length == 2;
      this.indices[0].calculateUsefulVariables(assignment);
      this.indices[1].calculateUsefulVariables(assignment);
      IValue row = this.indices[0].evaluate(assignment);
      IValue column = this.indices[1].evaluate(assignment);
      assert row instanceof IntValue;
      assert column instanceof IntValue;
      TwoDArrayVariable twoD = (TwoDArrayVariable) this.array;
      assignment
          .SetUsefulVariable(twoD.getElement(row.asInt(), column.asInt()));
      return;

    }
    default:
      throw new UnsupportedOperationException("Only support at most 2-d array.");
    }

  }
}
