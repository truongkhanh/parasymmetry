package css.expr;

import java.util.Set;

public interface IExpressionWithDomain extends IExpression {

  Set<Object> getDomain();

}
