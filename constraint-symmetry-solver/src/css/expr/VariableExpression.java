package css.expr;

import java.util.List;
import java.util.Map;
import java.util.Set;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.value.IValue;
import css.value.IValue.ValueType;
import css.variable.IScalarVariable;


public class VariableExpression implements IExpression, IExpressionWithDomain {

  public VariableExpression(IScalarVariable variable) {
    super();
    this.variable = variable;
  }

  private final IScalarVariable variable;

  public final IScalarVariable getVariable() {
    return this.variable;
  }

  @Override
  public IValue evaluate(Assignment assignment) throws EvaluationException {
    return assignment.getValue(this.variable);
  }

  @Override
  public ValueType getType() {
    return this.variable.getType();
  }

  @Override
  public void typeCheck() throws TypeCheckingFailureException {
  }

  @Override
  public String toString() {
    return this.variable.getName();
  }

  @Override
  public void collectVariables(Map<IScalarVariable, List<IExpression>>  variableCollector) {
    variableCollector.put(this.variable, null);
  }

  @Override
  public Set<Object> getDomain() {
    return this.variable.getDomain();
  }

  @Override
  public void calculateUsefulVariables(Assignment assignment) {
    assignment.SetUsefulVariable(this.variable);

  }

  @Override
  public boolean shaojieEquality(IExpression other) {
    if (!this.getClass().equals(other.getClass())) {
      return false;
    }
    
    VariableExpression exp = (VariableExpression)other; 
    Map<String, String> annotations1 = this.getVariable().getAllAnnotations();
    Map<String, String> annotations2 = exp.getVariable().getAllAnnotations();
    
    
  
    return exp.getDomain().equals(this.getDomain()) && annotations1.equals(annotations2);
  }
  
  

}
