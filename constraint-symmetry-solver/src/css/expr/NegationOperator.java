package css.expr;


import java.util.List;
import java.util.Map;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.value.BoolValue;
import css.value.IValue;
import css.value.IValue.ValueType;
import css.variable.IScalarVariable;


public class NegationOperator implements IExpression {

  private final IExpression enclosed;

  public NegationOperator(IExpression enclosed) {
    super();
    this.enclosed = enclosed;
  }

  @Override
  public IValue evaluate(Assignment assignment) throws EvaluationException {
    return new BoolValue(!this.enclosed.evaluate(assignment).asBoolean());
  }

  @Override
  public ValueType getType() {
    return ValueType.Bool;
  }

  @Override
  public void typeCheck() throws TypeCheckingFailureException {
    this.enclosed.typeCheck();

    if (this.enclosed.getType().equals(IValue.ValueType.Bool)) {
      return;
    } else
      throw new TypeCheckingFailureException(
          "The type of the enclosed expression is not BOOL. Enclosed = "
              + this.enclosed.getType());
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("!(");
    builder.append(this.enclosed.toString());
    builder.append(')');
    return builder.toString();
  }

  @Override
  public void collectVariables(Map<IScalarVariable, List<IExpression>>  variableCollector) {
    this.enclosed.collectVariables(variableCollector);
  }

  @Override
  public void calculateUsefulVariables(Assignment assignment) {
    // TODO Auto-generated method stub

  }

  @Override
  public boolean shaojieEquality(IExpression other) {
    if (!this.getClass().equals(other.getClass())) {
      return false;
    }
    
    NegationOperator exp = (NegationOperator)other;
    return this.enclosed.equals(exp.enclosed);
  }
  
  
  

}
