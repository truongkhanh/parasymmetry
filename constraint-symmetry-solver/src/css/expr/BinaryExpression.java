package css.expr;


import java.util.List;
import java.util.Map;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.expr.operators.IBinaryOperator;
import css.value.IValue;
import css.value.IValue.ValueType;
import css.variable.IScalarVariable;


public class BinaryExpression implements IExpression {

  protected final IExpression left;

  protected final IExpression right;

  protected final IBinaryOperator operator;

  public BinaryExpression(IExpression left, IExpression right,
      IBinaryOperator operator) {
    super();
    this.left = left;
    this.right = right;
    this.operator = operator;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append('(');
    builder.append(this.left.toString());
    builder.append(' ').append(this.operator.toString()).append(' ');
    builder.append(this.right.toString());
    builder.append(')');
    return builder.toString();
  }

  public final IExpression getLeft() {
    return left;
  }

  public final IExpression getRight() {
    return right;
  }

  public final IBinaryOperator getOperator() {
    return this.operator;
  }

  @Override
  public IValue evaluate(Assignment assignment) throws EvaluationException {
    //this.calculateUsefulVariables(assignment);

    IValue leftValue = this.left.evaluate(assignment);

    if (this.operator.shortCircuit(leftValue) != null) {
      return leftValue;
    }

    return this.operator.evalute(leftValue, this.right.evaluate(assignment));
  }

  @Override
  public ValueType getType() {
    return this.operator.getType();
  }

  @Override
  public void typeCheck() throws TypeCheckingFailureException {
    this.left.typeCheck();
    this.right.typeCheck();

    String message = this.operator.typeCheck(this.left.getType(),
        this.right.getType());
    if (message == null || message.length() == 0)
      return;
    else
      throw new TypeCheckingFailureException("In expression " + this.toString()
          + ": " + message);
  }

  @Override
  public void collectVariables(Map<IScalarVariable, List<IExpression>>  variableCollector) {
    this.left.collectVariables(variableCollector);
    this.right.collectVariables(variableCollector);
  }

  @Override
  public void calculateUsefulVariables(Assignment assignment)
      throws EvaluationException {
    left.calculateUsefulVariables(assignment);
    right.calculateUsefulVariables(assignment);

  }

  @Override
  public boolean shaojieEquality(IExpression other) {
    if (!other.getClass().equals(this.getClass())) {
      return false;
    }
    
    final BinaryExpression exp = (BinaryExpression)other;
    return left.shaojieEquality(exp.left) && this.getOperator().equals(exp.getOperator()) 
        && right.shaojieEquality(exp.right);
  }
  
  

}
