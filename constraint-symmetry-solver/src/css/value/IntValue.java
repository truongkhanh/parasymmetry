package css.value;

public class IntValue implements IValue {

  private final int value;

  @Override
  public ValueType getType() {
    return ValueType.Int;
  }

  public IntValue(int value) {
    this.value = value;
  }

  @Override
  public boolean asBoolean() {
    throw new UnsupportedOperationException();
  }

  @Override
  public int asInt() {
    return this.value;
  }

  @Override
  public String toString() {
    return String.valueOf(this.value);
  }

}
