package css.value;

public class BoolValue implements IValue {

  private final boolean value;

  public BoolValue(boolean value) {
    this.value = value;
  }

  @Override
  public ValueType getType() {
    return ValueType.Bool;
  }

  @Override
  public boolean asBoolean() {
    return this.value;
  }

  @Override
  public int asInt() {
    throw new UnsupportedOperationException();
  }

  @Override
  public String toString() {
    return this.value ? "true" : "false";
  }

}
