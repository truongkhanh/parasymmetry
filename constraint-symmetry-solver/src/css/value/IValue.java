package css.value;

public interface IValue {

  enum ValueType {
    Int, Bool;
  }

  ValueType getType();

  boolean asBoolean();

  int asInt();

}
