package css.assignment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import css.constraint.Constraint;
import css.exceptions.EvaluationException;
import css.expr.IExpression;
import css.value.IValue;
import css.variable.IScalarVariable;

public class Assignment {

	public static List<Assignment> createAllAssignments(
			Map<IScalarVariable, List<IExpression>> variables, Constraint cons) {
		Set<Assignment> assignments = new HashSet<Assignment>();
		Map<IScalarVariable, IValue> assignment = new HashMap<IScalarVariable, IValue>();
		
		// for those whose value depends on some other variables' value.
		List<IScalarVariable> dependantVariables = new ArrayList<IScalarVariable>();
		
		// for those whose value is independent no matter what values other variables take.
		List<IScalarVariable> independantVariables = new ArrayList<IScalarVariable>();
		
		for (IScalarVariable var : variables.keySet()) {
			if (variables.get(var) != null) {
				dependantVariables.add(var);
			} else {
				independantVariables.add(var);
			}

		}

		createAssignment(assignments, assignment, variables,
				independantVariables, dependantVariables, 0, cons);
		return new ArrayList<Assignment>(assignments);
	}

	private static void createAssignment(Set<Assignment> assignmentCollector,
			Map<IScalarVariable, IValue> assignmentMap,
			Map<IScalarVariable, List<IExpression>> variables,
			List<IScalarVariable> independantVariables,
			List<IScalarVariable> dependantVariables, int varIndex,
			Constraint constraint) {
		if (varIndex == variables.size()) {
			Assignment assignment = new Assignment(assignmentMap);
			if (constraint.testSatisfaction(assignment)) {
				assignmentCollector.add(assignment);
			}
			return;
		}

		if (varIndex < independantVariables.size()) {

			IScalarVariable var = independantVariables.get(varIndex);
			for (int i = 0; i < var.sizeOfDomain(); ++i) {
				assignmentMap.put(var, var.getValue(i));
				createAssignment(assignmentCollector, assignmentMap, variables,
						independantVariables, dependantVariables, varIndex + 1,
						constraint);
			}
		} else {
			int indexInDependent = varIndex - independantVariables.size();
			List<IExpression> relyExpression = variables.get(dependantVariables
					.get(indexInDependent));
			Map<IScalarVariable, List<IExpression>> relyExpressionCollector = new HashMap<IScalarVariable, List<IExpression>>();
			for (IExpression expression : relyExpression) {
				expression.collectVariables(relyExpressionCollector);
			}

			if (assignmentMap.keySet().containsAll(
					relyExpressionCollector.keySet())) {
				try {
					List<IValue> indexValues = new ArrayList<IValue>();
					for (IExpression expression : relyExpression) {
						indexValues.add(expression.evaluate(new Assignment(
								assignmentMap)));
					}

					StringBuilder toCompare = new StringBuilder();
					for (IValue value : indexValues) {

						toCompare.append("[" + String.valueOf(value.asInt())
								+ "]");
					}

					IScalarVariable var = dependantVariables
							.get(indexInDependent);

					if (var.getName().contains(toCompare.toString())) {
						for (int i = 0; i < var.sizeOfDomain(); ++i) {
							assignmentMap.put(var, var.getValue(i));
							createAssignment(assignmentCollector,
									assignmentMap, variables,
									independantVariables, dependantVariables,
									varIndex + 1, constraint);
							assignmentMap.remove(var);
						}
					} else {
						createAssignment(assignmentCollector, assignmentMap,
								variables, independantVariables,
								dependantVariables, varIndex + 1, constraint);
					}

				} catch (EvaluationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				throw new RuntimeException(
						"some variables which the current variable is not evaluated");

			}

		}

	}

	private Map<IScalarVariable, IValue> assignment;

	private Map<IScalarVariable, Boolean> actuallyUsedVariables;

	private Assignment(Map<IScalarVariable, IValue> assignment) {
		super();
		this.assignment = new HashMap<IScalarVariable, IValue>(assignment);
		this.actuallyUsedVariables = new HashMap<IScalarVariable, Boolean>();
		for (IScalarVariable var : this.assignment.keySet()) {
			this.actuallyUsedVariables.put(var, null);

		}
	}

	public void ClearUselessVariables() {

		// Map<IScalarVariable, IValue> newAssignment = new
		// HashMap<IScalarVariable, IValue>();

		for (IScalarVariable var : actuallyUsedVariables.keySet()) {
			if (actuallyUsedVariables.get(var) == null
					|| !actuallyUsedVariables.get(var)) {
				assignment.remove(var);
			}

		}

	}

	public void SetUsefulVariable(IScalarVariable var) {
		this.actuallyUsedVariables.put(var, true);

	}

	public Collection<IValue> getAllValues() {
		return this.assignment.values();
	}

	public IValue getValue(IScalarVariable variable) {
		if (!this.assignment.containsKey(variable)) {
			throw new RuntimeException("Variable " + variable
					+ " does not exist in the assignment.");
		}
		return this.assignment.get(variable);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append('{');
		for (IScalarVariable var : assignment.keySet()) {
			if (builder.length() != 1)
				builder.append(", ");
			builder.append(var.getName()).append('=')
					.append(assignment.get(var));
		}
		builder.append('}');
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((assignment == null) ? 0 : assignment.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Assignment other = (Assignment) obj;
		if (assignment == null) {
			if (other.assignment != null)
				return false;
		} else if (!assignment.equals(other.assignment))
			return false;
		return true;
	}

}
