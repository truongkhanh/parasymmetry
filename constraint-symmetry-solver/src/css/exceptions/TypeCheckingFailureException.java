package css.exceptions;

public class TypeCheckingFailureException extends Exception {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public TypeCheckingFailureException(String message) {
    super(message);
  }

}
