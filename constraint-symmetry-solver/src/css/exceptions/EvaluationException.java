package css.exceptions;

public class EvaluationException extends Exception {

  private static final long serialVersionUID = 1L;

  public EvaluationException() {
    super();
  }

  public EvaluationException(String arg0, Throwable arg1) {
    super(arg0, arg1);
  }

  public EvaluationException(String arg0) {
    super(arg0);
  }

  public EvaluationException(Throwable arg0) {
    super(arg0);
  }

}
