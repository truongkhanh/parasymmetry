package css.exceptions;

public class ParsingFailureException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public ParsingFailureException() {
    super();
  }

  public ParsingFailureException(String arg0) {
    super(arg0);
  }

}
