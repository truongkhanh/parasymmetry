package css.main;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.Stack;

import org.antlr.runtime.RecognitionException;

import css.core.CPLProgram;
import css.exceptions.TypeCheckingFailureException;
import css.parser.CPLProgramBuilder;
import css.symmetrygraph.SymmetryConstraintGraph;
import css.symmetrygraph.color.ColorPartitioningStrategy;
import css.symmetrygraph.color.IColorPartitioningStrategy;

public class Main {

	private final static IColorPartitioningStrategy COLORING_STRATEGY = new ColorPartitioningStrategy();

	private static String nameWithoutExtension;

	// public static void main(String[] args) throws FileNotFoundException,
	// IOException, RecognitionException, TypeCheckingFailureException {
	// if (args.length != 1) {
	// System.out.println("ccp <program-file-path>");
	// return;
	// }
	// String programPath = args[0];
	//
	// //// List<Integer> dimensions = new ArrayList<Integer>();
	// //// dimensions.add(10);
	// ////// dimensions.add(3);
	// ////// dimensions.add(3);
	// //// generate("_cstate", "_cstate", dimensions);
	// //
	// // String programPath =
	// "/home/neo/workspace/constraint-symmetry-solver2/"
	// // +
	// "constraint-symmetry-solver/test-cpl/leaderelectionring/12/leaderelectionring.txt";
	// File programFile = new File(programPath);
	//
	// if (!programFile.exists()) {
	// System.err.println("CPL path '" + programFile + "' does not exist.");
	// return;
	// }
	// long startTime = System.currentTimeMillis();
	// convert(programFile);
	// long endTime = System.currentTimeMillis();
	//
	// System.out.println("total running time: " + (endTime - startTime));
	// }
	public static void main(String[] args) throws FileNotFoundException,
			IOException, RecognitionException, TypeCheckingFailureException {

		// this is used when debugging
		if (args.length != 1) {
			System.out.println("<filename>");
			return;
		}
		String fileName = args[0];

		// FIXME: where to store the saucy and mapping files?
		// the input filename is like XXX.constraints.txt. the true file name
		// without extension is XXX
		nameWithoutExtension = fileName.substring(0, fileName.lastIndexOf("."));
		nameWithoutExtension = nameWithoutExtension.substring(0,
				nameWithoutExtension.lastIndexOf('.')); // the true file name.

		// / redirect the console output to a log file.
		File logFile = new File(nameWithoutExtension + ".graph.log.txt");
		BufferedOutputStream logBuffer = new BufferedOutputStream(
				new FileOutputStream(logFile));
		PrintStream log = new PrintStream(logBuffer, true);
		System.setOut(log);

		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		String line = null;
		StringBuilder builder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append(ls);
		}

		reader.close();
		
		// //List<Integer> dimensions = new ArrayList<Integer>();
		// //dimensions.add(10);
		// ////dimensions.add(3);
		// ////dimensions.add(3);
		// //generate("_cstate", "_cstate", dimensions);
		//
		// String programPath =
		// "/home/neo/workspace/constraint-symmetry-solver2/"
		// +
		// "constraint-symmetry-solver/test-cpl/leaderelectionring/12/leaderelectionring.txt";

		long startTime = System.currentTimeMillis();
		// convert(programFile);
		convert(fileName, builder.toString());
		long endTime = System.currentTimeMillis();

		System.out.println("total running time: " + (endTime - startTime));
	}

	public static void generate(String a, String b, List<Integer> dimensions) {
		generate(a, b, dimensions, new Stack<Integer>());
	}

	private static void generate(String a, String b, List<Integer> dimensions,
			Stack<Integer> indices) {
		final int indicesSize = indices.size();
		if (indicesSize >= dimensions.size()) {
			StringBuilder builder = new StringBuilder();
			for (Integer i : indices) {
				builder.append('[').append(i).append(']');
			}
			final String stringIndices = builder.toString();
			System.out.println(a + stringIndices + "==" + b + stringIndices
					+ ";");
			return;
		}
		final int dimension = dimensions.get(indicesSize).intValue();
		for (int i = 0; i < dimension; ++i) {
			indices.push(i);
			generate(a, b, dimensions, indices);
			indices.pop();
		}
	}

	// public static void convert(File programFile) throws
	// FileNotFoundException,
	// IOException, RecognitionException, TypeCheckingFailureException {
	// System.out.println("building CPL program...");
	// CPLProgram program = CPLProgramBuilder.build(programFile);
	// System.out.println("done with CPL program");
	//
	// System.out.println("building symmetry constraint graph...");
	// SymmetryConstraintGraph graph = new SymmetryConstraintGraph(program,
	// COLORING_STRATEGY);
	// System.out.println("done with symmetry constraint graph");
	//
	// String nameWithoutExtension = programFile.getName().substring(0,
	// programFile.getName().lastIndexOf(".txt"));
	// File saucyFile = new File(programFile.getParentFile(),
	// nameWithoutExtension + ".saucy.txt");
	// File mappingFile = new File(programFile.getParentFile(),
	// nameWithoutExtension + ".mapping.txt");
	//
	// System.out.println("writing saucy to file " + saucyFile);
	// BufferedWriter saucyWriter = new BufferedWriter(new FileWriter(
	// saucyFile));
	// BufferedOutputStream stream;
	//
	// graph.outputSaucyGraph(saucyWriter);
	// saucyWriter.close();
	// System.out.println("done with saucy file");
	//
	// System.out.println("writing mapping to file " + mappingFile);
	// BufferedWriter mappingWriter = new BufferedWriter(new FileWriter(
	// mappingFile));
	// graph.save(mappingWriter);
	// mappingWriter.close();
	// System.out.println("done with mapping file");
	// System.out.println("DONE!!!");
	// // writeStringToFile(saucyFile, graph.outputSaucyGraph());
	// // writeStringToFile(mappingFile, graph.toString());
	// // System.out.println("wrote saucy to file " + saucyFile);
	// // System.out.println("wrote mapping to file " + mappingFile);
	// }
	public static void convert(String fileName, String content)
			throws FileNotFoundException, IOException, RecognitionException,
			TypeCheckingFailureException {
		System.out.println("building CPL program...");
		CPLProgram program = CPLProgramBuilder.build(content);
		System.out.println("done with CPL program");

		System.out.println("building symmetry constraint graph...");
		SymmetryConstraintGraph graph = new SymmetryConstraintGraph(program,
				COLORING_STRATEGY);
		System.out.println("done with symmetry constraint graph");

		File saucyFile = new File(nameWithoutExtension + ".saucy.txt");
		File mappingFile = new File(nameWithoutExtension + ".mapping.txt");

		System.out.println("writing saucy to file " + saucyFile);
		BufferedWriter saucyWriter = new BufferedWriter(new FileWriter(
				saucyFile));
		// BufferedOutputStream stream;

		graph.outputSaucyGraph(saucyWriter);
		saucyWriter.close();
		System.out.println("done with saucy file");

		System.out.println("writing mapping to file " + mappingFile);
		BufferedWriter mappingWriter = new BufferedWriter(new FileWriter(
				mappingFile));
		graph.save(mappingWriter);
		mappingWriter.close();
		System.out.println("done with mapping file");
		System.out.println("DONE!!!");
		// writeStringToFile(saucyFile, graph.outputSaucyGraph());
		// writeStringToFile(mappingFile, graph.toString());
		// System.out.println("wrote saucy to file " + saucyFile);
		// System.out.println("wrote mapping to file " + mappingFile);
		
		//Khanh
		//we need to call saucy to get the output
		Runtime r = Runtime.getRuntime();
		Process p = r.exec(new String[]{"saucy.exe", nameWithoutExtension + ".saucy.txt"});
		InputStream input = p.getInputStream();
		
		// write the inputStream to a FileOutputStream
		OutputStream outputStream = new FileOutputStream(new File(nameWithoutExtension + ".output.txt"));
		 
		int read = 0;
		byte[] bytes = new byte[1024];
 
		while ((read = input.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}
		
		outputStream.close();
	}
}
