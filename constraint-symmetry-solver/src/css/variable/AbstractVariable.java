package css.variable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class AbstractVariable implements IScalarVariable {

  protected final String name;

  private final Map<String, String> annotations;

  public AbstractVariable(String name, Map<String, String> annotations) {
    super();
    this.name = name;
    this.annotations = new HashMap<String, String>(annotations);
  }

  @Override
  public String getAnnotationValue(String annotationName) {
    return this.annotations.get(annotationName);
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    AbstractVariable other = (AbstractVariable) obj;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(this.getType()).append(' ').append(this.getName())
        .append(" : ");
    builder.append('{');
    final int sizeOfDomain = this.sizeOfDomain();
    if (sizeOfDomain > 0)
      builder.append(this.getValue(0));
    for (int i = 1; i < sizeOfDomain; ++i)
      builder.append(", ").append(this.getValue(i));
    builder.append('}');

    if (this.annotations.size() > 0) {
      builder.append(" @(");
      Iterator<Map.Entry<String, String>> annotationIterator = this.annotations
          .entrySet().iterator();
      if (annotationIterator.hasNext()) {
        Map.Entry<String, String> firstElement = annotationIterator.next();
        builder.append(firstElement.getKey()).append('=')
            .append(firstElement.getValue());
      }
      while (annotationIterator.hasNext()) {
        Map.Entry<String, String> element = annotationIterator.next();
        builder.append(", ").append(element.getKey()).append('=')
            .append(element.getValue());
      }
      builder.append(')');
    }

    return builder.toString();
  }

  @Override
  public Map<String, String> getAllAnnotations() {
   return this.annotations;
  }
}
