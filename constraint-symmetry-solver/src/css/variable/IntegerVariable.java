package css.variable;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import css.value.IValue;
import css.value.IntValue;
import css.value.IValue.ValueType;


public class IntegerVariable extends AbstractVariable {

  private final IntValue[] domain;

  public IntegerVariable(String name, Set<Integer> domain) {
    this(name, domain, Collections.<String, String> emptyMap());
  }

  public IntegerVariable(String name, Set<Integer> domain,
      Map<String, String> annotations) {
    super(name, annotations);
    this.domain = new IntValue[domain.size()];
    int i = -1;
    for (Integer integer : domain) {
      this.domain[++i] = new IntValue(integer.intValue());
    }
  }

  @Override
  public ValueType getType() {
    return ValueType.Int;
  }

  @Override
  public int sizeOfDomain() {
    return domain.length;
  }

  @Override
  public IValue getValue(int index) {
    return this.domain[index];
  }

  @Override
  public Set<Object> getDomain() {
    HashSet<Object> domain = new HashSet<Object>();
    for (IntValue value : this.domain) {
      domain.add(value.asInt());
    }
    return domain;
  }

}
