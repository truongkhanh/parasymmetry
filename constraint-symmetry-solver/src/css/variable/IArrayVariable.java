package css.variable;

import java.util.List;

import css.value.IValue;


/**
 * this serves as an index map for a list of elements.
 * 
 * @author neo
 * 
 */
public interface IArrayVariable extends IAnnotatable {

  String getName();

  IValue.ValueType getElementType();

  // int sizeOfDomain();

  List<IScalarVariable> getAllElements();

  int getDimension();

  boolean shaojieEquality(IArrayVariable array);

 // boolean shaojieEquality(IArrayVariable other);

  // IVariable getElement(int index);
  //
  // IVariable getElement(int row, int column);

  // Set<Object> getDomain();

}
