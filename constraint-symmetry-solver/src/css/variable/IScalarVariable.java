package css.variable;

import css.value.IValue;

public interface IScalarVariable extends IWithNameAndDomain, IAnnotatable {

	// String getName();
	//
	// IValue.ValueType getType();

	/**
	 * how many values exist in the domain of current variable.
	 * @return
	 */
	int sizeOfDomain();

	/**
	 * get the specified value according to the position in a domain.
	 * @param index
	 * @return
	 */
	IValue getValue(int index);

	// Set<Object> getDomain();

}
