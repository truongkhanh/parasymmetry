package css.variable;

import css.value.IValue;

/**
 * local variable is the bound variable in forall/exists expression.
 * 
 * its value is not assigned by the Assignment, but controlled by forall/exists.
 * 
 * @author neo
 * 
 */
public interface IScalarLocalVariable extends IWithNameAndDomain {

  IValue getCurrentValue();

}
