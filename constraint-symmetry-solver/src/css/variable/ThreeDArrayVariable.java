package css.variable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ThreeDArrayVariable extends AbstractArrayVariable {

  private final int x;

  private final int y;

  private final int z;

  private ThreeDArrayVariable(String name, int x, int y, int z,
      List<IScalarVariable> elements, Map<String, String> annotations) {
    super(name, elements, annotations);
    if (x <= 0)
      throw new IllegalArgumentException("x cannot be <= 0");
    if (y <= 0)
      throw new IllegalArgumentException("y cannot be <= 0");
    if (z <= 0)
      throw new IllegalArgumentException("z cannot be <= 0");
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  public int getZ() {
    return z;
  }

  public IScalarVariable getElement(int x, int y, int z) {
    if (x >= this.x || x < 0)
      throw new IndexOutOfBoundsException("x index is " + x + ", x = " + this.x);
    if (y >= this.y || y < 0)
      throw new IndexOutOfBoundsException("y index is " + y + ", y = " + this.y);
    if (z >= this.z || z < 0)
      throw new IndexOutOfBoundsException("z index is " + z + ", z = " + this.z);
    return this.elements.get(x * this.y * this.z + y * this.z + z);
  }

  public static ThreeDArrayVariable createBooleanArray(String name, int x,
      int y, int z, Set<Boolean> domain, Map<String, String> annotations) {
    final int length = x * y * z;
    List<IScalarVariable> elements = new ArrayList<IScalarVariable>();
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) {
        for (int k = 0; k < z; ++k) {
          elements.add(new BooleanVariable(name + '[' + i + "][" + j + "][" + k
              + ']', domain, annotations));
        }
      }
    }
    assert elements.size() == length;
    return new ThreeDArrayVariable(name, x, y, z, elements, annotations);
  }
  
  public static ThreeDArrayVariable createIntegerArray(String name, int x,
      int y, int z, Set<Integer> domain, Map<String, String> annotations) {
    final int length = x * y * z;
    List<IScalarVariable> elements = new ArrayList<IScalarVariable>();
    for (int i = 0; i < x; ++i) {
      for (int j = 0; j < y; ++j) {
        for (int k = 0; k < z; ++k) {
          elements.add(new IntegerVariable(name + '[' + i + "][" + j + "][" + k
              + ']', domain, annotations));
        }
      }
    }
    assert elements.size() == length;
    return new ThreeDArrayVariable(name, x, y, z, elements, annotations);
  }

  @Override
  public int getDimension() {
    return 3;
  }

}
