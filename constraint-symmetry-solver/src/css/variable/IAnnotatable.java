package css.variable;

import java.util.Map;

public interface IAnnotatable {

  String getAnnotationValue(String annotationName);
  
   Map<String, String> getAllAnnotations();

}
