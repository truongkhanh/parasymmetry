package css.variable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import css.value.IValue.ValueType;


public abstract class AbstractArrayVariable implements IArrayVariable {

  private final String name;

  protected final List<IScalarVariable> elements;

  private final Map<String, String> annotations;

  protected AbstractArrayVariable(String name, List<IScalarVariable> elements,
      Map<String, String> annotations) {
    super();
    if (elements.isEmpty()) {
      throw new RuntimeException("Array cannot be of length 0.");
    }
    this.annotations = new HashMap<String, String>(annotations);
    this.name = name;
    this.elements = Collections
        .unmodifiableList(new ArrayList<IScalarVariable>(elements));
  }

  @Override
  public String getAnnotationValue(String annotationName) {
    return this.annotations.get(annotationName);
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  public ValueType getElementType() {
    assert this.elements.size() > 0;
    return this.elements.get(0).getType();
  }

  @Override
  public List<IScalarVariable> getAllElements() {
    return this.elements;
  }

  @Override
  public Map<String, String> getAllAnnotations() {
    return this.annotations;
  }

  @Override
  public boolean shaojieEquality(IArrayVariable array) {
    if (this.getDimension() != array.getDimension()) {
      return false;
    }

    if (!this.getAllAnnotations().equals(array.getAllAnnotations())) {
      return false;
    }

    return this.elements.get(0).getDomain()
        .equals(array.getAllElements().get(0).getDomain());
  }

}
