package css.variable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class OneDArrayVariable extends AbstractArrayVariable {

  public static OneDArrayVariable createBooleanArray(String name, int length,
      Set<Boolean> domain, Map<String, String> annotations) {
    if (length <= 0)
      throw new IllegalArgumentException("length cannot be <= 0");
    List<IScalarVariable> elements = new ArrayList<IScalarVariable>(length);
    for (int i = 0; i < length; ++i) {
      elements.add(new BooleanVariable(name + '[' + i + ']', domain,
          annotations));
    }
    assert elements.size() == length;
    return new OneDArrayVariable(name, elements, annotations);
  }

  public IScalarVariable getElement(int index) {
    if (index < 0 || index >= this.elements.size())
      throw new IndexOutOfBoundsException(this.getName() + "  index is "
          + index + ", array length = " + this.elements.size());
    return this.elements.get(index);
  }

  public static OneDArrayVariable createIntegerArray(String name, int length,
      Set<Integer> domain, Map<String, String> annotations) {
    if (length <= 0)
      throw new IllegalArgumentException("length cannot be <= 0");
    List<IScalarVariable> elements = new ArrayList<IScalarVariable>(length);
    for (int i = 0; i < length; ++i) {
      elements.add(new IntegerVariable(name + '[' + i + ']', domain,
          annotations));
    }
    assert elements.size() == length;
    return new OneDArrayVariable(name, elements, annotations);
  }

  public int length() {
    return this.elements.size();
  }

  private OneDArrayVariable(String name, List<IScalarVariable> elements,
      Map<String, String> annotations) {
    super(name, elements, annotations);
  }

  @Override
  public int getDimension() {
    return 1;
  }

}
