package css.variable;

import java.util.Set;

import css.value.IValue;


public interface IWithNameAndDomain {

  String getName();

  IValue.ValueType getType();

  Set<Object> getDomain();

}
