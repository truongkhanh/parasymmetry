package css.variable;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import css.value.BoolValue;
import css.value.IValue;
import css.value.IValue.ValueType;


public class BooleanVariable extends AbstractVariable {

  private final BoolValue[] domain;

  public BooleanVariable(String name, Set<Boolean> domain) {
    this(name, domain, Collections.<String, String> emptyMap());
  }

  public BooleanVariable(String name, Set<Boolean> domain,
      Map<String, String> annotations) {
    super(name, annotations);
    this.domain = new BoolValue[domain.size()];
    int i = -1;
    for (Boolean boolean1 : domain) {
      this.domain[++i] = new BoolValue(boolean1.booleanValue());
    }
  }

  @Override
  public ValueType getType() {
    return ValueType.Bool;
  }

  @Override
  public int sizeOfDomain() {
    return domain.length;
  }

  @Override
  public IValue getValue(int index) {
    return this.domain[index];
  }

  @Override
  public Set<Object> getDomain() {
    HashSet<Object> domain = new HashSet<Object>();
    for (BoolValue value : this.domain) {
      domain.add(value.asBoolean());
    }
    return domain;
  }
}
