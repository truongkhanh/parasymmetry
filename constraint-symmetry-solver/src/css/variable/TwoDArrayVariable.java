package css.variable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TwoDArrayVariable extends AbstractArrayVariable {

  private final int row;

  private final int column;

  private TwoDArrayVariable(String name, int row, int column,
      List<IScalarVariable> elements, Map<String, String> annotations) {
    super(name, elements, annotations);
    if (row <= 0)
      throw new IllegalArgumentException("row cannot be <= 0");
    if (column <= 0)
      throw new IllegalArgumentException("column cannot be <= 0");
    this.row = row;
    this.column = column;
  }

  public int getRow() {
    return row;
  }

  public int getColumn() {
    return column;
  }

  public IScalarVariable getElement(int rowIndex, int columnIndex) {
    if (rowIndex >= row || rowIndex < 0)
      throw new IndexOutOfBoundsException("rowIndex is " + rowIndex + ", row "
          + this.row);
    if (columnIndex >= column || columnIndex < 0)
      throw new IndexOutOfBoundsException(this.getName() + "  columnIndex is " + columnIndex
          + ", column " + this.column);
    return this.elements.get(rowIndex * column + columnIndex);
  }

  public static TwoDArrayVariable createBooleanArray(String name, int row,
      int column, Set<Boolean> domain, Map<String, String> annotations) {
    final int length = row * column;
    List<IScalarVariable> elements = new ArrayList<IScalarVariable>(length);
    for (int i = 0; i < row; ++i) {
      for (int j = 0; j < column; ++j)
        elements.add(new BooleanVariable(name + '[' + i + "][" + j + ']',
            domain, annotations));
    }
    assert elements.size() == length;
    return new TwoDArrayVariable(name, row, column, elements, annotations);
  }

  public static TwoDArrayVariable createIntegerArray(String name, int row,
      int column, Set<Integer> domain, Map<String, String> annotations) {
    final int length = row * column;
    List<IScalarVariable> elements = new ArrayList<IScalarVariable>(length);
    for (int i = 0; i < row; ++i) {
      for (int j = 0; j < column; ++j)
        elements.add(new IntegerVariable(name + '[' + i + "][" + j + ']',
            domain, annotations));
    }
    assert elements.size() == length;
    return new TwoDArrayVariable(name, row, column, elements, annotations);
  }

  @Override
  public int getDimension() {
    return 2;
  }

}
