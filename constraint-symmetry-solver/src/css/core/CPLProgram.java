package css.core;

import java.util.ArrayList;
import java.util.List;

import css.constraint.Constraint;
import css.variable.IScalarVariable;


public class CPLProgram {

  private final List<IScalarVariable> variables;

  private final List<Constraint> constraints;

  public CPLProgram(List<IScalarVariable> variables, List<Constraint> constraints) {
    super();
    this.variables = new ArrayList<IScalarVariable>(variables);
    this.constraints = new ArrayList<Constraint>(constraints);
  }

  public List<IScalarVariable> getVariables() {
    return variables;
  }

  public List<Constraint> getConstraints() {
    return constraints;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (IScalarVariable var : variables)
      builder.append(var.toString()).append('\n');
    builder.append('\n');
    for (Constraint c : this.constraints)
      builder.append(c.toString()).append('\n');
    return builder.toString();
  }

}