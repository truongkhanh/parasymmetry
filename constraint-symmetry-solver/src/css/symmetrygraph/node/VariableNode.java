package css.symmetrygraph.node;

import java.util.ArrayList;
import java.util.List;

import css.variable.IScalarVariable;

public class VariableNode extends AbstractNode {

	private final IScalarVariable variable;

	private List<ValueNode> valueNodes;

	public VariableNode(IScalarVariable variable) {
		super();
		this.variable = variable;
		this.valueNodes = this.createValueNodes(variable);
	}

	private List<ValueNode> createValueNodes(IScalarVariable variable) {
		int size = variable.sizeOfDomain();
		List<ValueNode> valueNodes = new ArrayList<ValueNode>();
		for (int i = 0; i < size; ++i) {
			valueNodes.add(new ValueNode(variable.getValue(i)));
		}
		return valueNodes;
	}

	public List<ValueNode> getValueNodes() {
		return valueNodes;
	}

	public IScalarVariable getVariable() {
		return variable;
	}

	@Override
	protected void toString(StringBuilder builder) {
		builder.append("variable=").append(this.variable);
	}

	@Override
	protected String getType() {
		return "Variable";
	}
}
