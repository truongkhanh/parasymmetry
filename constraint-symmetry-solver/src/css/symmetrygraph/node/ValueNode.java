package css.symmetrygraph.node;

import css.value.IValue;

public class ValueNode extends AbstractNode {

  private final IValue value; 
  
  private  boolean isNotIsolated; 
  
  private boolean existGuardInit;
  
  private boolean isNotUseless;

  public ValueNode(IValue value) {
    super();
    this.value = value;
  }

  public IValue getValue() {
    return value;
  }

  @Override
  protected void toString(StringBuilder builder) {
    builder.append("value=").append(value);
  }

  @Override
  protected String getType() {
    return "Value";
  }

  public boolean isNotIsolated() {
    return isNotIsolated;
  }

  public void setNotIsolated(boolean isIsolated) {
    this.isNotIsolated = isIsolated;
  }

  public boolean isNotUseless() {
    return isNotUseless;
  }

  public void setNotUseless(boolean isNotUseless) {
    this.isNotUseless = isNotUseless;
  }

  public boolean isExistGuardInit() {
    return existGuardInit;
  }

  public void setExistGuardInit(boolean existGuardInit) {
    this.existGuardInit = existGuardInit;
  }
  
  
  
}
