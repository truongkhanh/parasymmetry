package css.symmetrygraph.node;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import css.assignment.Assignment;
import css.constraint.Constraint;
import css.value.IValue;

public class ConstraintNode extends AbstractNode {

	private final Constraint constraint;

	private List<AssignmentNode> assignments;

	public ConstraintNode(Constraint constraint,
			Map<IValue, ValueNode> valueNodeMap) {
		super();
		this.constraint = constraint;
		this.assignments = (List<AssignmentNode>) this.createAssignments(
				constraint, valueNodeMap);
	}

	public List<AssignmentNode> getAssignments() {
		return assignments;
	}

	private List<? extends AssignmentNode> createAssignments(
			Constraint constraint, Map<IValue, ValueNode> valueNodeMap) {
		List<AssignmentNode> assignmentNodes = new ArrayList<AssignmentNode>();
		List<Assignment> assignments = constraint.getSatisfiableAssignments();
		for (Assignment assign : assignments) {
			Set<ValueNode> valueNodes = new HashSet<ValueNode>();
			for (IValue value : assign.getAllValues()) {
				valueNodes.add(valueNodeMap.get(value));
			}
			assignmentNodes.add(new AssignmentNode(assign, this, valueNodes));
		}
		return assignmentNodes;
	}

	// public List<AssignmentNode> getAssignments() {
	// return assignments;
	// }

	public Constraint getConstraint() {
		return constraint;
	}

	@Override
	protected void toString(StringBuilder builder) {
		builder.append("constraint=").append(this.constraint);
	}

	@Override
	protected String getType() {
		return "Constraint";
	}
}
