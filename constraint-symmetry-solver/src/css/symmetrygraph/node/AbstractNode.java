package css.symmetrygraph.node;

public abstract class AbstractNode {

  private static int idSequence;

  private final int id;

  private int idForColor;

  public AbstractNode() {
    this.id = idSequence++;
  }

  public void setIdForColor(int idForColor) {
    this.idForColor = idForColor;
  }

  public int getIdForColor() {
    return idForColor;
  }

  public int getId() {
    return id;
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append("").append(this.idForColor).append("-")
        .append(this.getType()).append(" [id=").append(this.id).append(", ");
    this.toString(builder);
    builder.append(']');
    return builder.toString();
  }

  protected abstract String getType();

  protected abstract void toString(StringBuilder builder);
}
