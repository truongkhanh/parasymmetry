package css.symmetrygraph.node;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import css.assignment.Assignment;


public class AssignmentNode extends AbstractNode {

  private final Assignment assignment;

  private final ConstraintNode constraintNode;

  private  List<ValueNode> valueNodes;

  public AssignmentNode(Assignment assignment, ConstraintNode constraintNode,
      Set<ValueNode> valueNodes) {
    super();
    this.assignment = assignment;
    this.constraintNode = constraintNode;
    this.valueNodes = (new ArrayList<ValueNode>(
        valueNodes));
  }

  public List<ValueNode> getValueNodes() {
    return valueNodes;
  }

  public ConstraintNode getConstraintNode() {
    return constraintNode;
  }

  public Assignment getAssignment() {
    return assignment;
  }

  @Override
  protected void toString(StringBuilder builder) {
    builder.append("assignment=").append(this.assignment);
  }

  @Override
  protected String getType() {
    return "Assignment";
  }
}
