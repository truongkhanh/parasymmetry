package css.symmetrygraph.color;

import java.util.List;

import css.symmetrygraph.SymmetryConstraintGraph;


public interface IColorPartitioningStrategy {

  List<ColorZone> buildZones(SymmetryConstraintGraph graph);
}
