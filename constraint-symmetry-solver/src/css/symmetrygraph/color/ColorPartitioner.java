package css.symmetrygraph.color;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import css.symmetrygraph.SymmetryConstraintGraph;
import css.symmetrygraph.node.AbstractNode;


public class ColorPartitioner implements IColorPartitioner {

  private final List<ColorZone> zones;

  private final Map<AbstractNode, ColorZone> node2ZoneMap;

  public ColorPartitioner(SymmetryConstraintGraph graph,
      IColorPartitioningStrategy strategy) {
    this.zones = strategy.buildZones(graph);
    this.node2ZoneMap = this.buildNode2ZoneMap(this.zones);
  }

  private Map<AbstractNode, ColorZone> buildNode2ZoneMap(List<ColorZone> zones) {
    Map<AbstractNode, ColorZone> map = new HashMap<AbstractNode, ColorZone>();
    for (ColorZone zone : zones) {
      for (AbstractNode node : zone.getNodes()) {
        if (map.containsKey(node))
          throw new RuntimeException();
        map.put(node, zone);
      }
    }
    return map;
  }

  /*
   * (non-Javadoc)
   * 
   * @see donkey.symmetrygraph.color.IColorPartitioner#numberOfZones()
   */
  @Override
  public int numberOfZones() {
    return this.zones.size();
  }

  /*
   * (non-Javadoc)
   * 
   * @see donkey.symmetrygraph.color.IColorPartitioner#getZones()
   */
  @Override
  public List<ColorZone> getZones() {
    return Collections.unmodifiableList(zones);
  }

  /*
   * (non-Javadoc)
   * 
   * @see
   * donkey.symmetrygraph.color.IColorPartitioner#getColor(donkey.symmetrygraph
   * .node.AbstractNode)
   */
  @Override
  public int getColor(AbstractNode node) {
    return this.node2ZoneMap.get(node).getColor();
  }

}
