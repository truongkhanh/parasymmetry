package css.symmetrygraph.color;

import java.util.List;

import css.symmetrygraph.node.AbstractNode;


public interface IColorPartitioner {

  public abstract int numberOfZones();

  public abstract List<ColorZone> getZones();

  public abstract int getColor(AbstractNode node);

}