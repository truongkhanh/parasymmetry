package css.symmetrygraph.color;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import css.expr.IExpression;
import css.symmetrygraph.SymmetryConstraintGraph;
import css.symmetrygraph.node.AbstractNode;
import css.symmetrygraph.node.AssignmentNode;
import css.symmetrygraph.node.ConstraintNode;
import css.symmetrygraph.node.ValueNode;
import css.symmetrygraph.node.VariableNode;
import css.utils.Constants;

public class ColorPartitioningStrategy extends DefaultColorPartitioningStrategy {

	@Override
	public List<ColorZone> buildZones(SymmetryConstraintGraph graph) {
		List<ColorZone> zones = new ArrayList<ColorZone>();
		zones.addAll(buildVariableColorZones(graph));
		zones.addAll(buildConstaintColorZones(graph));
		return zones;
	}

	/**
	 * build constraint zones
	 */
	@Override
	protected Collection<? extends ColorZone> buildConstaintColorZones(
			SymmetryConstraintGraph graph) {
		List<ColorZone> zones = new ArrayList<ColorZone>();

		for (ConstraintNode consNode : graph.getConstraintNodes()) {
			IExpression exp = consNode.getConstraint().getExpression();

			boolean hasFound = false;
			for (ColorZone zone : zones) {

				if (zone.getUnfinalizedNodes().size() > 0) {
					ConstraintNode con = (ConstraintNode) zone
							.getUnfinalizedNodes().iterator().next();

					if (exp.shaojieEquality(con.getConstraint().getExpression())) {
						zone.addNode(consNode);
						hasFound = true;
					}
				}
			}

			if (!hasFound) {
				ColorZone zone = new ColorZone();
				zones.add(zone);
				zone.addNode(consNode);
			}

		}

		List<ColorZone> assignZones = new ArrayList<ColorZone>();
		for (ColorZone zone : zones) {
			ColorZone assignmentZone = new ColorZone();
			for (ConstraintNode consNode : graph.getConstraintNodes()) {
				if (zone.getNodes().contains(consNode)) {
					for (AssignmentNode assignNode : consNode.getAssignments()) {
						assignmentZone.addNode(assignNode);
					}
				}
			}

			assignZones.add(assignmentZone);
		}

		zones.addAll(assignZones);
		List<ColorZone> returnZones = new ArrayList<ColorZone>();
		for (ColorZone zone : zones) {
			if (zone.getNodes().size() > 0) {
				returnZones.add(zone);
			}

		}

		return returnZones;
	}

	/**
	 * build variables color zone. Strategy:
	 * <p>
	 * 1. each of them is a local variable of the same domain in the same
	 * summand
	 * </p>
	 * 
	 * <p>
	 * 2. or each of them is an original global variable of the same domain
	 * </p>
	 * 
	 * <p>
	 * 3. or each of them is the latest version of a global variable of the same
	 * domain.
	 */
	@Override
	protected Collection<ColorZone> buildVariableColorZones(
			SymmetryConstraintGraph graph) {

		// 1. separate variables to zones regarding to the domains they have.
		Map<Set<Object>, ColorZone> variableZone = new HashMap<Set<Object>, ColorZone>();
		for (VariableNode varNode : graph.getVariableNodes()) {
			Set<Object> domain = varNode.getVariable().getDomain();
			if (variableZone.containsKey(domain)) {
				variableZone.get(domain).addNode(varNode);
			} else {
				ColorZone newZone = new ColorZone();
				newZone.addNode(varNode);
				variableZone.put(domain, newZone);
			}
		}

		// 2. for a specific domain, if the variables are original global,
		// then they belongs to the same zone.

		List<ColorZone> newVariableZones = new ArrayList<ColorZone>();
		List<ColorZone> valueZones = new ArrayList<ColorZone>();

		for (Set<Object> domain : variableZone.keySet()) {
			ColorZone zone = variableZone.get(domain); // zone already assigned
														// to the variables in
														// the domain.

			Map<String, ColorZone> newVariableZone = new HashMap<String, ColorZone>();

			// for each variables in the zone.
			for (AbstractNode varNode : zone.getNodes()) {
				VariableNode variableNode = (VariableNode) varNode;

				String isGlobal = variableNode.getVariable()
						.getAnnotationValue(Constants.GLOBAL);

				// if it is a local variable.
				if (!Boolean.parseBoolean(isGlobal)) {
					String sumnum = variableNode.getVariable()
							.getAnnotationValue(Constants.SUMNUM);

					addToMap(newVariableZone, sumnum, variableNode);
				} else {
					// is global variable
					// determine whether current variable is last version.
					if (variableNode.getVariable().getName()
							.contains(Constants.LASTVERSION)) {
						addToMap(newVariableZone, Constants.LASTVERSION,
								variableNode);
					} else {
						addToMap(newVariableZone, Constants.GLOBAL,
								variableNode);
					}
				}
			}
			newVariableZones.addAll(newVariableZone.values());

			// for each variable in the same color zone, add all of their value
			// nodes to the same color zone respectively.
			for (ColorZone _zone : newVariableZone.values()) {
				ColorZone newZone = new ColorZone();
				for (AbstractNode variable : _zone.getNodes()) {
					for (ValueNode value : ((VariableNode) variable)
							.getValueNodes()) {
						newZone.addNode(value);
					}
				}

				if (newZone.getNodes().size() > 0) {
					valueZones.add(newZone);
				}
			}
		}

		List<ColorZone> returnZones = new ArrayList<ColorZone>();
		returnZones.addAll(newVariableZones);
		returnZones.addAll(valueZones);

		return returnZones;
	}

	/**
	 * add node with specific key value to map.
	 * 
	 * @param map
	 * @param value
	 * @param node
	 */
	private void addToMap(Map<String, ColorZone> map, String value,
			VariableNode node) {
		if (map.containsKey(value)) {
			map.get(value).addNode(node);
		} else {
			ColorZone newZone = new ColorZone();
			newZone.addNode(node);
			map.put(value, newZone);
		}
	}
}
