package css.symmetrygraph.color;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import css.expr.IExpression;
import css.symmetrygraph.SymmetryConstraintGraph;
import css.symmetrygraph.node.AbstractNode;
import css.symmetrygraph.node.AssignmentNode;
import css.symmetrygraph.node.ConstraintNode;
import css.symmetrygraph.node.ValueNode;
import css.symmetrygraph.node.VariableNode;
import css.value.IValue;
import css.value.IValue.ValueType;
import css.variable.IScalarVariable;


public class ShaojieColorPartitioningStrategy extends
    DefaultColorPartitioningStrategy {

  @Override
  public List<ColorZone> buildZones(SymmetryConstraintGraph graph) {
    List<ColorZone> zones = new ArrayList<ColorZone>();
    zones.addAll(buildVariableColorZones(graph));
    // zones.addAll(buildValueColorZones(graph));
    zones.addAll(buildConstaintColorZones(graph));
    return zones;
  }

  @Override
  protected Collection<ColorZone> buildVariableColorZones(
      SymmetryConstraintGraph graph) {
    Map<Set<Object>, ColorZone> variableZone = new HashMap<Set<Object>, ColorZone>();
    for (VariableNode varNode : graph.getVariableNodes()) {
      Set<Object> domain = varNode.getVariable().getDomain();
      if (variableZone.containsKey(domain)) {
        variableZone.get(domain).addNode(varNode);
      } else {
        ColorZone newZone = new ColorZone();
        newZone.addNode(varNode);
        variableZone.put(domain, newZone);
      }
    }

    List<ColorZone> newVariableZones = new ArrayList<ColorZone>();
    List<ColorZone> valueZones = new ArrayList<ColorZone>();

    for (Set<Object> domain : variableZone.keySet()) {
      ColorZone zone = variableZone.get(domain);
      Map<String, ColorZone> newVariableZone = new HashMap<String, ColorZone>();
      for (AbstractNode varNode : zone.getNodes()) {
        VariableNode variableNode = (VariableNode) varNode;
        
        String annotationValue = variableNode.getVariable().getAnnotationValue("global");
        if(newVariableZone.containsKey(annotationValue)) {
          newVariableZone.get(annotationValue).addNode(varNode);
        }
        else {
          ColorZone newZone = new ColorZone();
          newZone.addNode(varNode);
          newVariableZone.put(annotationValue, newZone);
        }
      }
      
      newVariableZones.addAll(newVariableZone.values());
      
     
      
      for(ColorZone _zone: newVariableZone.values()) {
        ColorZone newZone = new ColorZone();
        for(AbstractNode variable: _zone.getNodes()) {
          for (ValueNode value : ((VariableNode)variable).getValueNodes()) {
            newZone.addNode(value);
          }
        }
        
        if (newZone.getNodes().size() > 0){
          valueZones.add(newZone);
        }              
      }

    }

    // for (Set<Object> domain : variableZone.keySet()) {
    // ColorZone zone = variableZone.get(domain);
    // ColorZone globalZone = new ColorZone();
    // ColorZone localZone = new ColorZone();
    // ColorZone finalZone = new ColorZone();
    // for (AbstractNode varNode : zone.getNodes()) {
    // VariableNode variableNode = (VariableNode) varNode;
    // IScalarVariable var = variableNode.getVariable();
    // if (var.getAnnotationValue("global").equals("true")) {
    // globalZone.addNode(variableNode);
    // } else if (var.getAnnotationValue("global").equals("final")) {
    //
    // finalZone.addNode(variableNode);
    // } else {
    // localZone.addNode(variableNode);
    // }
    // }
    //
    // if (globalZone.getNodes().size() > 0) {
    // newVariableZones.add(globalZone);
    // }
    //
    // if (finalZone.getNodes().size() > 0) {
    // newVariableZones.add(finalZone);
    // }
    //
    // if (localZone.getNodes().size() > 0) {
    // newVariableZones.add(localZone);
    // }
    //
    // }
    //
    // List<ColorZone> valueZones = new ArrayList<ColorZone>();
    // ColorZone trueZone = new ColorZone();
    // ColorZone falseZone = new ColorZone();
    // for (Set<Object> domain : variableZone.keySet()) {
    // List<ColorZone> intGlobal = new ArrayList<ColorZone>();
    //
    // for (int i = 0; i < domain.size(); i++) {
    // intGlobal.add(new ColorZone());
    // }
    //
    // ColorZone intLocal = new ColorZone();
    //
    // ColorZone intFinal = new ColorZone();
    //
    // ColorZone zone = variableZone.get(domain);
    // for (AbstractNode varNode : zone.getNodes()) {
    // VariableNode variableNode = (VariableNode) varNode;
    // IScalarVariable var = variableNode.getVariable();
    //
    // if (var.getType().equals(IValue.ValueType.Bool)) {
    // for (ValueNode value : variableNode.getValueNodes()) {
    // if (value.getValue().asBoolean()) {
    // trueZone.addNode(value);
    //
    // } else {
    // falseZone.addNode(value);
    //
    // }
    //
    // }
    //
    // } else {
    //
    // if (var.getAnnotationValue("global").equals("true")) {
    // for (int i = 0; i < variableNode.getValueNodes().size(); i++) {
    // // ad-hoc
    // intGlobal.get(0).addNode(variableNode.getValueNodes().get(i));
    // }
    //
    // }else if (var.getAnnotationValue("global").equals("final")) {
    // for (ValueNode valueNode : variableNode.getValueNodes()) {
    // intFinal.addNode(valueNode);
    // }
    // }
    //
    //
    // else {
    // for (ValueNode valueNode : variableNode.getValueNodes()) {
    // intLocal.addNode(valueNode);
    // }
    // }
    //
    // }
    //
    // }
    //
    // for (int i = 0; i < intGlobal.size(); i++) {
    // ColorZone _zone = intGlobal.get(i);
    // if (_zone.getNodes().size() > 0) {
    // valueZones.add(_zone);
    // }
    // }
    //
    // if (intLocal.getNodes().size() > 0) {
    // valueZones.add(intLocal);
    // }
    //
    // if (intFinal.getNodes().size() > 0) {
    // valueZones.add(intFinal);
    // }
    //
    // }
    //
    // if (trueZone.getNodes().size() > 0) {
    // valueZones.add(trueZone);
    // }
    //
    // if (falseZone.getNodes().size() > 0) {
    // valueZones.add(falseZone);
    // }

    List<ColorZone> returnZones = new ArrayList<ColorZone>();
     returnZones.addAll(newVariableZones);
     returnZones.addAll(valueZones);

    return returnZones;
  }

  @Override
  protected Collection<? extends ColorZone> buildValueColorZones(
      SymmetryConstraintGraph graph) {

    List<ColorZone> zones = new ArrayList<ColorZone>();
    ColorZone trueZone = new ColorZone();
    ColorZone falseZone = new ColorZone();
    List<ColorZone> otherZones = new ArrayList<ColorZone>();

    List<VariableNode> globals = new ArrayList<VariableNode>();
    List<VariableNode> bools = new ArrayList<VariableNode>();
    List<VariableNode> others = new ArrayList<VariableNode>();

    for (VariableNode varNode : graph.getVariableNodes()) {
      IScalarVariable variable = varNode.getVariable();
      if (variable.getType().equals(IValue.ValueType.Bool)) {
        bools.add(varNode);
        continue;

      }

      if (variable.getAnnotationValue("global").equals("true")) {
        globals.add(varNode);
      } else {
        others.add(varNode);
      }

    }

    for (VariableNode variable : bools) {
      for (ValueNode value : variable.getValueNodes()) {
        if (value.getValue().asBoolean()) {
          trueZone.addNode(value);

        } else {
          falseZone.addNode(value);

        }

      }
      
    }

    if (trueZone.getNodes().size() > 0) {
      zones.add(trueZone);
    }

    if (falseZone.getNodes().size() > 0) {
      zones.add(falseZone);
    }

    // List<List<VariableNode>> otherVariablePartitons = new
    // ArrayList<List<VariableNode>>();

    int different = 0;
    for (int i = 0; i < others.size(); i = different) {
      // List<VariableNode> newPartition = new ArrayList<VariableNode>();
      final VariableNode firstVariable = others.get(i);
      // newPartition.add(firstVariable);
      ColorZone newZone = new ColorZone();
      for (ValueNode value : firstVariable.getValueNodes()) {
        newZone.addNode(value);
      }

      for (int j = i + 1; j < others.size(); j++) {
        VariableNode secondVariable = others.get(j);
        if (secondVariable.getVariable().getDomain()
            .equals(firstVariable.getVariable().getDomain())) {
          for (ValueNode value : secondVariable.getValueNodes()) {
            newZone.addNode(value);
          }

        } else {
          boolean hasFound = false;
          for (ColorZone zone : otherZones) {
            if (zone.getNodes().contains(secondVariable.getValueNodes().get(0))) {
              hasFound = true;
              break;
            }
          }

          if (!hasFound && different == i) {
            different = j;
          }

        }
      }

      otherZones.add(newZone);

      if (different == i) {
        break;
      }
    }

    zones.addAll(otherZones);

    Map<Integer, ColorZone> int2ColorZoneMap = new HashMap<Integer, ColorZone>();
    for (VariableNode variable : globals) {
      for (ValueNode valueNode : variable.getValueNodes()) {
        final IValue value = valueNode.getValue();
        assert (value.getType() == ValueType.Int);
        final Integer intValue = value.asInt();
        ColorZone targetZone = int2ColorZoneMap.get(intValue);
        if (targetZone == null) {
          targetZone = new ColorZone();
          int2ColorZoneMap.put(intValue, targetZone);
        }
        targetZone.addNode(valueNode);
      }
    }

    if (int2ColorZoneMap.size() > 0) {
      zones.addAll(int2ColorZoneMap.values());
    }

    return zones;

  }

  @Override
  protected Collection<? extends ColorZone> buildConstaintColorZones(
      SymmetryConstraintGraph graph) {

    List<ColorZone> zones = new ArrayList<ColorZone>();

    for (ConstraintNode consNode : graph.getConstraintNodes()) {
      IExpression exp = consNode.getConstraint().getExpression();

      boolean hasFound = false;
      for (ColorZone zone : zones) {

        if (zone.getUnfinalizedNodes().size() > 0) {
          ConstraintNode con = (ConstraintNode) zone.getUnfinalizedNodes()
              .iterator().next();

          if (exp.shaojieEquality(con.getConstraint().getExpression())) {
            zone.addNode(consNode);

            hasFound = true;
          }
        }

      }

      if (!hasFound) {
        ColorZone zone = new ColorZone();
        zones.add(zone);
        zone.addNode(consNode);
      }

    }

    List<ColorZone> assignZones = new ArrayList<ColorZone>();
    for (ColorZone zone : zones) {
      ColorZone assignmentZone = new ColorZone();
      for (ConstraintNode consNode : graph.getConstraintNodes()) {
        if (zone.getNodes().contains(consNode)) {
          for (AssignmentNode assignNode : consNode.getAssignments()) {
            assignmentZone.addNode(assignNode);
          }
        }
      }

      assignZones.add(assignmentZone);
    }

    zones.addAll(assignZones);
    List<ColorZone> returnZones = new ArrayList<ColorZone>();
    for(ColorZone zone : zones) {
      if (zone.getNodes().size() > 0) {
        returnZones.add(zone);
      }
      
    }
    
    return returnZones;
  }
}
