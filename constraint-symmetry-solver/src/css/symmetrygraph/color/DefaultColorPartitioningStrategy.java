package css.symmetrygraph.color;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import css.symmetrygraph.SymmetryConstraintGraph;
import css.symmetrygraph.node.AssignmentNode;
import css.symmetrygraph.node.ConstraintNode;
import css.symmetrygraph.node.ValueNode;
import css.symmetrygraph.node.VariableNode;
import css.value.IValue;

public class DefaultColorPartitioningStrategy implements
		IColorPartitioningStrategy {

	@Override
	public List<ColorZone> buildZones(SymmetryConstraintGraph graph) {
		List<ColorZone> zones = new ArrayList<ColorZone>();
		zones.addAll(buildVariableColorZones(graph));
		zones.addAll(buildValueColorZones(graph));
		zones.addAll(buildConstaintColorZones(graph));
		zones.addAll(buildAssignmentColorZones(graph));
		return zones;
	}

	protected Collection<? extends ColorZone> buildAssignmentColorZones(
			SymmetryConstraintGraph graph) {
		List<ColorZone> zones = new ArrayList<ColorZone>();
		for (ConstraintNode consNode : graph.getConstraintNodes()) {
			ColorZone zone = new ColorZone();
			zones.add(zone);
			for (AssignmentNode assignNode : consNode.getAssignments()) {
				zone.addNode(assignNode);
			}
		}
		return zones;
	}

	protected Collection<? extends ColorZone> buildConstaintColorZones(
			SymmetryConstraintGraph graph) {
		List<ColorZone> zones = new ArrayList<ColorZone>();
		for (ConstraintNode consNode : graph.getConstraintNodes()) {
			ColorZone zone = new ColorZone();
			zones.add(zone);
			zone.addNode(consNode);
		}
		return zones;
	}

	protected Collection<? extends ColorZone> buildValueColorZones(
			SymmetryConstraintGraph graph) {
		List<ColorZone> zones = new ArrayList<ColorZone>();
		ColorZone trueZone = new ColorZone();
		ColorZone falseZone = new ColorZone();
		ColorZone intZone = new ColorZone();

		// for (VariableNode varNode : graph.getVariableNodes()) {
		// if (varNode.getVariable().getType().equals(IValue.ValueType.Bool)) {
		// ColorZone zone = new ColorZone();
		// zones.add(zone);
		// zone.addNode(valueNode);
		// }
		// } else {
		// ColorZone zone = new ColorZone();
		// zones.add(zone);
		// for (ValueNode valueNode : varNode.getValueNodes()) {
		// zone.addNode(valueNode);
		// }
		// }
		// }

		for (VariableNode varNode : graph.getVariableNodes()) {
			System.out.println(varNode.getVariable());
			// for boolean variable
			if (varNode.getVariable().getType().equals(IValue.ValueType.Bool)) {
				for (ValueNode valueNode : varNode.getValueNodes()) {
					if (valueNode.getValue().asBoolean()) {
						trueZone.addNode(valueNode);
					} else {
						falseZone.addNode(valueNode);
					}
				}

			} else {
				for (ValueNode valueNode : varNode.getValueNodes()) {
					intZone.addNode(valueNode);
				}
			}

		}
		if (trueZone.getNodes().size() > 0) {
			zones.add(trueZone);
		}
		if (falseZone.getNodes().size() > 0) {
			zones.add(falseZone);
		}
		zones.add(intZone);

		return zones;
	}

	protected Collection<ColorZone> buildVariableColorZones(
			SymmetryConstraintGraph graph) {
		Map<Set<Object>, ColorZone> variableZone = new HashMap<Set<Object>, ColorZone>();
		for (VariableNode varNode : graph.getVariableNodes()) {
			Set<Object> domain = varNode.getVariable().getDomain();
			if (variableZone.containsKey(domain)) {
				variableZone.get(domain).addNode(varNode);
			} else {
				ColorZone newZone = new ColorZone();
				newZone.addNode(varNode);
				variableZone.put(domain, newZone);
			}
		}
		return variableZone.values();
	}
}
