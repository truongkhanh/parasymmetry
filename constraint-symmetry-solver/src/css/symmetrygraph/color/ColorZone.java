package css.symmetrygraph.color;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import css.symmetrygraph.node.AbstractNode;

public class ColorZone {

	private static int colorSequence;

	private final int color;

	private Set<AbstractNode> nodes;

	private List<AbstractNode> nodeList;

	public ColorZone() {
		super();
		this.color = colorSequence++;
		this.nodes = new HashSet<AbstractNode>();
	}

	public void addNode(AbstractNode node) {
		if (nodes == null) {
			throw new RuntimeException(
					"After calling getNodes(), you can never call this method again. "
							+ "I deem that you have done the zone construction work. ");
		}
		this.nodes.add(node);
	}

	public int getColor() {
		return color;
	}

	public Set<AbstractNode> getUnfinalizedNodes() {
		return nodes;
	}

	public List<AbstractNode> getNodes() {
		if (nodeList == null) {
			this.nodeList = new ArrayList<AbstractNode>(this.nodes);
			this.nodes = null;
		}
		return this.nodeList;
	}

	public AbstractNode getFirstNode() {
		if (this.nodeList.size() == 0)
			throw new RuntimeException("Empty color zone.");
		return this.nodeList.get(0);
	}
}
