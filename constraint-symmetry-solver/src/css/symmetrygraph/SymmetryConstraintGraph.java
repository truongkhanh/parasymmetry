package css.symmetrygraph;

import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import css.constraint.Constraint;
import css.core.CPLProgram;
import css.symmetrygraph.color.ColorPartitioner;
import css.symmetrygraph.color.ColorZone;
import css.symmetrygraph.color.IColorPartitioner;
import css.symmetrygraph.color.IColorPartitioningStrategy;
import css.symmetrygraph.node.AbstractNode;
import css.symmetrygraph.node.AssignmentNode;
import css.symmetrygraph.node.ConstraintNode;
import css.symmetrygraph.node.Edge;
import css.symmetrygraph.node.ValueNode;
import css.symmetrygraph.node.VariableNode;
import css.value.IValue;
import css.variable.IScalarVariable;

public class SymmetryConstraintGraph {

	private final Set<AssignmentNode> assignmentNodes;

	private final Set<ConstraintNode> constraintNodes;

	private final Set<VariableNode> variableNodes;

	private final Set<ValueNode> valueNodes;

	private final int numberOfVerticesInSaucyGraph;

	private final Set<Edge> edges;

	private final IColorPartitioner partitioner;

	public SymmetryConstraintGraph(CPLProgram program,
			IColorPartitioningStrategy coloringStrategy) {
		this.assignmentNodes = new HashSet<AssignmentNode>();
		this.constraintNodes = new HashSet<ConstraintNode>();
		this.variableNodes = new HashSet<VariableNode>();
		this.valueNodes = new HashSet<ValueNode>();
		this.edges = new HashSet<Edge>();

		this.build(program);

		this.partitioner = new ColorPartitioner(this, coloringStrategy);
		this.numberOfVerticesInSaucyGraph = this
				.assingIdForColorZones(this.partitioner);
	}

	private int assingIdForColorZones(IColorPartitioner partitioner) {
		int sequence = 0;
		for (ColorZone zone : partitioner.getZones()) {
			for (AbstractNode node : zone.getNodes()) {
				node.setIdForColor(++sequence);
			}
		}
		return sequence + 1;
	}

	public static void adhoc_outputSaucyGraph(Writer writer) throws IOException {
		int numberOfEdge = 10;

		int numberOfNodePlus1 = 12;

		writer.write(Integer.toString(numberOfNodePlus1));
		writer.write(' ');
		writer.write(Integer.toString(numberOfEdge));
		writer.write(' ');
		writer.write(Integer.toString(1));
		writer.write(' ');
		writer.append('\n');

		for (int i = 1; i < numberOfNodePlus1 - 1; i++) {
			if (2 * i <= numberOfNodePlus1 - 1) {
				writer.write(Integer.toString(i));
				writer.write(' ');
				writer.write(Integer.toString(2 * i));
				writer.write(' ');
				writer.write('\n');
			}

			if (2 * i + 1 <= numberOfNodePlus1 - 1) {
				writer.write(Integer.toString(i));
				writer.write(' ');
				writer.write(Integer.toString(2 * i + 1));
				writer.write(' ');
				writer.write('\n');
			}

		}

	}

	public void outputSaucyGraph(Writer writer) throws IOException {
		// StringBuilder builder = new StringBuilder();
		// builder.append(this.numberOfVertices()).append(' ')
		// .append(this.edges.size()).append(' ')
		// .append(this.partitioner.numberOfZones()).append(' ');

		writer.write(Integer.toString(this.numberOfVerticesInSaucyGraph));
		writer.write(' ');
		writer.write(Integer.toString(this.edges.size()));
		writer.write(' ');
		writer.write(Integer.toString(this.partitioner.numberOfZones()));
		writer.write(' ');

		List<ColorZone> zones = this.partitioner.getZones();
		for (int i = 1; i < zones.size(); ++i) {
			ColorZone zone = zones.get(i);
			// builder.append(zone.getFirstNode().getIdForColor()).append(' ');
			writer.write(Integer.toString(zone.getFirstNode().getIdForColor()));
			writer.write(' ');
		}
		writer.append('\n');
		for (Edge edge : this.edges) {
			// builder.append(edge.getNode1().getIdForColor()).append(' ')
			// .append(edge.getNode2().getIdForColor()).append('\n');
			writer.write(Integer.toString(edge.getNode1().getIdForColor()));
			writer.write(' ');
			writer.write(Integer.toString(edge.getNode2().getIdForColor()));
			writer.write(' ');
			writer.write('\n');
		}
		// return builder.toString();
	}

	// private int numberOfVertices() {
	// return this.variableNodes.size() + this.valueNodes.size()
	// + this.constraintNodes.size() + this.assignmentNodes.size();
	// }

	public Set<ConstraintNode> getConstraintNodes() {
		return Collections.unmodifiableSet(this.constraintNodes);
	}

	public Set<VariableNode> getVariableNodes() {
		return Collections.unmodifiableSet(this.variableNodes);
	}

	private void addEdge(AbstractNode node1, AbstractNode node2) {
		this.edges.add(new Edge(node1, node2));
	}

	public void build(CPLProgram program) {
		Map<IValue, ValueNode> valueNodeMap = new HashMap<IValue, ValueNode>();

		// build variable and value nodes.
		for (IScalarVariable var : program.getVariables()) {
			VariableNode node = new VariableNode(var);
			this.variableNodes.add(node);
			this.valueNodes.addAll(node.getValueNodes());

			for (ValueNode valueNode : node.getValueNodes()) {
				valueNodeMap.put(valueNode.getValue(), valueNode);
				// variable node -- value node.
				this.addEdge(node, valueNode);
			}
		}

		// build constraint and assignment nodes.
		for (Constraint cons : program.getConstraints()) {
			ConstraintNode node = new ConstraintNode(cons, valueNodeMap);

			// current constraint comes from the guard of a summand!
			boolean isGuard = (!cons.getAnnotations().isEmpty());

			this.constraintNodes.add(node);
			this.assignmentNodes.addAll(node.getAssignments());
			for (AssignmentNode assignNode : node.getAssignments()) {
				// add an edge between constraint node and all assignment nodes
				// which satisfy the constraint.
				this.addEdge(node, assignNode);
				for (ValueNode valueNode : assignNode.getValueNodes()) {
					// add an edge between assignment node and all value nodes which combine the assignment.
					this.addEdge(assignNode, valueNode);

					if (isGuard) {
						valueNode.setNotUseless(true);
						// valueNode.setExistGuardInit(true);
					}
					// this value node is involving in an assignment, it's not isolated!
					valueNode.setNotIsolated(true);
				}
			}
		}

		// simplify
		// TODO: mechanism used?!
		Set<VariableNode> toBeRemovedVariables = new HashSet<VariableNode>();
		Set<ValueNode> toBeRemovedValues = new HashSet<ValueNode>();
		Set<ValueNode> uselessValues = new HashSet<ValueNode>();
		for (VariableNode node : this.variableNodes) {

			boolean allIsolatedVariables = true;
			boolean appearedInGuard = false;
			Set<ValueNode> removedValues = new HashSet<ValueNode>();
			Set<ValueNode> uselessValues2 = new HashSet<ValueNode>();
			for (ValueNode valueNode : node.getValueNodes()) {
				if (valueNode.isNotIsolated()) {
					allIsolatedVariables = false;

				} else {
					toBeRemovedValues.add(valueNode);
					removedValues.add(valueNode);
					this.edges.remove(new Edge(node, valueNode));
				}

				if (valueNode.isNotUseless()) {
					appearedInGuard = true;
				} else {
					uselessValues2.add(valueNode);
				}
			}

			if (appearedInGuard) {
				for (ValueNode valueNode : uselessValues2) {
					node.getValueNodes().remove(valueNode);
					this.edges.remove(new Edge(node, valueNode));
				}
				uselessValues.addAll(uselessValues2);
			}

			if (removedValues.size() > 0) {
				node.getValueNodes().removeAll(removedValues);
			}

			if (allIsolatedVariables) {
				toBeRemovedVariables.add(node);
			}

		}

		for (AssignmentNode assignmentNode : this.assignmentNodes) {
			for (ValueNode valueNode : uselessValues) {
				if (this.edges.contains(new Edge(assignmentNode, valueNode))) {
					for (ValueNode value : assignmentNode.getValueNodes()) {
						this.edges.remove(new Edge(assignmentNode, value));

					}
					assignmentNode.getValueNodes().clear();
					break;

				}

			}

			if (assignmentNode.getValueNodes().isEmpty()) {
				ConstraintNode constraintNode = assignmentNode
						.getConstraintNode();
				constraintNode.getAssignments().remove(assignmentNode);
				this.edges.remove(new Edge(constraintNode, assignmentNode));
			}

		}

		// for(ConstraintNode consNode: this.constraintNodes) {
		// System.out.println("constraint: " + consNode.toString());
		// for (AssignmentNode assignnode : consNode.getAssignments()) {
		// System.out.println("assignment: " + assignnode.toString());
		// }
		//
		// }

		this.variableNodes.removeAll(toBeRemovedVariables);
		this.valueNodes.removeAll(toBeRemovedValues);
	}

	private void toString(AbstractNode node, Writer builder) throws IOException {
		builder.append(node.toString()).append("[color=")
				.append(Integer.toString(this.partitioner.getColor(node)))
				.append("]").append('\n');
	}

	public String save(Writer builder) throws IOException {
		// StringBuilder builder = new StringBuilder();
		builder.append("=== Vertices ===\n");

		// builder.append("Variable Nodes: \n");
		for (VariableNode node : sortVariables(this.variableNodes)) {
			toString(node, builder);
			for (ValueNode valueNode : node.getValueNodes()) {
				builder.append('\t');
				toString(valueNode, builder);
			}
		}
		builder.append('\n');

		// builder.append("Constraint Nodes: \n");
		for (ConstraintNode consNode : sortConstraints(this.constraintNodes)) {
			toString(consNode, builder);
			for (AssignmentNode assignnode : consNode.getAssignments()) {
				builder.append('\t');
				toString(assignnode, builder);
				for (ValueNode valueNode : assignnode.getValueNodes()) {
					builder.append("\t\t");
					toString(valueNode, builder);
				}
			}
		}

		// builder.append("Assignment Nodes:\n");
		// for (AbstractNode node : this.assignmentNodes) {
		// builder.append(node.toString()).append('\n');
		// }
		return builder.toString();
	}

	private static final Comparator<AbstractNode> ColoredNodeComparator = new Comparator<AbstractNode>() {
		@Override
		public int compare(AbstractNode o1, AbstractNode o2) {
			return o1.getIdForColor() - o2.getIdForColor();
		}
	};

	private ConstraintNode[] sortConstraints(
			Set<ConstraintNode> constraintNodes2) {
		ConstraintNode[] nodes = new ConstraintNode[constraintNodes2.size()];
		constraintNodes2.toArray(nodes);
		Arrays.sort(nodes, ColoredNodeComparator);
		return nodes;
	}

	private VariableNode[] sortVariables(Set<VariableNode> variableNodes2) {
		VariableNode[] nodes = variableNodes2
				.toArray(new VariableNode[variableNodes2.size()]);
		Arrays.sort(nodes, ColoredNodeComparator);
		return nodes;
	}
}
