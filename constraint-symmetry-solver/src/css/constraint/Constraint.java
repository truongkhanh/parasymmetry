package css.constraint;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import css.assignment.Assignment;
import css.exceptions.EvaluationException;
import css.exceptions.TypeCheckingFailureException;
import css.expr.IExpression;
import css.value.IValue;
import css.variable.IScalarVariable;


public class Constraint {

  private final IExpression expression;

  // @Deprecated
  // private final List<IScalarVariable> variables;
  private final List<Assignment> satisfiableAssignments;

  private final Map<String, String> annotations;

  public Constraint(Map<String, String> annotations, IExpression expression)
      throws TypeCheckingFailureException {
    super();
    this.expression = expression; 
    
    if (annotations != null) {
      this.annotations = Collections.unmodifiableMap(new HashMap<String, String>(
          annotations));
    }
    else {
      this.annotations = null;
    }
    
    
    this.typeCheck();

    Map<IScalarVariable, List<IExpression>> variableCollector = new HashMap<IScalarVariable, List<IExpression>>();
    expression.collectVariables(variableCollector);
    // this.variables = Collections.unmodifiableList(new
    // ArrayList<IScalarVariable>(
    // variableCollector));
    this.satisfiableAssignments = Collections.unmodifiableList(this
        .testAllAssignments(variableCollector));
  }

  public Map<String, String> getAnnotations() {
    return annotations;
  }

  /*
   * private void TreeSearching() { List<IScalarVariable> variables = new
   * ArrayList<IScalarVariable>(); variables.addAll(variables);
   * Map<IScalarVariable, IValue> solution = new HashMap<IScalarVariable,
   * IValue>(); Search_BT(variables, solution) ;
   * 
   * }
   * 
   * private void Search_BT(List<IScalarVariable> variables,
   * Map<IScalarVariable, IValue> solution) {
   * 
   * for(IScalarVariable var: variables) { for (int i = 0; i <
   * var.sizeOfDomain(); ++i) { IValue value = var.getValue(i); if
   * (Consistent(var, value, solution)) { solution.put(var, value); if
   * (variables.size() == 1) { this.satisfiableAssignments.add(new
   * Assignment(solution));
   * 
   * } else { List<IScalarVariable> futureVariables = new
   * ArrayList<IScalarVariable>(variables); futureVariables.remove(var);
   * Search_BT(futureVariables, solution); }
   * 
   * } }
   * 
   * }
   * 
   * }
   */
  // modified by shaojie
  private List<? extends Assignment> testAllAssignments(
      Map<IScalarVariable, List<IExpression>> variables) {
    List<Assignment> satisfiable = Assignment.createAllAssignments(variables,
        this);
    String textualConstraint = this.toString();
    System.out.println("In constraint [" + textualConstraint
        + "], #satisfiable assignments = " + satisfiable.size());
    return satisfiable;
  }

  // private List<? extends Assignment> testAllAssignments() {
  // List<Assignment> assignments = Assignment.createAllAssignments(this
  // .getVariables());
  // String textualConstraint = this.toString();
  // System.out.println("In constraint [" + textualConstraint
  // + "], #assginments = " + assignments.size());
  // List<Assignment> satisfiable = new ArrayList<Assignment>();
  // for (Assignment assign : assignments) {
  // if (this.testSatisfaction(assign))
  // satisfiable.add(assign);
  // }
  // System.out.println("In constraint [" + textualConstraint
  // + "], #satisfiable assignments = " + satisfiable.size());
  // return satisfiable;
  // }

  public List<Assignment> getSatisfiableAssignments() {
    return satisfiableAssignments;
  }

  // public List<IScalarVariable> getVariables() {
  // return variables;
  // }

  public void typeCheck() throws TypeCheckingFailureException {
    if (expression.getType().equals(IValue.ValueType.Bool)) {
      this.expression.typeCheck();
      return;
    } else
      throw new TypeCheckingFailureException(
          "The root expression of a constraint must be of type Bool.");
  }

  public boolean testSatisfaction(Assignment assignment) {
    try {
      if (this.expression.evaluate(assignment).asBoolean()) {
        // assignment.ClearUselessVariables();
        return true;
      } else {
        return false;
      }
    } catch (EvaluationException e) {
      e.printStackTrace();
      return false;
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    builder.append(expression.toString());
    
    if (this.annotations.size() > 0) {
      builder.append(" @(");
      Iterator<Map.Entry<String, String>> annotationIterator = this.annotations
          .entrySet().iterator();
      if (annotationIterator.hasNext()) {
        Map.Entry<String, String> firstElement = annotationIterator.next();
        builder.append(firstElement.getKey()).append('=')
            .append(firstElement.getValue());
      }
      while (annotationIterator.hasNext()) {
        Map.Entry<String, String> element = annotationIterator.next();
        builder.append(", ").append(element.getKey()).append('=')
            .append(element.getValue());
      }
      builder.append(')');
    }
    
    return builder.toString();
  }

  public IExpression getExpression() {
    return expression;
  }

}
