grammar CPL;

options {
	language=Java;
	backtrack=true;
}

tokens {
        PLUS = '+';
        MINUS = '-';
        TIMES = '*';
        DIVIDE = '/';
        MODULO = '%';
       
        
        EQUALITY = '==';
        NON_EQUALITY = '!=';
        
        AND = '&&';
        OR = '||';
        
        IMPLY = '->';
        
        XOR = '^';
        
        POWER = '^^';
        
        NOT = '!';
        
        GT = '>';
        LT = '<';
        GE = '>=';
        LE = '<=';
        
        DOM = 'dom';
        VAR = 'var';
        CONS = 'cons';
        
        FORALL = 'forall';
        EXIST = 'exists';
        IN = 'in';
        
        INT_TYPE = 'int';
        BOOL_TYPE = 'bool';
        
        TRUE = 'true';
        FALSE = 'false'; 
        
        WRITE = 'Write';        
}


@header{
package output;
import ast.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
}

@lexer::header {package output;}


@parser::members
{

        private final Map<String,  String> macroMap = new HashMap<String, String>();

        boolean isRightToLeft( int type )
        {
                return true;
        }
        int getOperatorPrecedence( int type )
        {
                switch ( type )
                {
                case POWER:
                   	 return 1;
                case TIMES:
                case DIVIDE:
                case MODULO:
                        return 2;
                case PLUS:
                case MINUS:
                        return 3;
                case GT:
                case LT:
                case GE:
                case LE:
                	return 4;
                case EQUALITY:
                case NON_EQUALITY:
                	return 5;
                case XOR:
                	return 6;                	
               case AND:
               case OR:
               	return 7;
               case IMPLY:
               	return 8;
                default:
                        throw new RuntimeException("Cannot reach here."); // really this shouldn't be hit
                }
        }
        int findPivot( List operators, int startIndex, int stopIndex )
        {
                int pivot = startIndex;
                int pivotRank = getOperatorPrecedence( ((Token)operators.get(pivot)).getType() );
                for ( int i = startIndex + 1; i <= stopIndex; i++ )
                {
                        int type = ((Token)operators.get(i)).getType();
                        int current = getOperatorPrecedence( type );
                        boolean rtl = isRightToLeft(type);
                        if ( current > pivotRank || (current == pivotRank && rtl) )
                        {
                                pivot = i;
                                pivotRank = current;
                        }
                }
                return pivot;
        }
        AbstractASTExpression createPrecedenceTree( List expressions, List operators, int startIndex, int stopIndex )
        {
                if ( stopIndex == startIndex )
                        return (AbstractASTExpression)expressions.get(startIndex);

                int pivot = findPivot( operators, startIndex, stopIndex - 1 );
                Token token = (Token)operators.get(pivot);
                AbstractASTExpression left = createPrecedenceTree( expressions, operators, startIndex, pivot);
                AbstractASTExpression right = createPrecedenceTree( expressions, operators, pivot + 1, stopIndex );
                ASTBinaryExpression root = new ASTBinaryExpression(token.getText(), left, right);
                return root;
        }
        AbstractASTExpression createPrecedenceTree( List expressions, List operators )
        {
                return createPrecedenceTree( expressions, operators, 0, expressions.size() - 1 );
        }
}



num_exp returns [ASTNumberExpression value]
	:	INT {$value = new ASTNumberExpression(Integer.parseInt($INT.text));};

var_exp	returns [AbstractASTExpression value]
@init {
	List<AbstractASTExpression> indices = new ArrayList<AbstractASTExpression>();
}
	:	ID ('[' exp ']' {indices.add($exp.value);})*
 	             {
 	             		if (indices.isEmpty()) {
 	             			$value = new ASTVariableExpression($ID.text);
 	             		} else {
 	             			$value = new ASTArrayIndexingExpression($ID.text, indices);
 	             		}
 	             	}
	;



enclose_exp returns [AbstractASTExpression value]
	:	'('! exp ')'! {$value = $exp.value;};

bool_exp returns [AbstractASTExpression value]	
	:	TRUE {$value = ASTBooleanExpression.TRUE;} 
	| 	FALSE {$value = ASTBooleanExpression.FALSE;}
	|	NOT atom_exp {$value = new ASTNotOperator($atom_exp.value);}
	;

forall_exp returns [ASTForallExpression value]	
	:
	FORALL varName=ID IN domName=ID '.' atom_exp
	{$value = new ASTForallExpression($varName.text, $domName.text, $atom_exp.value);}
	;

exist_exp returns [ASTExistentialExpression value]
	:
	EXIST varName=ID IN domName=ID '.' atom_exp
	{$value = new ASTExistentialExpression($varName.text, $domName.text, $atom_exp.value);}
	;		
/*
write_exp  returns[ASTWriteExpression value]
@init{
	List<AbstractASTExpression> indices = new ArrayList<AbstractASTExpression>();
}
	:
	WRITE var1Name=ID var2Name=ID atom_exp  ('[' exp ']' {indices.add($exp.value);})*
	{$value = new ASTWriteExpression($var1Name.text, $var2Name.text,  indices, $atom_exp.value); )
	}
	;
*/



atom_exp returns [AbstractASTExpression value]
	:
	(
		num_exp {$value = $num_exp.value;}
		| bool_exp {$value = $bool_exp.value;}
		| var_exp {$value = $var_exp.value;}
		| enclose_exp {$value = $enclose_exp.value;}
		| forall_exp {$value = $forall_exp.value;}
		| exist_exp {$value = $exist_exp.value;}
		//|write_exp {$value = $write_exp.value;}
	)
	;

// $>




exp returns [AbstractASTExpression value]
@init
{
	ArrayList<AbstractASTExpression> expressions = new ArrayList<AbstractASTExpression>();
	ArrayList<Token> operators = new ArrayList<Token>();
}
	: 
	left=atom_exp 
		{expressions.add($left.value);}
	(operator right=atom_exp {operators.add($operator.start); expressions.add($right.value);})*
	{$value = createPrecedenceTree(expressions, operators);}
	;

var_type returns [String value]
	: INT_TYPE {$value = $INT_TYPE.text;} | BOOL_TYPE {$value = $BOOL_TYPE.text;}
	;
	


domain_element returns [String value]
	: TRUE {$value = $TRUE.text;}
	| FALSE {$value = $FALSE.text;}
	| INT {$value = $INT.text;}
	| identifier_or_constant {$value = $identifier_or_constant.text;}
	;

var_def [List<ASTDomDefinition> doms] returns [AbstractASTVarDefinition value]
@init
{
	ArrayList<String> domainList = new ArrayList<String>();
	ArrayList<Integer> dimensions = new ArrayList<Integer>();
	Map<String, String> annotations = new HashMap<String, String>();
}
	:
	('@' '(' (annotationName=ID '=' STRING ','? {annotations.put($annotationName.text, $STRING.text.substring(1, $STRING.text.length() - 1));})*  ')')?
	var_type name=ID 
	('[' dims=identifier_or_constant ']'  {dimensions.add(Integer.parseInt($dims.text));})*
	(
		'{' (domain_element {domainList.add($domain_element.value);})+ '}'
		|
		DOM domName=ID
		{
		for (ASTDomDefinition dom: doms) {
		    if (dom.getName().equals($domName.text)) {
		        domainList.addAll(dom.getDomain());	
		        break;
		    }
		}
		}
	)
	{
		if (dimensions.isEmpty())
			$value = new ASTScalarVarDefinition($var_type.value, $name.text, domainList, annotations);
		else
			$value = new ASTArrayDefinition($var_type.value, $name.text, domainList, dimensions, annotations);
	}

	;	



dom_def returns [ASTDomDefinition value] 
@init
{
	ArrayList<String> domainList = new ArrayList<String>();
}
	:
 	var_type ID
 	 '{' 
 	 	first=domain_element {domainList.add($first.value);}
 	 	(
 	 		'..' right=domain_element
 	 		{
 	 			final int rightBound = Integer.parseInt($right.value);
 	 			final int leftBound = Integer.parseInt($first.value);
 	 			for (int i = leftBound + 1; i <= rightBound; ++i) {
 	 				domainList.add(String.valueOf(i));
 	 			}
 	 		}
	 	 	| (rest=domain_element {domainList.add($rest.value);})*
	 	 )
 	 	
 	 
 	 '}'
	{$value = new ASTDomDefinition($var_type.value, $ID.text, domainList);}
	;

constraint returns [ASTConstraint value]
@init
{
	Map<String, String> annotations = new HashMap<String, String>();
}
	:
	('@' '(' (annotationName=ID '=' STRING ','? {annotations.put($annotationName.text, $STRING.text.substring(1, $STRING.text.length() - 1));})*  ')')?
	exp
	{$value = new ASTConstraint(annotations, $exp.value);}
	;

program returns [ASTProgram value]
@init
{
	ArrayList<ASTDomDefinition> domDefinitions = new ArrayList<ASTDomDefinition>();
	ArrayList<AbstractASTVarDefinition> varDefinitions = new ArrayList<AbstractASTVarDefinition>();
	List<ASTConstraint> constraints = new ArrayList<ASTConstraint>();	
	
	String tempValue = null;
}
	:

	('#define' key=ID (macroValue=ID{tempValue = $macroValue.text;}| INT{tempValue = $INT.text;}) {macroMap.put($key.text, tempValue);})*
	(DOM ':'  (dom_def ';'? {domDefinitions.add($dom_def.value);})*)?
	VAR ':' (var_def[domDefinitions]  ';'?  {varDefinitions.add($var_def.value);})+
	CONS ':' 
	(
		constraint ';' 
		{constraints.add($constraint.value);}
	)+
	{$value = new ASTProgram(domDefinitions, varDefinitions, constraints);}
	;






	
	
operator 
	:	'+' | '-' | '*' | '/' | '%' 
		| '==' | '!=' | '>' | '>=' | '<' | '<=' 
		| '&&' | '||' | '^^' | '^' | '->'
	;


identifier_or_constant returns [String text]
 	: ID 
 	{
 		final String key = $ID.text;
 		if (this.macroMap.containsKey(key)) {
 			$text =  this.macroMap.get(key);
 		} else {
 			$text = key;
 		}
 	
 	}
 	| INT {$text = $INT.text;}
 	
 	;	

ID  :	('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT :	'0'..'9'+
    ;

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) {$channel=HIDDEN;}
    ;

STRING
    :  '"' ( ESC_SEQ | ~('\\'|'"') )* '"'
    ;

fragment
HEX_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\"'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;
    
    
 COMMENT
    :   '//' ~('\n'|'\r')* '\r'? '\n' {$channel=HIDDEN;}
    |   '/*' ( options {greedy=false;} : . )* '*/' {$channel=HIDDEN;}
    ;   
