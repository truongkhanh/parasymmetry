package ast;

public class ASTVariableExpression extends AbstractASTExpression {

  private String name;

  public ASTVariableExpression(String name) {
    super();
    this.name = name;
  }

  public String getName() {
    return name;
  }

}
