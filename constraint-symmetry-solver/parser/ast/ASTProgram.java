package ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ASTProgram {

  private final List<AbstractASTVarDefinition> varDefinitions;

  private final List<ASTDomDefinition> domDefintions;

  private final List<ASTConstraint> constraints;

  public ASTProgram(List<ASTDomDefinition> domDefinitions,
      List<AbstractASTVarDefinition> varDefinitions,
      List<ASTConstraint> constraints) {
    super();
    this.domDefintions = Collections
        .unmodifiableList(new ArrayList<ASTDomDefinition>(domDefinitions));
    this.varDefinitions = Collections
        .unmodifiableList(new ArrayList<AbstractASTVarDefinition>(
            varDefinitions));
    this.constraints = Collections
        .unmodifiableList(new ArrayList<ASTConstraint>(constraints));
  }

  public List<ASTDomDefinition> getDomDefintions() {
    return domDefintions;
  }

  public List<AbstractASTVarDefinition> getVarDefinitions() {
    return varDefinitions;
  }

  public List<ASTConstraint> getConstraints() {
    return constraints;
  }

}
