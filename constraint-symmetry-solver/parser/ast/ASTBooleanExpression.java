package ast;

public class ASTBooleanExpression extends AbstractASTExpression {

  protected ASTBooleanExpression() {
  }

  public static ASTBooleanExpression TRUE = new ASTBooleanExpression();

  public static ASTBooleanExpression FALSE = new ASTBooleanExpression();
}
