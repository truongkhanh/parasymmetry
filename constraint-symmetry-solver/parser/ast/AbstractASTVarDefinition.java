package ast;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractASTVarDefinition extends
    AbstractTypedWithDomDefinition {

  private final Map<String, String> annotations;

  public AbstractASTVarDefinition(String type, String name,
      List<String> domain, Map<String, String> annotations) {
    super(type, name, domain);
    this.annotations = Collections.unmodifiableMap(new HashMap<String, String>(
        annotations));
  }

  public Map<String, String> getAnnotations() {
    return annotations;
  }

}
