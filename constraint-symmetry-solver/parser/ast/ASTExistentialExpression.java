package ast;

public class ASTExistentialExpression extends AbstractASTQuantifierExpression {

  public ASTExistentialExpression(String varName, String domName,
      AbstractASTExpression child) {
    super(varName, domName, child);
  }

}
