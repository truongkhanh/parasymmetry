package ast;

import java.util.List;

public class ASTDomDefinition extends AbstractTypedWithDomDefinition {

  public ASTDomDefinition(String type, String name, List<String> domain) {
    super(type, name, domain);
  }

}
