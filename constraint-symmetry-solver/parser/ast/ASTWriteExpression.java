package ast;

import java.util.List;

public class ASTWriteExpression extends AbstractASTExpression {

  private final String array1Name; 

  private final String array2Name;

  private AbstractASTExpression[] indices;	

  private AbstractASTExpression value;

  public ASTWriteExpression(String var1Name, String var2Name,  List<AbstractASTExpression> indices, AbstractASTExpression value
    ) {
    if (indices.size() <= 0)
      throw new IllegalArgumentException("Empty index expressions.");
  

    this.array1Name = var1Name;
    this.array2Name = var2Name;
   
    this.value = value;
    this.indices = indices.toArray(new AbstractASTExpression[indices.size()]);
  }

}
