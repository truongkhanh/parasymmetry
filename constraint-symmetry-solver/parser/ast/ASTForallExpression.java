package ast;

public class ASTForallExpression extends AbstractASTQuantifierExpression {

  public ASTForallExpression(String varName, String domName,
      AbstractASTExpression child) {
    super(varName, domName, child);
  }

}
