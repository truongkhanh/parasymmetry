package ast;

import java.util.List;

public class ASTArrayIndexingExpression extends AbstractASTExpression {

  private final String arrayName;

  private AbstractASTExpression[] indices;

  public ASTArrayIndexingExpression(String arrayName,
      List<AbstractASTExpression> indices) {
    if (indices.size() == 0)
      throw new IllegalArgumentException("Empty index expressions.");
    this.arrayName = arrayName;
    this.indices = indices.toArray(new AbstractASTExpression[indices.size()]);
  }

  public String getArrayName() {
    return arrayName;
  }

  public int numberOfIndices() {
    return this.indices.length;
  }

  public AbstractASTExpression getIndex(int index) {
    return this.indices[index];
  }

}
