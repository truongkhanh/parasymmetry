package ast;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import css.exceptions.ParsingFailureException;

public class AbstractTypedWithDomDefinition {

	protected final String type;

	protected final Set<String> domain;

	protected final String name;

	public AbstractTypedWithDomDefinition(String type, String name,
			List<String> domain) {
		super();
		if (domain.isEmpty()) {
		  throw new ParsingFailureException("The domain for variable " + name + " is empty!");
		}
		this.type = type;
		this.name = name;
		this.domain = Collections.unmodifiableSet(new HashSet<String>(domain));
	}

	public String getType() {
		return type;
	}

	public Set<String> getDomain() {
		return domain;
	}

	public String getName() {
		return name;
	}
}
