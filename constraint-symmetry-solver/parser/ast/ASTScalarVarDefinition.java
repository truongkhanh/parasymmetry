package ast;

import java.util.List;
import java.util.Map;

public class ASTScalarVarDefinition extends AbstractASTVarDefinition {

  public ASTScalarVarDefinition(String type, String name, List<String> domain,
      Map<String, String> annotations) {
    super(type, name, domain, annotations);
  }

}
