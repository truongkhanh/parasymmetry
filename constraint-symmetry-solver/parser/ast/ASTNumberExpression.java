package ast;

public class ASTNumberExpression extends AbstractASTExpression {

  private int value;

  public ASTNumberExpression(int value) {
    super();
    this.value = value;
  }

  public int getValue() {
    return value;
  }

}
