package ast;

public class ASTNotOperator extends AbstractASTUnaryExpression {

  public ASTNotOperator(AbstractASTExpression expression) {
    super(expression);
  }

}
