package ast;


public class AbstractASTUnaryExpression extends AbstractASTExpression {

  protected final AbstractASTExpression child;

  public AbstractASTUnaryExpression(AbstractASTExpression child) {
    super();
    this.child = child;
  }

  public AbstractASTExpression getChild() {
    return child;
  }
}
