package ast;

import java.util.List;
import java.util.Map;

public class ASTArrayDefinition extends AbstractASTVarDefinition {

  private final int[] dimensions;

  public ASTArrayDefinition(String type, String name, List<String> domain,
      List<Integer> dimensions, Map<String, String> annotations) {
    super(type, name, domain, annotations);
    this.dimensions = new int[dimensions.size()];
    for (int i = 0; i < dimensions.size(); ++i) {
      this.dimensions[i] = dimensions.get(i).intValue();
    }
  }

  public int numberOfDimensions() {
    return this.dimensions.length;
  }

  public int getDimension(int index) {
    return this.dimensions[index];
  }

}
