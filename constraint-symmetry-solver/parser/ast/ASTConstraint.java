package ast;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ASTConstraint {

  private final Map<String, String> annotations;

  private final AbstractASTExpression expression;

  public ASTConstraint(Map<String, String> annotations,
      AbstractASTExpression expression) {
    this.annotations = Collections.unmodifiableMap(new HashMap<String, String>(
        annotations));
    this.expression = expression;
  }

  public AbstractASTExpression getExpression() {
    return expression;
  }

  public Map<String, String> getAnnotations() {
    return annotations;
  }

}
