package ast;

public class ASTBinaryExpression extends AbstractASTExpression {

  private String operator;

  private AbstractASTExpression left;

  private AbstractASTExpression right;

  public ASTBinaryExpression(String operator, AbstractASTExpression left,
      AbstractASTExpression right) {
    super();
    this.operator = operator;
    this.left = left;
    this.right = right;
  }

  public String getOperator() {
    return operator;
  }

  public AbstractASTExpression getLeft() {
    return left;
  }

  public AbstractASTExpression getRight() {
    return right;
  }

}
