package ast;

public abstract class AbstractASTQuantifierExpression extends
    AbstractASTUnaryExpression {

  protected final String localVariableName;

  protected final String domainName;

  // FORALL varName=ID IN domName=ID '.' exp
  public AbstractASTQuantifierExpression(String varName, String domName,
      AbstractASTExpression child) {
    super(child);
    this.localVariableName = varName;
    this.domainName = domName;
  }

  public String getDomainName() {
    return domainName;
  }

  public String getLocalVariableName() {
    return localVariableName;
  }

}
