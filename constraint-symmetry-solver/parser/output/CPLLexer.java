// $ANTLR 3.4 /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g 2012-10-10 23:43:28
package output;

import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked"})
public class CPLLexer extends Lexer {
    public static final int EOF=-1;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int AND=4;
    public static final int BOOL_TYPE=5;
    public static final int COMMENT=6;
    public static final int CONS=7;
    public static final int DIVIDE=8;
    public static final int DOM=9;
    public static final int EQUALITY=10;
    public static final int ESC_SEQ=11;
    public static final int EXIST=12;
    public static final int FALSE=13;
    public static final int FORALL=14;
    public static final int GE=15;
    public static final int GT=16;
    public static final int HEX_DIGIT=17;
    public static final int ID=18;
    public static final int IMPLY=19;
    public static final int IN=20;
    public static final int INT=21;
    public static final int INT_TYPE=22;
    public static final int LE=23;
    public static final int LT=24;
    public static final int MINUS=25;
    public static final int MODULO=26;
    public static final int NON_EQUALITY=27;
    public static final int NOT=28;
    public static final int OCTAL_ESC=29;
    public static final int OR=30;
    public static final int PLUS=31;
    public static final int POWER=32;
    public static final int STRING=33;
    public static final int TIMES=34;
    public static final int TRUE=35;
    public static final int UNICODE_ESC=36;
    public static final int VAR=37;
    public static final int WRITE=38;
    public static final int WS=39;
    public static final int XOR=40;

    // delegates
    // delegators
    public Lexer[] getDelegates() {
        return new Lexer[] {};
    }

    public CPLLexer() {} 
    public CPLLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public CPLLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);
    }
    public String getGrammarFileName() { return "/home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g"; }

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:9:5: ( '&&' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:9:7: '&&'
            {
            match("&&"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "BOOL_TYPE"
    public final void mBOOL_TYPE() throws RecognitionException {
        try {
            int _type = BOOL_TYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:10:11: ( 'bool' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:10:13: 'bool'
            {
            match("bool"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "BOOL_TYPE"

    // $ANTLR start "CONS"
    public final void mCONS() throws RecognitionException {
        try {
            int _type = CONS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:11:6: ( 'cons' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:11:8: 'cons'
            {
            match("cons"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "CONS"

    // $ANTLR start "DIVIDE"
    public final void mDIVIDE() throws RecognitionException {
        try {
            int _type = DIVIDE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:12:8: ( '/' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:12:10: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DIVIDE"

    // $ANTLR start "DOM"
    public final void mDOM() throws RecognitionException {
        try {
            int _type = DOM;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:13:5: ( 'dom' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:13:7: 'dom'
            {
            match("dom"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "DOM"

    // $ANTLR start "EQUALITY"
    public final void mEQUALITY() throws RecognitionException {
        try {
            int _type = EQUALITY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:14:10: ( '==' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:14:12: '=='
            {
            match("=="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EQUALITY"

    // $ANTLR start "EXIST"
    public final void mEXIST() throws RecognitionException {
        try {
            int _type = EXIST;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:15:7: ( 'exists' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:15:9: 'exists'
            {
            match("exists"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "EXIST"

    // $ANTLR start "FALSE"
    public final void mFALSE() throws RecognitionException {
        try {
            int _type = FALSE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:16:7: ( 'false' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:16:9: 'false'
            {
            match("false"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FALSE"

    // $ANTLR start "FORALL"
    public final void mFORALL() throws RecognitionException {
        try {
            int _type = FORALL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:17:8: ( 'forall' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:17:10: 'forall'
            {
            match("forall"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "FORALL"

    // $ANTLR start "GE"
    public final void mGE() throws RecognitionException {
        try {
            int _type = GE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:18:4: ( '>=' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:18:6: '>='
            {
            match(">="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GE"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:19:4: ( '>' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:19:6: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "IMPLY"
    public final void mIMPLY() throws RecognitionException {
        try {
            int _type = IMPLY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:20:7: ( '->' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:20:9: '->'
            {
            match("->"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IMPLY"

    // $ANTLR start "IN"
    public final void mIN() throws RecognitionException {
        try {
            int _type = IN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:21:4: ( 'in' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:21:6: 'in'
            {
            match("in"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "IN"

    // $ANTLR start "INT_TYPE"
    public final void mINT_TYPE() throws RecognitionException {
        try {
            int _type = INT_TYPE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:22:10: ( 'int' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:22:12: 'int'
            {
            match("int"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INT_TYPE"

    // $ANTLR start "LE"
    public final void mLE() throws RecognitionException {
        try {
            int _type = LE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:23:4: ( '<=' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:23:6: '<='
            {
            match("<="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LE"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:24:4: ( '<' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:24:6: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:25:7: ( '-' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:25:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "MODULO"
    public final void mMODULO() throws RecognitionException {
        try {
            int _type = MODULO;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:26:8: ( '%' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:26:10: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "MODULO"

    // $ANTLR start "NON_EQUALITY"
    public final void mNON_EQUALITY() throws RecognitionException {
        try {
            int _type = NON_EQUALITY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:27:14: ( '!=' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:27:16: '!='
            {
            match("!="); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NON_EQUALITY"

    // $ANTLR start "NOT"
    public final void mNOT() throws RecognitionException {
        try {
            int _type = NOT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:28:5: ( '!' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:28:7: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "NOT"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:29:4: ( '||' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:29:6: '||'
            {
            match("||"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:30:6: ( '+' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:30:8: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "POWER"
    public final void mPOWER() throws RecognitionException {
        try {
            int _type = POWER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:31:7: ( '^^' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:31:9: '^^'
            {
            match("^^"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "POWER"

    // $ANTLR start "TIMES"
    public final void mTIMES() throws RecognitionException {
        try {
            int _type = TIMES;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:32:7: ( '*' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:32:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TIMES"

    // $ANTLR start "TRUE"
    public final void mTRUE() throws RecognitionException {
        try {
            int _type = TRUE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:33:6: ( 'true' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:33:8: 'true'
            {
            match("true"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "TRUE"

    // $ANTLR start "VAR"
    public final void mVAR() throws RecognitionException {
        try {
            int _type = VAR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:34:5: ( 'var' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:34:7: 'var'
            {
            match("var"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "VAR"

    // $ANTLR start "WRITE"
    public final void mWRITE() throws RecognitionException {
        try {
            int _type = WRITE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:35:7: ( 'Write' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:35:9: 'Write'
            {
            match("Write"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WRITE"

    // $ANTLR start "XOR"
    public final void mXOR() throws RecognitionException {
        try {
            int _type = XOR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:36:5: ( '^' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:36:7: '^'
            {
            match('^'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "XOR"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:37:7: ( '#define' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:37:9: '#define'
            {
            match("#define"); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:38:7: ( '(' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:38:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:39:7: ( ')' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:39:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:40:7: ( ',' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:40:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:41:7: ( '.' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:41:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:42:7: ( '..' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:42:9: '..'
            {
            match(".."); 



            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:43:7: ( ':' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:43:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:44:7: ( ';' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:44:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:45:7: ( '=' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:45:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:46:7: ( '@' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:46:9: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:47:7: ( '[' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:47:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:48:7: ( ']' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:48:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:49:7: ( '{' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:49:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:50:7: ( '}' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:50:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "ID"
    public final void mID() throws RecognitionException {
        try {
            int _type = ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:362:5: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:362:7: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            {
            if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:362:31: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ID"

    // $ANTLR start "INT"
    public final void mINT() throws RecognitionException {
        try {
            int _type = INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:365:5: ( ( '0' .. '9' )+ )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:365:7: ( '0' .. '9' )+
            {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:365:7: ( '0' .. '9' )+
            int cnt2=0;
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:
            	    {
            	    if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt2 >= 1 ) break loop2;
                        EarlyExitException eee =
                            new EarlyExitException(2, input);
                        throw eee;
                }
                cnt2++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "INT"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:368:5: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:368:9: ( ' ' | '\\t' | '\\r' | '\\n' )
            {
            if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "WS"

    // $ANTLR start "STRING"
    public final void mSTRING() throws RecognitionException {
        try {
            int _type = STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:376:5: ( '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:376:8: '\"' ( ESC_SEQ |~ ( '\\\\' | '\"' ) )* '\"'
            {
            match('\"'); 

            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:376:12: ( ESC_SEQ |~ ( '\\\\' | '\"' ) )*
            loop3:
            do {
                int alt3=3;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='\\') ) {
                    alt3=1;
                }
                else if ( ((LA3_0 >= '\u0000' && LA3_0 <= '!')||(LA3_0 >= '#' && LA3_0 <= '[')||(LA3_0 >= ']' && LA3_0 <= '\uFFFF')) ) {
                    alt3=2;
                }


                switch (alt3) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:376:14: ESC_SEQ
            	    {
            	    mESC_SEQ(); 


            	    }
            	    break;
            	case 2 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:376:24: ~ ( '\\\\' | '\"' )
            	    {
            	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '!')||(input.LA(1) >= '#' && input.LA(1) <= '[')||(input.LA(1) >= ']' && input.LA(1) <= '\uFFFF') ) {
            	        input.consume();
            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;
            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            match('\"'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "STRING"

    // $ANTLR start "HEX_DIGIT"
    public final void mHEX_DIGIT() throws RecognitionException {
        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:381:11: ( ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' ) )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:
            {
            if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
                input.consume();
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;
            }


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "HEX_DIGIT"

    // $ANTLR start "ESC_SEQ"
    public final void mESC_SEQ() throws RecognitionException {
        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:385:5: ( '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' ) | UNICODE_ESC | OCTAL_ESC )
            int alt4=3;
            int LA4_0 = input.LA(1);

            if ( (LA4_0=='\\') ) {
                switch ( input.LA(2) ) {
                case '\"':
                case '\'':
                case '\\':
                case 'b':
                case 'f':
                case 'n':
                case 'r':
                case 't':
                    {
                    alt4=1;
                    }
                    break;
                case 'u':
                    {
                    alt4=2;
                    }
                    break;
                case '0':
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                    {
                    alt4=3;
                    }
                    break;
                default:
                    NoViableAltException nvae =
                        new NoViableAltException("", 4, 1, input);

                    throw nvae;

                }

            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;

            }
            switch (alt4) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:385:9: '\\\\' ( 'b' | 't' | 'n' | 'f' | 'r' | '\\\"' | '\\'' | '\\\\' )
                    {
                    match('\\'); 

                    if ( input.LA(1)=='\"'||input.LA(1)=='\''||input.LA(1)=='\\'||input.LA(1)=='b'||input.LA(1)=='f'||input.LA(1)=='n'||input.LA(1)=='r'||input.LA(1)=='t' ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:386:9: UNICODE_ESC
                    {
                    mUNICODE_ESC(); 


                    }
                    break;
                case 3 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:387:9: OCTAL_ESC
                    {
                    mOCTAL_ESC(); 


                    }
                    break;

            }

        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "ESC_SEQ"

    // $ANTLR start "OCTAL_ESC"
    public final void mOCTAL_ESC() throws RecognitionException {
        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:392:5: ( '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\' ( '0' .. '7' ) )
            int alt5=3;
            int LA5_0 = input.LA(1);

            if ( (LA5_0=='\\') ) {
                int LA5_1 = input.LA(2);

                if ( ((LA5_1 >= '0' && LA5_1 <= '3')) ) {
                    int LA5_2 = input.LA(3);

                    if ( ((LA5_2 >= '0' && LA5_2 <= '7')) ) {
                        int LA5_4 = input.LA(4);

                        if ( ((LA5_4 >= '0' && LA5_4 <= '7')) ) {
                            alt5=1;
                        }
                        else {
                            alt5=2;
                        }
                    }
                    else {
                        alt5=3;
                    }
                }
                else if ( ((LA5_1 >= '4' && LA5_1 <= '7')) ) {
                    int LA5_3 = input.LA(3);

                    if ( ((LA5_3 >= '0' && LA5_3 <= '7')) ) {
                        alt5=2;
                    }
                    else {
                        alt5=3;
                    }
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }
            switch (alt5) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:392:9: '\\\\' ( '0' .. '3' ) ( '0' .. '7' ) ( '0' .. '7' )
                    {
                    match('\\'); 

                    if ( (input.LA(1) >= '0' && input.LA(1) <= '3') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:393:9: '\\\\' ( '0' .. '7' ) ( '0' .. '7' )
                    {
                    match('\\'); 

                    if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;
                case 3 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:394:9: '\\\\' ( '0' .. '7' )
                    {
                    match('\\'); 

                    if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
                        input.consume();
                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;
                    }


                    }
                    break;

            }

        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "OCTAL_ESC"

    // $ANTLR start "UNICODE_ESC"
    public final void mUNICODE_ESC() throws RecognitionException {
        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:399:5: ( '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:399:9: '\\\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
            {
            match('\\'); 

            match('u'); 

            mHEX_DIGIT(); 


            mHEX_DIGIT(); 


            mHEX_DIGIT(); 


            mHEX_DIGIT(); 


            }


        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "UNICODE_ESC"

    // $ANTLR start "COMMENT"
    public final void mCOMMENT() throws RecognitionException {
        try {
            int _type = COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:403:5: ( '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n' | '/*' ( options {greedy=false; } : . )* '*/' )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0=='/') ) {
                int LA9_1 = input.LA(2);

                if ( (LA9_1=='/') ) {
                    alt9=1;
                }
                else if ( (LA9_1=='*') ) {
                    alt9=2;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 9, 1, input);

                    throw nvae;

                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;

            }
            switch (alt9) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:403:9: '//' (~ ( '\\n' | '\\r' ) )* ( '\\r' )? '\\n'
                    {
                    match("//"); 



                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:403:14: (~ ( '\\n' | '\\r' ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( ((LA6_0 >= '\u0000' && LA6_0 <= '\t')||(LA6_0 >= '\u000B' && LA6_0 <= '\f')||(LA6_0 >= '\u000E' && LA6_0 <= '\uFFFF')) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:
                    	    {
                    	    if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
                    	        input.consume();
                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;
                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:403:28: ( '\\r' )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0=='\r') ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:403:28: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }


                    match('\n'); 

                    _channel=HIDDEN;

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:404:9: '/*' ( options {greedy=false; } : . )* '*/'
                    {
                    match("/*"); 



                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:404:14: ( options {greedy=false; } : . )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0=='*') ) {
                            int LA8_1 = input.LA(2);

                            if ( (LA8_1=='/') ) {
                                alt8=2;
                            }
                            else if ( ((LA8_1 >= '\u0000' && LA8_1 <= '.')||(LA8_1 >= '0' && LA8_1 <= '\uFFFF')) ) {
                                alt8=1;
                            }


                        }
                        else if ( ((LA8_0 >= '\u0000' && LA8_0 <= ')')||(LA8_0 >= '+' && LA8_0 <= '\uFFFF')) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:404:42: .
                    	    {
                    	    matchAny(); 

                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);


                    match("*/"); 



                    _channel=HIDDEN;

                    }
                    break;

            }
            state.type = _type;
            state.channel = _channel;
        }
        finally {
        	// do for sure before leaving
        }
    }
    // $ANTLR end "COMMENT"

    public void mTokens() throws RecognitionException {
        // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:8: ( AND | BOOL_TYPE | CONS | DIVIDE | DOM | EQUALITY | EXIST | FALSE | FORALL | GE | GT | IMPLY | IN | INT_TYPE | LE | LT | MINUS | MODULO | NON_EQUALITY | NOT | OR | PLUS | POWER | TIMES | TRUE | VAR | WRITE | XOR | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | ID | INT | WS | STRING | COMMENT )
        int alt10=47;
        alt10 = dfa10.predict(input);
        switch (alt10) {
            case 1 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:10: AND
                {
                mAND(); 


                }
                break;
            case 2 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:14: BOOL_TYPE
                {
                mBOOL_TYPE(); 


                }
                break;
            case 3 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:24: CONS
                {
                mCONS(); 


                }
                break;
            case 4 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:29: DIVIDE
                {
                mDIVIDE(); 


                }
                break;
            case 5 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:36: DOM
                {
                mDOM(); 


                }
                break;
            case 6 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:40: EQUALITY
                {
                mEQUALITY(); 


                }
                break;
            case 7 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:49: EXIST
                {
                mEXIST(); 


                }
                break;
            case 8 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:55: FALSE
                {
                mFALSE(); 


                }
                break;
            case 9 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:61: FORALL
                {
                mFORALL(); 


                }
                break;
            case 10 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:68: GE
                {
                mGE(); 


                }
                break;
            case 11 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:71: GT
                {
                mGT(); 


                }
                break;
            case 12 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:74: IMPLY
                {
                mIMPLY(); 


                }
                break;
            case 13 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:80: IN
                {
                mIN(); 


                }
                break;
            case 14 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:83: INT_TYPE
                {
                mINT_TYPE(); 


                }
                break;
            case 15 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:92: LE
                {
                mLE(); 


                }
                break;
            case 16 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:95: LT
                {
                mLT(); 


                }
                break;
            case 17 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:98: MINUS
                {
                mMINUS(); 


                }
                break;
            case 18 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:104: MODULO
                {
                mMODULO(); 


                }
                break;
            case 19 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:111: NON_EQUALITY
                {
                mNON_EQUALITY(); 


                }
                break;
            case 20 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:124: NOT
                {
                mNOT(); 


                }
                break;
            case 21 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:128: OR
                {
                mOR(); 


                }
                break;
            case 22 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:131: PLUS
                {
                mPLUS(); 


                }
                break;
            case 23 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:136: POWER
                {
                mPOWER(); 


                }
                break;
            case 24 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:142: TIMES
                {
                mTIMES(); 


                }
                break;
            case 25 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:148: TRUE
                {
                mTRUE(); 


                }
                break;
            case 26 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:153: VAR
                {
                mVAR(); 


                }
                break;
            case 27 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:157: WRITE
                {
                mWRITE(); 


                }
                break;
            case 28 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:163: XOR
                {
                mXOR(); 


                }
                break;
            case 29 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:167: T__41
                {
                mT__41(); 


                }
                break;
            case 30 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:173: T__42
                {
                mT__42(); 


                }
                break;
            case 31 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:179: T__43
                {
                mT__43(); 


                }
                break;
            case 32 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:185: T__44
                {
                mT__44(); 


                }
                break;
            case 33 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:191: T__45
                {
                mT__45(); 


                }
                break;
            case 34 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:197: T__46
                {
                mT__46(); 


                }
                break;
            case 35 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:203: T__47
                {
                mT__47(); 


                }
                break;
            case 36 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:209: T__48
                {
                mT__48(); 


                }
                break;
            case 37 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:215: T__49
                {
                mT__49(); 


                }
                break;
            case 38 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:221: T__50
                {
                mT__50(); 


                }
                break;
            case 39 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:227: T__51
                {
                mT__51(); 


                }
                break;
            case 40 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:233: T__52
                {
                mT__52(); 


                }
                break;
            case 41 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:239: T__53
                {
                mT__53(); 


                }
                break;
            case 42 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:245: T__54
                {
                mT__54(); 


                }
                break;
            case 43 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:251: ID
                {
                mID(); 


                }
                break;
            case 44 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:254: INT
                {
                mINT(); 


                }
                break;
            case 45 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:258: WS
                {
                mWS(); 


                }
                break;
            case 46 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:261: STRING
                {
                mSTRING(); 


                }
                break;
            case 47 :
                // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:1:268: COMMENT
                {
                mCOMMENT(); 


                }
                break;

        }

    }


    protected DFA10 dfa10 = new DFA10(this);
    static final String DFA10_eotS =
        "\2\uffff\2\42\1\51\1\42\1\54\2\42\1\61\1\63\1\42\1\66\1\uffff\1"+
        "\70\2\uffff\1\72\1\uffff\3\42\4\uffff\1\77\13\uffff\2\42\2\uffff"+
        "\1\42\2\uffff\3\42\4\uffff\1\107\6\uffff\3\42\2\uffff\2\42\1\115"+
        "\3\42\1\121\1\uffff\1\42\1\123\1\42\1\125\1\126\1\uffff\3\42\1\uffff"+
        "\1\132\1\uffff\1\42\2\uffff\1\42\1\135\1\42\1\uffff\1\137\1\140"+
        "\1\uffff\1\141\3\uffff";
    static final String DFA10_eofS =
        "\142\uffff";
    static final String DFA10_minS =
        "\1\11\1\uffff\2\157\1\52\1\157\1\75\1\170\1\141\1\75\1\76\1\156"+
        "\1\75\1\uffff\1\75\2\uffff\1\136\1\uffff\1\162\1\141\1\162\4\uffff"+
        "\1\56\13\uffff\1\157\1\156\2\uffff\1\155\2\uffff\1\151\1\154\1\162"+
        "\4\uffff\1\60\6\uffff\1\165\1\162\1\151\2\uffff\1\154\1\163\1\60"+
        "\2\163\1\141\1\60\1\uffff\1\145\1\60\1\164\2\60\1\uffff\1\164\1"+
        "\145\1\154\1\uffff\1\60\1\uffff\1\145\2\uffff\1\163\1\60\1\154\1"+
        "\uffff\2\60\1\uffff\1\60\3\uffff";
    static final String DFA10_maxS =
        "\1\175\1\uffff\2\157\1\57\1\157\1\75\1\170\1\157\1\75\1\76\1\156"+
        "\1\75\1\uffff\1\75\2\uffff\1\136\1\uffff\1\162\1\141\1\162\4\uffff"+
        "\1\56\13\uffff\1\157\1\156\2\uffff\1\155\2\uffff\1\151\1\154\1\162"+
        "\4\uffff\1\172\6\uffff\1\165\1\162\1\151\2\uffff\1\154\1\163\1\172"+
        "\2\163\1\141\1\172\1\uffff\1\145\1\172\1\164\2\172\1\uffff\1\164"+
        "\1\145\1\154\1\uffff\1\172\1\uffff\1\145\2\uffff\1\163\1\172\1\154"+
        "\1\uffff\2\172\1\uffff\1\172\3\uffff";
    static final String DFA10_acceptS =
        "\1\uffff\1\1\13\uffff\1\22\1\uffff\1\25\1\26\1\uffff\1\30\3\uffff"+
        "\1\35\1\36\1\37\1\40\1\uffff\1\43\1\44\1\46\1\47\1\50\1\51\1\52"+
        "\1\53\1\54\1\55\1\56\2\uffff\1\57\1\4\1\uffff\1\6\1\45\3\uffff\1"+
        "\12\1\13\1\14\1\21\1\uffff\1\17\1\20\1\23\1\24\1\27\1\34\3\uffff"+
        "\1\42\1\41\7\uffff\1\15\5\uffff\1\5\3\uffff\1\16\1\uffff\1\32\1"+
        "\uffff\1\2\1\3\3\uffff\1\31\2\uffff\1\10\1\uffff\1\33\1\7\1\11";
    static final String DFA10_specialS =
        "\142\uffff}>";
    static final String[] DFA10_transitionS = {
            "\2\44\2\uffff\1\44\22\uffff\1\44\1\16\1\45\1\26\1\uffff\1\15"+
            "\1\1\1\uffff\1\27\1\30\1\22\1\20\1\31\1\12\1\32\1\4\12\43\1"+
            "\33\1\34\1\14\1\6\1\11\1\uffff\1\35\26\42\1\25\3\42\1\36\1\uffff"+
            "\1\37\1\21\1\42\1\uffff\1\42\1\2\1\3\1\5\1\7\1\10\2\42\1\13"+
            "\12\42\1\23\1\42\1\24\4\42\1\40\1\17\1\41",
            "",
            "\1\46",
            "\1\47",
            "\1\50\4\uffff\1\50",
            "\1\52",
            "\1\53",
            "\1\55",
            "\1\56\15\uffff\1\57",
            "\1\60",
            "\1\62",
            "\1\64",
            "\1\65",
            "",
            "\1\67",
            "",
            "",
            "\1\71",
            "",
            "\1\73",
            "\1\74",
            "\1\75",
            "",
            "",
            "",
            "",
            "\1\76",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\100",
            "\1\101",
            "",
            "",
            "\1\102",
            "",
            "",
            "\1\103",
            "\1\104",
            "\1\105",
            "",
            "",
            "",
            "",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\23\42\1\106\6\42",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\110",
            "\1\111",
            "\1\112",
            "",
            "",
            "\1\113",
            "\1\114",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "\1\116",
            "\1\117",
            "\1\120",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "",
            "\1\122",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "\1\124",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "",
            "\1\127",
            "\1\130",
            "\1\131",
            "",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "",
            "\1\133",
            "",
            "",
            "\1\134",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "\1\136",
            "",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "",
            "\12\42\7\uffff\32\42\4\uffff\1\42\1\uffff\32\42",
            "",
            "",
            ""
    };

    static final short[] DFA10_eot = DFA.unpackEncodedString(DFA10_eotS);
    static final short[] DFA10_eof = DFA.unpackEncodedString(DFA10_eofS);
    static final char[] DFA10_min = DFA.unpackEncodedStringToUnsignedChars(DFA10_minS);
    static final char[] DFA10_max = DFA.unpackEncodedStringToUnsignedChars(DFA10_maxS);
    static final short[] DFA10_accept = DFA.unpackEncodedString(DFA10_acceptS);
    static final short[] DFA10_special = DFA.unpackEncodedString(DFA10_specialS);
    static final short[][] DFA10_transition;

    static {
        int numStates = DFA10_transitionS.length;
        DFA10_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA10_transition[i] = DFA.unpackEncodedString(DFA10_transitionS[i]);
        }
    }

    class DFA10 extends DFA {

        public DFA10(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 10;
            this.eot = DFA10_eot;
            this.eof = DFA10_eof;
            this.min = DFA10_min;
            this.max = DFA10_max;
            this.accept = DFA10_accept;
            this.special = DFA10_special;
            this.transition = DFA10_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( AND | BOOL_TYPE | CONS | DIVIDE | DOM | EQUALITY | EXIST | FALSE | FORALL | GE | GT | IMPLY | IN | INT_TYPE | LE | LT | MINUS | MODULO | NON_EQUALITY | NOT | OR | PLUS | POWER | TIMES | TRUE | VAR | WRITE | XOR | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | ID | INT | WS | STRING | COMMENT );";
        }
    }
 

}