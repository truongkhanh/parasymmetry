// $ANTLR 3.4 /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g 2012-10-10 23:43:28

package output;
import ast.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;


@SuppressWarnings({"all", "warnings", "unchecked"})
public class CPLParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "AND", "BOOL_TYPE", "COMMENT", "CONS", "DIVIDE", "DOM", "EQUALITY", "ESC_SEQ", "EXIST", "FALSE", "FORALL", "GE", "GT", "HEX_DIGIT", "ID", "IMPLY", "IN", "INT", "INT_TYPE", "LE", "LT", "MINUS", "MODULO", "NON_EQUALITY", "NOT", "OCTAL_ESC", "OR", "PLUS", "POWER", "STRING", "TIMES", "TRUE", "UNICODE_ESC", "VAR", "WRITE", "WS", "XOR", "'#define'", "'('", "')'", "','", "'.'", "'..'", "':'", "';'", "'='", "'@'", "'['", "']'", "'{'", "'}'"
    };

    public static final int EOF=-1;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__50=50;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int AND=4;
    public static final int BOOL_TYPE=5;
    public static final int COMMENT=6;
    public static final int CONS=7;
    public static final int DIVIDE=8;
    public static final int DOM=9;
    public static final int EQUALITY=10;
    public static final int ESC_SEQ=11;
    public static final int EXIST=12;
    public static final int FALSE=13;
    public static final int FORALL=14;
    public static final int GE=15;
    public static final int GT=16;
    public static final int HEX_DIGIT=17;
    public static final int ID=18;
    public static final int IMPLY=19;
    public static final int IN=20;
    public static final int INT=21;
    public static final int INT_TYPE=22;
    public static final int LE=23;
    public static final int LT=24;
    public static final int MINUS=25;
    public static final int MODULO=26;
    public static final int NON_EQUALITY=27;
    public static final int NOT=28;
    public static final int OCTAL_ESC=29;
    public static final int OR=30;
    public static final int PLUS=31;
    public static final int POWER=32;
    public static final int STRING=33;
    public static final int TIMES=34;
    public static final int TRUE=35;
    public static final int UNICODE_ESC=36;
    public static final int VAR=37;
    public static final int WRITE=38;
    public static final int WS=39;
    public static final int XOR=40;

    // delegates
    public Parser[] getDelegates() {
        return new Parser[] {};
    }

    // delegators


    public CPLParser(TokenStream input) {
        this(input, new RecognizerSharedState());
    }
    public CPLParser(TokenStream input, RecognizerSharedState state) {
        super(input, state);
    }

protected TreeAdaptor adaptor = new CommonTreeAdaptor();

public void setTreeAdaptor(TreeAdaptor adaptor) {
    this.adaptor = adaptor;
}
public TreeAdaptor getTreeAdaptor() {
    return adaptor;
}
    public String[] getTokenNames() { return CPLParser.tokenNames; }
    public String getGrammarFileName() { return "/home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g"; }



            private final Map<String,  String> macroMap = new HashMap<String, String>();

            boolean isRightToLeft( int type )
            {
                    return true;
            }
            int getOperatorPrecedence( int type )
            {
                    switch ( type )
                    {
                    case POWER:
                       	 return 1;
                    case TIMES:
                    case DIVIDE:
                    case MODULO:
                            return 2;
                    case PLUS:
                    case MINUS:
                            return 3;
                    case GT:
                    case LT:
                    case GE:
                    case LE:
                    	return 4;
                    case EQUALITY:
                    case NON_EQUALITY:
                    	return 5;
                    case XOR:
                    	return 6;                	
                   case AND:
                   case OR:
                   	return 7;
                   case IMPLY:
                   	return 8;
                    default:
                            throw new RuntimeException("Cannot reach here."); // really this shouldn't be hit
                    }
            }
            int findPivot( List operators, int startIndex, int stopIndex )
            {
                    int pivot = startIndex;
                    int pivotRank = getOperatorPrecedence( ((Token)operators.get(pivot)).getType() );
                    for ( int i = startIndex + 1; i <= stopIndex; i++ )
                    {
                            int type = ((Token)operators.get(i)).getType();
                            int current = getOperatorPrecedence( type );
                            boolean rtl = isRightToLeft(type);
                            if ( current > pivotRank || (current == pivotRank && rtl) )
                            {
                                    pivot = i;
                                    pivotRank = current;
                            }
                    }
                    return pivot;
            }
            AbstractASTExpression createPrecedenceTree( List expressions, List operators, int startIndex, int stopIndex )
            {
                    if ( stopIndex == startIndex )
                            return (AbstractASTExpression)expressions.get(startIndex);

                    int pivot = findPivot( operators, startIndex, stopIndex - 1 );
                    Token token = (Token)operators.get(pivot);
                    AbstractASTExpression left = createPrecedenceTree( expressions, operators, startIndex, pivot);
                    AbstractASTExpression right = createPrecedenceTree( expressions, operators, pivot + 1, stopIndex );
                    ASTBinaryExpression root = new ASTBinaryExpression(token.getText(), left, right);
                    return root;
            }
            AbstractASTExpression createPrecedenceTree( List expressions, List operators )
            {
                    return createPrecedenceTree( expressions, operators, 0, expressions.size() - 1 );
            }


    public static class num_exp_return extends ParserRuleReturnScope {
        public ASTNumberExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "num_exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:142:1: num_exp returns [ASTNumberExpression value] : INT ;
    public final CPLParser.num_exp_return num_exp() throws RecognitionException {
        CPLParser.num_exp_return retval = new CPLParser.num_exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token INT1=null;

        Object INT1_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:143:2: ( INT )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:143:4: INT
            {
            root_0 = (Object)adaptor.nil();


            INT1=(Token)match(input,INT,FOLLOW_INT_in_num_exp609); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            INT1_tree = 
            (Object)adaptor.create(INT1)
            ;
            adaptor.addChild(root_0, INT1_tree);
            }

            if ( state.backtracking==0 ) {retval.value = new ASTNumberExpression(Integer.parseInt((INT1!=null?INT1.getText():null)));}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "num_exp"


    public static class var_exp_return extends ParserRuleReturnScope {
        public AbstractASTExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "var_exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:145:1: var_exp returns [AbstractASTExpression value] : ID ( '[' exp ']' )* ;
    public final CPLParser.var_exp_return var_exp() throws RecognitionException {
        CPLParser.var_exp_return retval = new CPLParser.var_exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token ID2=null;
        Token char_literal3=null;
        Token char_literal5=null;
        CPLParser.exp_return exp4 =null;


        Object ID2_tree=null;
        Object char_literal3_tree=null;
        Object char_literal5_tree=null;


        	List<AbstractASTExpression> indices = new ArrayList<AbstractASTExpression>();

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:149:2: ( ID ( '[' exp ']' )* )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:149:4: ID ( '[' exp ']' )*
            {
            root_0 = (Object)adaptor.nil();


            ID2=(Token)match(input,ID,FOLLOW_ID_in_var_exp629); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            ID2_tree = 
            (Object)adaptor.create(ID2)
            ;
            adaptor.addChild(root_0, ID2_tree);
            }

            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:149:7: ( '[' exp ']' )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==51) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:149:8: '[' exp ']'
            	    {
            	    char_literal3=(Token)match(input,51,FOLLOW_51_in_var_exp632); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal3_tree = 
            	    (Object)adaptor.create(char_literal3)
            	    ;
            	    adaptor.addChild(root_0, char_literal3_tree);
            	    }

            	    pushFollow(FOLLOW_exp_in_var_exp634);
            	    exp4=exp();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, exp4.getTree());

            	    char_literal5=(Token)match(input,52,FOLLOW_52_in_var_exp636); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal5_tree = 
            	    (Object)adaptor.create(char_literal5)
            	    ;
            	    adaptor.addChild(root_0, char_literal5_tree);
            	    }

            	    if ( state.backtracking==0 ) {indices.add((exp4!=null?exp4.value:null));}

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            if ( state.backtracking==0 ) {
             	             		if (indices.isEmpty()) {
             	             			retval.value = new ASTVariableExpression((ID2!=null?ID2.getText():null));
             	             		} else {
             	             			retval.value = new ASTArrayIndexingExpression((ID2!=null?ID2.getText():null), indices);
             	             		}
             	             	}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "var_exp"


    public static class enclose_exp_return extends ParserRuleReturnScope {
        public AbstractASTExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "enclose_exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:161:1: enclose_exp returns [AbstractASTExpression value] : '(' ! exp ')' !;
    public final CPLParser.enclose_exp_return enclose_exp() throws RecognitionException {
        CPLParser.enclose_exp_return retval = new CPLParser.enclose_exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token char_literal6=null;
        Token char_literal8=null;
        CPLParser.exp_return exp7 =null;


        Object char_literal6_tree=null;
        Object char_literal8_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:162:2: ( '(' ! exp ')' !)
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:162:4: '(' ! exp ')' !
            {
            root_0 = (Object)adaptor.nil();


            char_literal6=(Token)match(input,42,FOLLOW_42_in_enclose_exp674); if (state.failed) return retval;

            pushFollow(FOLLOW_exp_in_enclose_exp677);
            exp7=exp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, exp7.getTree());

            char_literal8=(Token)match(input,43,FOLLOW_43_in_enclose_exp679); if (state.failed) return retval;

            if ( state.backtracking==0 ) {retval.value = (exp7!=null?exp7.value:null);}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "enclose_exp"


    public static class bool_exp_return extends ParserRuleReturnScope {
        public AbstractASTExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "bool_exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:164:1: bool_exp returns [AbstractASTExpression value] : ( TRUE | FALSE | NOT atom_exp );
    public final CPLParser.bool_exp_return bool_exp() throws RecognitionException {
        CPLParser.bool_exp_return retval = new CPLParser.bool_exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token TRUE9=null;
        Token FALSE10=null;
        Token NOT11=null;
        CPLParser.atom_exp_return atom_exp12 =null;


        Object TRUE9_tree=null;
        Object FALSE10_tree=null;
        Object NOT11_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:165:2: ( TRUE | FALSE | NOT atom_exp )
            int alt2=3;
            switch ( input.LA(1) ) {
            case TRUE:
                {
                alt2=1;
                }
                break;
            case FALSE:
                {
                alt2=2;
                }
                break;
            case NOT:
                {
                alt2=3;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;

            }

            switch (alt2) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:165:4: TRUE
                    {
                    root_0 = (Object)adaptor.nil();


                    TRUE9=(Token)match(input,TRUE,FOLLOW_TRUE_in_bool_exp696); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    TRUE9_tree = 
                    (Object)adaptor.create(TRUE9)
                    ;
                    adaptor.addChild(root_0, TRUE9_tree);
                    }

                    if ( state.backtracking==0 ) {retval.value = ASTBooleanExpression.TRUE;}

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:166:5: FALSE
                    {
                    root_0 = (Object)adaptor.nil();


                    FALSE10=(Token)match(input,FALSE,FOLLOW_FALSE_in_bool_exp705); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    FALSE10_tree = 
                    (Object)adaptor.create(FALSE10)
                    ;
                    adaptor.addChild(root_0, FALSE10_tree);
                    }

                    if ( state.backtracking==0 ) {retval.value = ASTBooleanExpression.FALSE;}

                    }
                    break;
                case 3 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:167:4: NOT atom_exp
                    {
                    root_0 = (Object)adaptor.nil();


                    NOT11=(Token)match(input,NOT,FOLLOW_NOT_in_bool_exp712); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    NOT11_tree = 
                    (Object)adaptor.create(NOT11)
                    ;
                    adaptor.addChild(root_0, NOT11_tree);
                    }

                    pushFollow(FOLLOW_atom_exp_in_bool_exp714);
                    atom_exp12=atom_exp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, atom_exp12.getTree());

                    if ( state.backtracking==0 ) {retval.value = new ASTNotOperator((atom_exp12!=null?atom_exp12.value:null));}

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "bool_exp"


    public static class forall_exp_return extends ParserRuleReturnScope {
        public ASTForallExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "forall_exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:170:1: forall_exp returns [ASTForallExpression value] : FORALL varName= ID IN domName= ID '.' atom_exp ;
    public final CPLParser.forall_exp_return forall_exp() throws RecognitionException {
        CPLParser.forall_exp_return retval = new CPLParser.forall_exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token varName=null;
        Token domName=null;
        Token FORALL13=null;
        Token IN14=null;
        Token char_literal15=null;
        CPLParser.atom_exp_return atom_exp16 =null;


        Object varName_tree=null;
        Object domName_tree=null;
        Object FORALL13_tree=null;
        Object IN14_tree=null;
        Object char_literal15_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:171:2: ( FORALL varName= ID IN domName= ID '.' atom_exp )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:172:2: FORALL varName= ID IN domName= ID '.' atom_exp
            {
            root_0 = (Object)adaptor.nil();


            FORALL13=(Token)match(input,FORALL,FOLLOW_FORALL_in_forall_exp733); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            FORALL13_tree = 
            (Object)adaptor.create(FORALL13)
            ;
            adaptor.addChild(root_0, FORALL13_tree);
            }

            varName=(Token)match(input,ID,FOLLOW_ID_in_forall_exp737); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            varName_tree = 
            (Object)adaptor.create(varName)
            ;
            adaptor.addChild(root_0, varName_tree);
            }

            IN14=(Token)match(input,IN,FOLLOW_IN_in_forall_exp739); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IN14_tree = 
            (Object)adaptor.create(IN14)
            ;
            adaptor.addChild(root_0, IN14_tree);
            }

            domName=(Token)match(input,ID,FOLLOW_ID_in_forall_exp743); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            domName_tree = 
            (Object)adaptor.create(domName)
            ;
            adaptor.addChild(root_0, domName_tree);
            }

            char_literal15=(Token)match(input,45,FOLLOW_45_in_forall_exp745); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal15_tree = 
            (Object)adaptor.create(char_literal15)
            ;
            adaptor.addChild(root_0, char_literal15_tree);
            }

            pushFollow(FOLLOW_atom_exp_in_forall_exp747);
            atom_exp16=atom_exp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, atom_exp16.getTree());

            if ( state.backtracking==0 ) {retval.value = new ASTForallExpression((varName!=null?varName.getText():null), (domName!=null?domName.getText():null), (atom_exp16!=null?atom_exp16.value:null));}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "forall_exp"


    public static class exist_exp_return extends ParserRuleReturnScope {
        public ASTExistentialExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "exist_exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:176:1: exist_exp returns [ASTExistentialExpression value] : EXIST varName= ID IN domName= ID '.' atom_exp ;
    public final CPLParser.exist_exp_return exist_exp() throws RecognitionException {
        CPLParser.exist_exp_return retval = new CPLParser.exist_exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token varName=null;
        Token domName=null;
        Token EXIST17=null;
        Token IN18=null;
        Token char_literal19=null;
        CPLParser.atom_exp_return atom_exp20 =null;


        Object varName_tree=null;
        Object domName_tree=null;
        Object EXIST17_tree=null;
        Object IN18_tree=null;
        Object char_literal19_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:177:2: ( EXIST varName= ID IN domName= ID '.' atom_exp )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:178:2: EXIST varName= ID IN domName= ID '.' atom_exp
            {
            root_0 = (Object)adaptor.nil();


            EXIST17=(Token)match(input,EXIST,FOLLOW_EXIST_in_exist_exp766); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            EXIST17_tree = 
            (Object)adaptor.create(EXIST17)
            ;
            adaptor.addChild(root_0, EXIST17_tree);
            }

            varName=(Token)match(input,ID,FOLLOW_ID_in_exist_exp770); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            varName_tree = 
            (Object)adaptor.create(varName)
            ;
            adaptor.addChild(root_0, varName_tree);
            }

            IN18=(Token)match(input,IN,FOLLOW_IN_in_exist_exp772); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            IN18_tree = 
            (Object)adaptor.create(IN18)
            ;
            adaptor.addChild(root_0, IN18_tree);
            }

            domName=(Token)match(input,ID,FOLLOW_ID_in_exist_exp776); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            domName_tree = 
            (Object)adaptor.create(domName)
            ;
            adaptor.addChild(root_0, domName_tree);
            }

            char_literal19=(Token)match(input,45,FOLLOW_45_in_exist_exp778); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal19_tree = 
            (Object)adaptor.create(char_literal19)
            ;
            adaptor.addChild(root_0, char_literal19_tree);
            }

            pushFollow(FOLLOW_atom_exp_in_exist_exp780);
            atom_exp20=atom_exp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, atom_exp20.getTree());

            if ( state.backtracking==0 ) {retval.value = new ASTExistentialExpression((varName!=null?varName.getText():null), (domName!=null?domName.getText():null), (atom_exp20!=null?atom_exp20.value:null));}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "exist_exp"


    public static class atom_exp_return extends ParserRuleReturnScope {
        public AbstractASTExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "atom_exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:195:1: atom_exp returns [AbstractASTExpression value] : ( num_exp | bool_exp | var_exp | enclose_exp | forall_exp | exist_exp ) ;
    public final CPLParser.atom_exp_return atom_exp() throws RecognitionException {
        CPLParser.atom_exp_return retval = new CPLParser.atom_exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        CPLParser.num_exp_return num_exp21 =null;

        CPLParser.bool_exp_return bool_exp22 =null;

        CPLParser.var_exp_return var_exp23 =null;

        CPLParser.enclose_exp_return enclose_exp24 =null;

        CPLParser.forall_exp_return forall_exp25 =null;

        CPLParser.exist_exp_return exist_exp26 =null;



        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:196:2: ( ( num_exp | bool_exp | var_exp | enclose_exp | forall_exp | exist_exp ) )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:197:2: ( num_exp | bool_exp | var_exp | enclose_exp | forall_exp | exist_exp )
            {
            root_0 = (Object)adaptor.nil();


            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:197:2: ( num_exp | bool_exp | var_exp | enclose_exp | forall_exp | exist_exp )
            int alt3=6;
            switch ( input.LA(1) ) {
            case INT:
                {
                alt3=1;
                }
                break;
            case FALSE:
            case NOT:
            case TRUE:
                {
                alt3=2;
                }
                break;
            case ID:
                {
                alt3=3;
                }
                break;
            case 42:
                {
                alt3=4;
                }
                break;
            case FORALL:
                {
                alt3=5;
                }
                break;
            case EXIST:
                {
                alt3=6;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;

            }

            switch (alt3) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:198:3: num_exp
                    {
                    pushFollow(FOLLOW_num_exp_in_atom_exp809);
                    num_exp21=num_exp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, num_exp21.getTree());

                    if ( state.backtracking==0 ) {retval.value = (num_exp21!=null?num_exp21.value:null);}

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:199:5: bool_exp
                    {
                    pushFollow(FOLLOW_bool_exp_in_atom_exp817);
                    bool_exp22=bool_exp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, bool_exp22.getTree());

                    if ( state.backtracking==0 ) {retval.value = (bool_exp22!=null?bool_exp22.value:null);}

                    }
                    break;
                case 3 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:200:5: var_exp
                    {
                    pushFollow(FOLLOW_var_exp_in_atom_exp825);
                    var_exp23=var_exp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, var_exp23.getTree());

                    if ( state.backtracking==0 ) {retval.value = (var_exp23!=null?var_exp23.value:null);}

                    }
                    break;
                case 4 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:201:5: enclose_exp
                    {
                    pushFollow(FOLLOW_enclose_exp_in_atom_exp833);
                    enclose_exp24=enclose_exp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, enclose_exp24.getTree());

                    if ( state.backtracking==0 ) {retval.value = (enclose_exp24!=null?enclose_exp24.value:null);}

                    }
                    break;
                case 5 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:202:5: forall_exp
                    {
                    pushFollow(FOLLOW_forall_exp_in_atom_exp841);
                    forall_exp25=forall_exp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, forall_exp25.getTree());

                    if ( state.backtracking==0 ) {retval.value = (forall_exp25!=null?forall_exp25.value:null);}

                    }
                    break;
                case 6 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:203:5: exist_exp
                    {
                    pushFollow(FOLLOW_exist_exp_in_atom_exp849);
                    exist_exp26=exist_exp();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, exist_exp26.getTree());

                    if ( state.backtracking==0 ) {retval.value = (exist_exp26!=null?exist_exp26.value:null);}

                    }
                    break;

            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "atom_exp"


    public static class exp_return extends ParserRuleReturnScope {
        public AbstractASTExpression value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "exp"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:213:1: exp returns [AbstractASTExpression value] : left= atom_exp ( operator right= atom_exp )* ;
    public final CPLParser.exp_return exp() throws RecognitionException {
        CPLParser.exp_return retval = new CPLParser.exp_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        CPLParser.atom_exp_return left =null;

        CPLParser.atom_exp_return right =null;

        CPLParser.operator_return operator27 =null;




        	ArrayList<AbstractASTExpression> expressions = new ArrayList<AbstractASTExpression>();
        	ArrayList<Token> operators = new ArrayList<Token>();

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:219:2: (left= atom_exp ( operator right= atom_exp )* )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:220:2: left= atom_exp ( operator right= atom_exp )*
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_atom_exp_in_exp886);
            left=atom_exp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, left.getTree());

            if ( state.backtracking==0 ) {expressions.add((left!=null?left.value:null));}

            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:222:2: ( operator right= atom_exp )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==AND||LA4_0==DIVIDE||LA4_0==EQUALITY||(LA4_0 >= GE && LA4_0 <= GT)||LA4_0==IMPLY||(LA4_0 >= LE && LA4_0 <= NON_EQUALITY)||(LA4_0 >= OR && LA4_0 <= POWER)||LA4_0==TIMES||LA4_0==XOR) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:222:3: operator right= atom_exp
            	    {
            	    pushFollow(FOLLOW_operator_in_exp895);
            	    operator27=operator();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, operator27.getTree());

            	    pushFollow(FOLLOW_atom_exp_in_exp899);
            	    right=atom_exp();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, right.getTree());

            	    if ( state.backtracking==0 ) {operators.add((operator27!=null?((Token)operator27.start):null)); expressions.add((right!=null?right.value:null));}

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);


            if ( state.backtracking==0 ) {retval.value = createPrecedenceTree(expressions, operators);}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "exp"


    public static class var_type_return extends ParserRuleReturnScope {
        public String value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "var_type"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:226:1: var_type returns [String value] : ( INT_TYPE | BOOL_TYPE );
    public final CPLParser.var_type_return var_type() throws RecognitionException {
        CPLParser.var_type_return retval = new CPLParser.var_type_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token INT_TYPE28=null;
        Token BOOL_TYPE29=null;

        Object INT_TYPE28_tree=null;
        Object BOOL_TYPE29_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:227:2: ( INT_TYPE | BOOL_TYPE )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==INT_TYPE) ) {
                alt5=1;
            }
            else if ( (LA5_0==BOOL_TYPE) ) {
                alt5=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;

            }
            switch (alt5) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:227:4: INT_TYPE
                    {
                    root_0 = (Object)adaptor.nil();


                    INT_TYPE28=(Token)match(input,INT_TYPE,FOLLOW_INT_TYPE_in_var_type921); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    INT_TYPE28_tree = 
                    (Object)adaptor.create(INT_TYPE28)
                    ;
                    adaptor.addChild(root_0, INT_TYPE28_tree);
                    }

                    if ( state.backtracking==0 ) {retval.value = (INT_TYPE28!=null?INT_TYPE28.getText():null);}

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:227:42: BOOL_TYPE
                    {
                    root_0 = (Object)adaptor.nil();


                    BOOL_TYPE29=(Token)match(input,BOOL_TYPE,FOLLOW_BOOL_TYPE_in_var_type927); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    BOOL_TYPE29_tree = 
                    (Object)adaptor.create(BOOL_TYPE29)
                    ;
                    adaptor.addChild(root_0, BOOL_TYPE29_tree);
                    }

                    if ( state.backtracking==0 ) {retval.value = (BOOL_TYPE29!=null?BOOL_TYPE29.getText():null);}

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "var_type"


    public static class domain_element_return extends ParserRuleReturnScope {
        public String value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "domain_element"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:232:1: domain_element returns [String value] : ( TRUE | FALSE | INT | identifier_or_constant );
    public final CPLParser.domain_element_return domain_element() throws RecognitionException {
        CPLParser.domain_element_return retval = new CPLParser.domain_element_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token TRUE30=null;
        Token FALSE31=null;
        Token INT32=null;
        CPLParser.identifier_or_constant_return identifier_or_constant33 =null;


        Object TRUE30_tree=null;
        Object FALSE31_tree=null;
        Object INT32_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:233:2: ( TRUE | FALSE | INT | identifier_or_constant )
            int alt6=4;
            switch ( input.LA(1) ) {
            case TRUE:
                {
                alt6=1;
                }
                break;
            case FALSE:
                {
                alt6=2;
                }
                break;
            case INT:
                {
                int LA6_3 = input.LA(2);

                if ( (synpred13_CPL()) ) {
                    alt6=3;
                }
                else if ( (true) ) {
                    alt6=4;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return retval;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 6, 3, input);

                    throw nvae;

                }
                }
                break;
            case ID:
                {
                alt6=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;

            }

            switch (alt6) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:233:4: TRUE
                    {
                    root_0 = (Object)adaptor.nil();


                    TRUE30=(Token)match(input,TRUE,FOLLOW_TRUE_in_domain_element947); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    TRUE30_tree = 
                    (Object)adaptor.create(TRUE30)
                    ;
                    adaptor.addChild(root_0, TRUE30_tree);
                    }

                    if ( state.backtracking==0 ) {retval.value = (TRUE30!=null?TRUE30.getText():null);}

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:234:4: FALSE
                    {
                    root_0 = (Object)adaptor.nil();


                    FALSE31=(Token)match(input,FALSE,FOLLOW_FALSE_in_domain_element954); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    FALSE31_tree = 
                    (Object)adaptor.create(FALSE31)
                    ;
                    adaptor.addChild(root_0, FALSE31_tree);
                    }

                    if ( state.backtracking==0 ) {retval.value = (FALSE31!=null?FALSE31.getText():null);}

                    }
                    break;
                case 3 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:235:4: INT
                    {
                    root_0 = (Object)adaptor.nil();


                    INT32=(Token)match(input,INT,FOLLOW_INT_in_domain_element961); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    INT32_tree = 
                    (Object)adaptor.create(INT32)
                    ;
                    adaptor.addChild(root_0, INT32_tree);
                    }

                    if ( state.backtracking==0 ) {retval.value = (INT32!=null?INT32.getText():null);}

                    }
                    break;
                case 4 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:236:4: identifier_or_constant
                    {
                    root_0 = (Object)adaptor.nil();


                    pushFollow(FOLLOW_identifier_or_constant_in_domain_element968);
                    identifier_or_constant33=identifier_or_constant();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, identifier_or_constant33.getTree());

                    if ( state.backtracking==0 ) {retval.value = (identifier_or_constant33!=null?identifier_or_constant33.text:null);}

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "domain_element"


    public static class var_def_return extends ParserRuleReturnScope {
        public AbstractASTVarDefinition value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "var_def"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:239:1: var_def[List<ASTDomDefinition> doms] returns [AbstractASTVarDefinition value] : ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )? var_type name= ID ( '[' dims= identifier_or_constant ']' )* ( '{' ( domain_element )+ '}' | DOM domName= ID ) ;
    public final CPLParser.var_def_return var_def(List<ASTDomDefinition> doms) throws RecognitionException {
        CPLParser.var_def_return retval = new CPLParser.var_def_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token annotationName=null;
        Token name=null;
        Token domName=null;
        Token char_literal34=null;
        Token char_literal35=null;
        Token char_literal36=null;
        Token STRING37=null;
        Token char_literal38=null;
        Token char_literal39=null;
        Token char_literal41=null;
        Token char_literal42=null;
        Token char_literal43=null;
        Token char_literal45=null;
        Token DOM46=null;
        CPLParser.identifier_or_constant_return dims =null;

        CPLParser.var_type_return var_type40 =null;

        CPLParser.domain_element_return domain_element44 =null;


        Object annotationName_tree=null;
        Object name_tree=null;
        Object domName_tree=null;
        Object char_literal34_tree=null;
        Object char_literal35_tree=null;
        Object char_literal36_tree=null;
        Object STRING37_tree=null;
        Object char_literal38_tree=null;
        Object char_literal39_tree=null;
        Object char_literal41_tree=null;
        Object char_literal42_tree=null;
        Object char_literal43_tree=null;
        Object char_literal45_tree=null;
        Object DOM46_tree=null;


        	ArrayList<String> domainList = new ArrayList<String>();
        	ArrayList<Integer> dimensions = new ArrayList<Integer>();
        	Map<String, String> annotations = new HashMap<String, String>();

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:246:2: ( ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )? var_type name= ID ( '[' dims= identifier_or_constant ']' )* ( '{' ( domain_element )+ '}' | DOM domName= ID ) )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:247:2: ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )? var_type name= ID ( '[' dims= identifier_or_constant ']' )* ( '{' ( domain_element )+ '}' | DOM domName= ID )
            {
            root_0 = (Object)adaptor.nil();


            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:247:2: ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==50) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:247:3: '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')'
                    {
                    char_literal34=(Token)match(input,50,FOLLOW_50_in_var_def994); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal34_tree = 
                    (Object)adaptor.create(char_literal34)
                    ;
                    adaptor.addChild(root_0, char_literal34_tree);
                    }

                    char_literal35=(Token)match(input,42,FOLLOW_42_in_var_def996); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal35_tree = 
                    (Object)adaptor.create(char_literal35)
                    ;
                    adaptor.addChild(root_0, char_literal35_tree);
                    }

                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:247:11: (annotationName= ID '=' STRING ( ',' )? )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==ID) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:247:12: annotationName= ID '=' STRING ( ',' )?
                    	    {
                    	    annotationName=(Token)match(input,ID,FOLLOW_ID_in_var_def1001); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    annotationName_tree = 
                    	    (Object)adaptor.create(annotationName)
                    	    ;
                    	    adaptor.addChild(root_0, annotationName_tree);
                    	    }

                    	    char_literal36=(Token)match(input,49,FOLLOW_49_in_var_def1003); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    char_literal36_tree = 
                    	    (Object)adaptor.create(char_literal36)
                    	    ;
                    	    adaptor.addChild(root_0, char_literal36_tree);
                    	    }

                    	    STRING37=(Token)match(input,STRING,FOLLOW_STRING_in_var_def1005); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    STRING37_tree = 
                    	    (Object)adaptor.create(STRING37)
                    	    ;
                    	    adaptor.addChild(root_0, STRING37_tree);
                    	    }

                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:247:41: ( ',' )?
                    	    int alt7=2;
                    	    int LA7_0 = input.LA(1);

                    	    if ( (LA7_0==44) ) {
                    	        alt7=1;
                    	    }
                    	    switch (alt7) {
                    	        case 1 :
                    	            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:247:41: ','
                    	            {
                    	            char_literal38=(Token)match(input,44,FOLLOW_44_in_var_def1007); if (state.failed) return retval;
                    	            if ( state.backtracking==0 ) {
                    	            char_literal38_tree = 
                    	            (Object)adaptor.create(char_literal38)
                    	            ;
                    	            adaptor.addChild(root_0, char_literal38_tree);
                    	            }

                    	            }
                    	            break;

                    	    }


                    	    if ( state.backtracking==0 ) {annotations.put((annotationName!=null?annotationName.getText():null), (STRING37!=null?STRING37.getText():null).substring(1, (STRING37!=null?STRING37.getText():null).length() - 1));}

                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);


                    char_literal39=(Token)match(input,43,FOLLOW_43_in_var_def1015); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal39_tree = 
                    (Object)adaptor.create(char_literal39)
                    ;
                    adaptor.addChild(root_0, char_literal39_tree);
                    }

                    }
                    break;

            }


            pushFollow(FOLLOW_var_type_in_var_def1020);
            var_type40=var_type();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, var_type40.getTree());

            name=(Token)match(input,ID,FOLLOW_ID_in_var_def1024); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            name_tree = 
            (Object)adaptor.create(name)
            ;
            adaptor.addChild(root_0, name_tree);
            }

            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:249:2: ( '[' dims= identifier_or_constant ']' )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==51) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:249:3: '[' dims= identifier_or_constant ']'
            	    {
            	    char_literal41=(Token)match(input,51,FOLLOW_51_in_var_def1029); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal41_tree = 
            	    (Object)adaptor.create(char_literal41)
            	    ;
            	    adaptor.addChild(root_0, char_literal41_tree);
            	    }

            	    pushFollow(FOLLOW_identifier_or_constant_in_var_def1033);
            	    dims=identifier_or_constant();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, dims.getTree());

            	    char_literal42=(Token)match(input,52,FOLLOW_52_in_var_def1035); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal42_tree = 
            	    (Object)adaptor.create(char_literal42)
            	    ;
            	    adaptor.addChild(root_0, char_literal42_tree);
            	    }

            	    if ( state.backtracking==0 ) {dimensions.add(Integer.parseInt((dims!=null?dims.text:null)));}

            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);


            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:250:2: ( '{' ( domain_element )+ '}' | DOM domName= ID )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==53) ) {
                alt12=1;
            }
            else if ( (LA12_0==DOM) ) {
                alt12=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;

            }
            switch (alt12) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:251:3: '{' ( domain_element )+ '}'
                    {
                    char_literal43=(Token)match(input,53,FOLLOW_53_in_var_def1047); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal43_tree = 
                    (Object)adaptor.create(char_literal43)
                    ;
                    adaptor.addChild(root_0, char_literal43_tree);
                    }

                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:251:7: ( domain_element )+
                    int cnt11=0;
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==FALSE||LA11_0==ID||LA11_0==INT||LA11_0==TRUE) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:251:8: domain_element
                    	    {
                    	    pushFollow(FOLLOW_domain_element_in_var_def1050);
                    	    domain_element44=domain_element();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, domain_element44.getTree());

                    	    if ( state.backtracking==0 ) {domainList.add((domain_element44!=null?domain_element44.value:null));}

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt11 >= 1 ) break loop11;
                    	    if (state.backtracking>0) {state.failed=true; return retval;}
                                EarlyExitException eee =
                                    new EarlyExitException(11, input);
                                throw eee;
                        }
                        cnt11++;
                    } while (true);


                    char_literal45=(Token)match(input,54,FOLLOW_54_in_var_def1056); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal45_tree = 
                    (Object)adaptor.create(char_literal45)
                    ;
                    adaptor.addChild(root_0, char_literal45_tree);
                    }

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:253:3: DOM domName= ID
                    {
                    DOM46=(Token)match(input,DOM,FOLLOW_DOM_in_var_def1064); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    DOM46_tree = 
                    (Object)adaptor.create(DOM46)
                    ;
                    adaptor.addChild(root_0, DOM46_tree);
                    }

                    domName=(Token)match(input,ID,FOLLOW_ID_in_var_def1068); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    domName_tree = 
                    (Object)adaptor.create(domName)
                    ;
                    adaptor.addChild(root_0, domName_tree);
                    }

                    if ( state.backtracking==0 ) {
                    		for (ASTDomDefinition dom: doms) {
                    		    if (dom.getName().equals((domName!=null?domName.getText():null))) {
                    		        domainList.addAll(dom.getDomain());	
                    		        break;
                    		    }
                    		}
                    		}

                    }
                    break;

            }


            if ( state.backtracking==0 ) {
            		if (dimensions.isEmpty())
            			retval.value = new ASTScalarVarDefinition((var_type40!=null?var_type40.value:null), (name!=null?name.getText():null), domainList, annotations);
            		else
            			retval.value = new ASTArrayDefinition((var_type40!=null?var_type40.value:null), (name!=null?name.getText():null), domainList, dimensions, annotations);
            	}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "var_def"


    public static class dom_def_return extends ParserRuleReturnScope {
        public ASTDomDefinition value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "dom_def"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:274:1: dom_def returns [ASTDomDefinition value] : var_type ID '{' first= domain_element ( '..' right= domain_element | (rest= domain_element )* ) '}' ;
    public final CPLParser.dom_def_return dom_def() throws RecognitionException {
        CPLParser.dom_def_return retval = new CPLParser.dom_def_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token ID48=null;
        Token char_literal49=null;
        Token string_literal50=null;
        Token char_literal51=null;
        CPLParser.domain_element_return first =null;

        CPLParser.domain_element_return right =null;

        CPLParser.domain_element_return rest =null;

        CPLParser.var_type_return var_type47 =null;


        Object ID48_tree=null;
        Object char_literal49_tree=null;
        Object string_literal50_tree=null;
        Object char_literal51_tree=null;


        	ArrayList<String> domainList = new ArrayList<String>();

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:279:2: ( var_type ID '{' first= domain_element ( '..' right= domain_element | (rest= domain_element )* ) '}' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:280:3: var_type ID '{' first= domain_element ( '..' right= domain_element | (rest= domain_element )* ) '}'
            {
            root_0 = (Object)adaptor.nil();


            pushFollow(FOLLOW_var_type_in_dom_def1105);
            var_type47=var_type();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, var_type47.getTree());

            ID48=(Token)match(input,ID,FOLLOW_ID_in_dom_def1107); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            ID48_tree = 
            (Object)adaptor.create(ID48)
            ;
            adaptor.addChild(root_0, ID48_tree);
            }

            char_literal49=(Token)match(input,53,FOLLOW_53_in_dom_def1112); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal49_tree = 
            (Object)adaptor.create(char_literal49)
            ;
            adaptor.addChild(root_0, char_literal49_tree);
            }

            pushFollow(FOLLOW_domain_element_in_dom_def1121);
            first=domain_element();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, first.getTree());

            if ( state.backtracking==0 ) {domainList.add((first!=null?first.value:null));}

            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:283:5: ( '..' right= domain_element | (rest= domain_element )* )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==46) ) {
                alt14=1;
            }
            else if ( (LA14_0==FALSE||LA14_0==ID||LA14_0==INT||LA14_0==TRUE||LA14_0==54) ) {
                alt14=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;

            }
            switch (alt14) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:284:6: '..' right= domain_element
                    {
                    string_literal50=(Token)match(input,46,FOLLOW_46_in_dom_def1136); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    string_literal50_tree = 
                    (Object)adaptor.create(string_literal50)
                    ;
                    adaptor.addChild(root_0, string_literal50_tree);
                    }

                    pushFollow(FOLLOW_domain_element_in_dom_def1140);
                    right=domain_element();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, right.getTree());

                    if ( state.backtracking==0 ) {
                     	 			final int rightBound = Integer.parseInt((right!=null?right.value:null));
                     	 			final int leftBound = Integer.parseInt((first!=null?first.value:null));
                     	 			for (int i = leftBound + 1; i <= rightBound; ++i) {
                     	 				domainList.add(String.valueOf(i));
                     	 			}
                     	 		}

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:292:8: (rest= domain_element )*
                    {
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:292:8: (rest= domain_element )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==FALSE||LA13_0==ID||LA13_0==INT||LA13_0==TRUE) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:292:9: rest= domain_element
                    	    {
                    	    pushFollow(FOLLOW_domain_element_in_dom_def1159);
                    	    rest=domain_element();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, rest.getTree());

                    	    if ( state.backtracking==0 ) {domainList.add((rest!=null?rest.value:null));}

                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);


                    }
                    break;

            }


            char_literal51=(Token)match(input,54,FOLLOW_54_in_dom_def1183); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal51_tree = 
            (Object)adaptor.create(char_literal51)
            ;
            adaptor.addChild(root_0, char_literal51_tree);
            }

            if ( state.backtracking==0 ) {retval.value = new ASTDomDefinition((var_type47!=null?var_type47.value:null), (ID48!=null?ID48.getText():null), domainList);}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "dom_def"


    public static class constraint_return extends ParserRuleReturnScope {
        public ASTConstraint value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "constraint"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:300:1: constraint returns [ASTConstraint value] : ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )? exp ;
    public final CPLParser.constraint_return constraint() throws RecognitionException {
        CPLParser.constraint_return retval = new CPLParser.constraint_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token annotationName=null;
        Token char_literal52=null;
        Token char_literal53=null;
        Token char_literal54=null;
        Token STRING55=null;
        Token char_literal56=null;
        Token char_literal57=null;
        CPLParser.exp_return exp58 =null;


        Object annotationName_tree=null;
        Object char_literal52_tree=null;
        Object char_literal53_tree=null;
        Object char_literal54_tree=null;
        Object STRING55_tree=null;
        Object char_literal56_tree=null;
        Object char_literal57_tree=null;


        	Map<String, String> annotations = new HashMap<String, String>();

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:305:2: ( ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )? exp )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:306:2: ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )? exp
            {
            root_0 = (Object)adaptor.nil();


            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:306:2: ( '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')' )?
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==50) ) {
                alt17=1;
            }
            switch (alt17) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:306:3: '@' '(' (annotationName= ID '=' STRING ( ',' )? )* ')'
                    {
                    char_literal52=(Token)match(input,50,FOLLOW_50_in_constraint1208); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal52_tree = 
                    (Object)adaptor.create(char_literal52)
                    ;
                    adaptor.addChild(root_0, char_literal52_tree);
                    }

                    char_literal53=(Token)match(input,42,FOLLOW_42_in_constraint1210); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal53_tree = 
                    (Object)adaptor.create(char_literal53)
                    ;
                    adaptor.addChild(root_0, char_literal53_tree);
                    }

                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:306:11: (annotationName= ID '=' STRING ( ',' )? )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==ID) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:306:12: annotationName= ID '=' STRING ( ',' )?
                    	    {
                    	    annotationName=(Token)match(input,ID,FOLLOW_ID_in_constraint1215); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    annotationName_tree = 
                    	    (Object)adaptor.create(annotationName)
                    	    ;
                    	    adaptor.addChild(root_0, annotationName_tree);
                    	    }

                    	    char_literal54=(Token)match(input,49,FOLLOW_49_in_constraint1217); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    char_literal54_tree = 
                    	    (Object)adaptor.create(char_literal54)
                    	    ;
                    	    adaptor.addChild(root_0, char_literal54_tree);
                    	    }

                    	    STRING55=(Token)match(input,STRING,FOLLOW_STRING_in_constraint1219); if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) {
                    	    STRING55_tree = 
                    	    (Object)adaptor.create(STRING55)
                    	    ;
                    	    adaptor.addChild(root_0, STRING55_tree);
                    	    }

                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:306:41: ( ',' )?
                    	    int alt15=2;
                    	    int LA15_0 = input.LA(1);

                    	    if ( (LA15_0==44) ) {
                    	        alt15=1;
                    	    }
                    	    switch (alt15) {
                    	        case 1 :
                    	            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:306:41: ','
                    	            {
                    	            char_literal56=(Token)match(input,44,FOLLOW_44_in_constraint1221); if (state.failed) return retval;
                    	            if ( state.backtracking==0 ) {
                    	            char_literal56_tree = 
                    	            (Object)adaptor.create(char_literal56)
                    	            ;
                    	            adaptor.addChild(root_0, char_literal56_tree);
                    	            }

                    	            }
                    	            break;

                    	    }


                    	    if ( state.backtracking==0 ) {annotations.put((annotationName!=null?annotationName.getText():null), (STRING55!=null?STRING55.getText():null).substring(1, (STRING55!=null?STRING55.getText():null).length() - 1));}

                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);


                    char_literal57=(Token)match(input,43,FOLLOW_43_in_constraint1229); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal57_tree = 
                    (Object)adaptor.create(char_literal57)
                    ;
                    adaptor.addChild(root_0, char_literal57_tree);
                    }

                    }
                    break;

            }


            pushFollow(FOLLOW_exp_in_constraint1234);
            exp58=exp();

            state._fsp--;
            if (state.failed) return retval;
            if ( state.backtracking==0 ) adaptor.addChild(root_0, exp58.getTree());

            if ( state.backtracking==0 ) {retval.value = new ASTConstraint(annotations, (exp58!=null?exp58.value:null));}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "constraint"


    public static class program_return extends ParserRuleReturnScope {
        public ASTProgram value;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "program"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:311:1: program returns [ASTProgram value] : ( '#define' key= ID (macroValue= ID | INT ) )* ( DOM ':' ( dom_def ( ';' )? )* )? VAR ':' ( var_def[domDefinitions] ( ';' )? )+ CONS ':' ( constraint ';' )+ ;
    public final CPLParser.program_return program() throws RecognitionException {
        CPLParser.program_return retval = new CPLParser.program_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token key=null;
        Token macroValue=null;
        Token string_literal59=null;
        Token INT60=null;
        Token DOM61=null;
        Token char_literal62=null;
        Token char_literal64=null;
        Token VAR65=null;
        Token char_literal66=null;
        Token char_literal68=null;
        Token CONS69=null;
        Token char_literal70=null;
        Token char_literal72=null;
        CPLParser.dom_def_return dom_def63 =null;

        CPLParser.var_def_return var_def67 =null;

        CPLParser.constraint_return constraint71 =null;


        Object key_tree=null;
        Object macroValue_tree=null;
        Object string_literal59_tree=null;
        Object INT60_tree=null;
        Object DOM61_tree=null;
        Object char_literal62_tree=null;
        Object char_literal64_tree=null;
        Object VAR65_tree=null;
        Object char_literal66_tree=null;
        Object char_literal68_tree=null;
        Object CONS69_tree=null;
        Object char_literal70_tree=null;
        Object char_literal72_tree=null;


        	ArrayList<ASTDomDefinition> domDefinitions = new ArrayList<ASTDomDefinition>();
        	ArrayList<AbstractASTVarDefinition> varDefinitions = new ArrayList<AbstractASTVarDefinition>();
        	List<ASTConstraint> constraints = new ArrayList<ASTConstraint>();	
        	
        	String tempValue = null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:320:2: ( ( '#define' key= ID (macroValue= ID | INT ) )* ( DOM ':' ( dom_def ( ';' )? )* )? VAR ':' ( var_def[domDefinitions] ( ';' )? )+ CONS ':' ( constraint ';' )+ )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:322:2: ( '#define' key= ID (macroValue= ID | INT ) )* ( DOM ':' ( dom_def ( ';' )? )* )? VAR ':' ( var_def[domDefinitions] ( ';' )? )+ CONS ':' ( constraint ';' )+
            {
            root_0 = (Object)adaptor.nil();


            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:322:2: ( '#define' key= ID (macroValue= ID | INT ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==41) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:322:3: '#define' key= ID (macroValue= ID | INT )
            	    {
            	    string_literal59=(Token)match(input,41,FOLLOW_41_in_program1260); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    string_literal59_tree = 
            	    (Object)adaptor.create(string_literal59)
            	    ;
            	    adaptor.addChild(root_0, string_literal59_tree);
            	    }

            	    key=(Token)match(input,ID,FOLLOW_ID_in_program1264); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    key_tree = 
            	    (Object)adaptor.create(key)
            	    ;
            	    adaptor.addChild(root_0, key_tree);
            	    }

            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:322:20: (macroValue= ID | INT )
            	    int alt18=2;
            	    int LA18_0 = input.LA(1);

            	    if ( (LA18_0==ID) ) {
            	        alt18=1;
            	    }
            	    else if ( (LA18_0==INT) ) {
            	        alt18=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return retval;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 18, 0, input);

            	        throw nvae;

            	    }
            	    switch (alt18) {
            	        case 1 :
            	            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:322:21: macroValue= ID
            	            {
            	            macroValue=(Token)match(input,ID,FOLLOW_ID_in_program1269); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            macroValue_tree = 
            	            (Object)adaptor.create(macroValue)
            	            ;
            	            adaptor.addChild(root_0, macroValue_tree);
            	            }

            	            if ( state.backtracking==0 ) {tempValue = (macroValue!=null?macroValue.getText():null);}

            	            }
            	            break;
            	        case 2 :
            	            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:322:67: INT
            	            {
            	            INT60=(Token)match(input,INT,FOLLOW_INT_in_program1273); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            INT60_tree = 
            	            (Object)adaptor.create(INT60)
            	            ;
            	            adaptor.addChild(root_0, INT60_tree);
            	            }

            	            if ( state.backtracking==0 ) {tempValue = (INT60!=null?INT60.getText():null);}

            	            }
            	            break;

            	    }


            	    if ( state.backtracking==0 ) {macroMap.put((key!=null?key.getText():null), tempValue);}

            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:323:2: ( DOM ':' ( dom_def ( ';' )? )* )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==DOM) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:323:3: DOM ':' ( dom_def ( ';' )? )*
                    {
                    DOM61=(Token)match(input,DOM,FOLLOW_DOM_in_program1283); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    DOM61_tree = 
                    (Object)adaptor.create(DOM61)
                    ;
                    adaptor.addChild(root_0, DOM61_tree);
                    }

                    char_literal62=(Token)match(input,47,FOLLOW_47_in_program1285); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    char_literal62_tree = 
                    (Object)adaptor.create(char_literal62)
                    ;
                    adaptor.addChild(root_0, char_literal62_tree);
                    }

                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:323:12: ( dom_def ( ';' )? )*
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==BOOL_TYPE||LA21_0==INT_TYPE) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:323:13: dom_def ( ';' )?
                    	    {
                    	    pushFollow(FOLLOW_dom_def_in_program1289);
                    	    dom_def63=dom_def();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) adaptor.addChild(root_0, dom_def63.getTree());

                    	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:323:21: ( ';' )?
                    	    int alt20=2;
                    	    int LA20_0 = input.LA(1);

                    	    if ( (LA20_0==48) ) {
                    	        alt20=1;
                    	    }
                    	    switch (alt20) {
                    	        case 1 :
                    	            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:323:21: ';'
                    	            {
                    	            char_literal64=(Token)match(input,48,FOLLOW_48_in_program1291); if (state.failed) return retval;
                    	            if ( state.backtracking==0 ) {
                    	            char_literal64_tree = 
                    	            (Object)adaptor.create(char_literal64)
                    	            ;
                    	            adaptor.addChild(root_0, char_literal64_tree);
                    	            }

                    	            }
                    	            break;

                    	    }


                    	    if ( state.backtracking==0 ) {domDefinitions.add((dom_def63!=null?dom_def63.value:null));}

                    	    }
                    	    break;

                    	default :
                    	    break loop21;
                        }
                    } while (true);


                    }
                    break;

            }


            VAR65=(Token)match(input,VAR,FOLLOW_VAR_in_program1301); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            VAR65_tree = 
            (Object)adaptor.create(VAR65)
            ;
            adaptor.addChild(root_0, VAR65_tree);
            }

            char_literal66=(Token)match(input,47,FOLLOW_47_in_program1303); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal66_tree = 
            (Object)adaptor.create(char_literal66)
            ;
            adaptor.addChild(root_0, char_literal66_tree);
            }

            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:324:10: ( var_def[domDefinitions] ( ';' )? )+
            int cnt24=0;
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==BOOL_TYPE||LA24_0==INT_TYPE||LA24_0==50) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:324:11: var_def[domDefinitions] ( ';' )?
            	    {
            	    pushFollow(FOLLOW_var_def_in_program1306);
            	    var_def67=var_def(domDefinitions);

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, var_def67.getTree());

            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:324:36: ( ';' )?
            	    int alt23=2;
            	    int LA23_0 = input.LA(1);

            	    if ( (LA23_0==48) ) {
            	        alt23=1;
            	    }
            	    switch (alt23) {
            	        case 1 :
            	            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:324:36: ';'
            	            {
            	            char_literal68=(Token)match(input,48,FOLLOW_48_in_program1310); if (state.failed) return retval;
            	            if ( state.backtracking==0 ) {
            	            char_literal68_tree = 
            	            (Object)adaptor.create(char_literal68)
            	            ;
            	            adaptor.addChild(root_0, char_literal68_tree);
            	            }

            	            }
            	            break;

            	    }


            	    if ( state.backtracking==0 ) {varDefinitions.add((var_def67!=null?var_def67.value:null));}

            	    }
            	    break;

            	default :
            	    if ( cnt24 >= 1 ) break loop24;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(24, input);
                        throw eee;
                }
                cnt24++;
            } while (true);


            CONS69=(Token)match(input,CONS,FOLLOW_CONS_in_program1319); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            CONS69_tree = 
            (Object)adaptor.create(CONS69)
            ;
            adaptor.addChild(root_0, CONS69_tree);
            }

            char_literal70=(Token)match(input,47,FOLLOW_47_in_program1321); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            char_literal70_tree = 
            (Object)adaptor.create(char_literal70)
            ;
            adaptor.addChild(root_0, char_literal70_tree);
            }

            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:326:2: ( constraint ';' )+
            int cnt25=0;
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( ((LA25_0 >= EXIST && LA25_0 <= FORALL)||LA25_0==ID||LA25_0==INT||LA25_0==NOT||LA25_0==TRUE||LA25_0==42||LA25_0==50) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:327:3: constraint ';'
            	    {
            	    pushFollow(FOLLOW_constraint_in_program1329);
            	    constraint71=constraint();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, constraint71.getTree());

            	    char_literal72=(Token)match(input,48,FOLLOW_48_in_program1331); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    char_literal72_tree = 
            	    (Object)adaptor.create(char_literal72)
            	    ;
            	    adaptor.addChild(root_0, char_literal72_tree);
            	    }

            	    if ( state.backtracking==0 ) {constraints.add((constraint71!=null?constraint71.value:null));}

            	    }
            	    break;

            	default :
            	    if ( cnt25 >= 1 ) break loop25;
            	    if (state.backtracking>0) {state.failed=true; return retval;}
                        EarlyExitException eee =
                            new EarlyExitException(25, input);
                        throw eee;
                }
                cnt25++;
            } while (true);


            if ( state.backtracking==0 ) {retval.value = new ASTProgram(domDefinitions, varDefinitions, constraints);}

            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "program"


    public static class operator_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "operator"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:340:1: operator : ( '+' | '-' | '*' | '/' | '%' | '==' | '!=' | '>' | '>=' | '<' | '<=' | '&&' | '||' | '^^' | '^' | '->' );
    public final CPLParser.operator_return operator() throws RecognitionException {
        CPLParser.operator_return retval = new CPLParser.operator_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token set73=null;

        Object set73_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:341:2: ( '+' | '-' | '*' | '/' | '%' | '==' | '!=' | '>' | '>=' | '<' | '<=' | '&&' | '||' | '^^' | '^' | '->' )
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:
            {
            root_0 = (Object)adaptor.nil();


            set73=(Token)input.LT(1);

            if ( input.LA(1)==AND||input.LA(1)==DIVIDE||input.LA(1)==EQUALITY||(input.LA(1) >= GE && input.LA(1) <= GT)||input.LA(1)==IMPLY||(input.LA(1) >= LE && input.LA(1) <= NON_EQUALITY)||(input.LA(1) >= OR && input.LA(1) <= POWER)||input.LA(1)==TIMES||input.LA(1)==XOR ) {
                input.consume();
                if ( state.backtracking==0 ) adaptor.addChild(root_0, 
                (Object)adaptor.create(set73)
                );
                state.errorRecovery=false;
                state.failed=false;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "operator"


    public static class identifier_or_constant_return extends ParserRuleReturnScope {
        public String text;
        Object tree;
        public Object getTree() { return tree; }
    };


    // $ANTLR start "identifier_or_constant"
    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:347:1: identifier_or_constant returns [String text] : ( ID | INT );
    public final CPLParser.identifier_or_constant_return identifier_or_constant() throws RecognitionException {
        CPLParser.identifier_or_constant_return retval = new CPLParser.identifier_or_constant_return();
        retval.start = input.LT(1);


        Object root_0 = null;

        Token ID74=null;
        Token INT75=null;

        Object ID74_tree=null;
        Object INT75_tree=null;

        try {
            // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:348:3: ( ID | INT )
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==ID) ) {
                alt26=1;
            }
            else if ( (LA26_0==INT) ) {
                alt26=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return retval;}
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;

            }
            switch (alt26) {
                case 1 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:348:5: ID
                    {
                    root_0 = (Object)adaptor.nil();


                    ID74=(Token)match(input,ID,FOLLOW_ID_in_identifier_or_constant1447); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    ID74_tree = 
                    (Object)adaptor.create(ID74)
                    ;
                    adaptor.addChild(root_0, ID74_tree);
                    }

                    if ( state.backtracking==0 ) {
                     		final String key = (ID74!=null?ID74.getText():null);
                     		if (this.macroMap.containsKey(key)) {
                     			retval.text =  this.macroMap.get(key);
                     		} else {
                     			retval.text = key;
                     		}
                     	
                     	}

                    }
                    break;
                case 2 :
                    // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:358:5: INT
                    {
                    root_0 = (Object)adaptor.nil();


                    INT75=(Token)match(input,INT,FOLLOW_INT_in_identifier_or_constant1458); if (state.failed) return retval;
                    if ( state.backtracking==0 ) {
                    INT75_tree = 
                    (Object)adaptor.create(INT75)
                    ;
                    adaptor.addChild(root_0, INT75_tree);
                    }

                    if ( state.backtracking==0 ) {retval.text = (INT75!=null?INT75.getText():null);}

                    }
                    break;

            }
            retval.stop = input.LT(-1);


            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }

        finally {
        	// do for sure before leaving
        }
        return retval;
    }
    // $ANTLR end "identifier_or_constant"

    // $ANTLR start synpred13_CPL
    public final void synpred13_CPL_fragment() throws RecognitionException {
        // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:235:4: ( INT )
        // /home/neo/workspace/constraint-symmetry-solver2/constraint-symmetry-solver/parser/CPL.g:235:4: INT
        {
        match(input,INT,FOLLOW_INT_in_synpred13_CPL961); if (state.failed) return ;

        }

    }
    // $ANTLR end synpred13_CPL

    // Delegated rules

    public final boolean synpred13_CPL() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred13_CPL_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


 

    public static final BitSet FOLLOW_INT_in_num_exp609 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ID_in_var_exp629 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_51_in_var_exp632 = new BitSet(new long[]{0x0000040810247000L});
    public static final BitSet FOLLOW_exp_in_var_exp634 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_52_in_var_exp636 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_42_in_enclose_exp674 = new BitSet(new long[]{0x0000040810247000L});
    public static final BitSet FOLLOW_exp_in_enclose_exp677 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_43_in_enclose_exp679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TRUE_in_bool_exp696 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FALSE_in_bool_exp705 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NOT_in_bool_exp712 = new BitSet(new long[]{0x0000040810247000L});
    public static final BitSet FOLLOW_atom_exp_in_bool_exp714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FORALL_in_forall_exp733 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_forall_exp737 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_IN_in_forall_exp739 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_forall_exp743 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_forall_exp745 = new BitSet(new long[]{0x0000040810247000L});
    public static final BitSet FOLLOW_atom_exp_in_forall_exp747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_EXIST_in_exist_exp766 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_exist_exp770 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_IN_in_exist_exp772 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_exist_exp776 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_45_in_exist_exp778 = new BitSet(new long[]{0x0000040810247000L});
    public static final BitSet FOLLOW_atom_exp_in_exist_exp780 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_num_exp_in_atom_exp809 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_bool_exp_in_atom_exp817 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_exp_in_atom_exp825 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_enclose_exp_in_atom_exp833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_forall_exp_in_atom_exp841 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_exist_exp_in_atom_exp849 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_atom_exp_in_exp886 = new BitSet(new long[]{0x00000105CF898512L});
    public static final BitSet FOLLOW_operator_in_exp895 = new BitSet(new long[]{0x0000040810247000L});
    public static final BitSet FOLLOW_atom_exp_in_exp899 = new BitSet(new long[]{0x00000105CF898512L});
    public static final BitSet FOLLOW_INT_TYPE_in_var_type921 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_BOOL_TYPE_in_var_type927 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_TRUE_in_domain_element947 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FALSE_in_domain_element954 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_domain_element961 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_identifier_or_constant_in_domain_element968 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_var_def994 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_var_def996 = new BitSet(new long[]{0x0000080000040000L});
    public static final BitSet FOLLOW_ID_in_var_def1001 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_49_in_var_def1003 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_STRING_in_var_def1005 = new BitSet(new long[]{0x0000180000040000L});
    public static final BitSet FOLLOW_44_in_var_def1007 = new BitSet(new long[]{0x0000080000040000L});
    public static final BitSet FOLLOW_43_in_var_def1015 = new BitSet(new long[]{0x0000000000400020L});
    public static final BitSet FOLLOW_var_type_in_var_def1020 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_var_def1024 = new BitSet(new long[]{0x0028000000000200L});
    public static final BitSet FOLLOW_51_in_var_def1029 = new BitSet(new long[]{0x0000000000240000L});
    public static final BitSet FOLLOW_identifier_or_constant_in_var_def1033 = new BitSet(new long[]{0x0010000000000000L});
    public static final BitSet FOLLOW_52_in_var_def1035 = new BitSet(new long[]{0x0028000000000200L});
    public static final BitSet FOLLOW_53_in_var_def1047 = new BitSet(new long[]{0x0000000800242000L});
    public static final BitSet FOLLOW_domain_element_in_var_def1050 = new BitSet(new long[]{0x0040000800242000L});
    public static final BitSet FOLLOW_54_in_var_def1056 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DOM_in_var_def1064 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_var_def1068 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_var_type_in_dom_def1105 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_dom_def1107 = new BitSet(new long[]{0x0020000000000000L});
    public static final BitSet FOLLOW_53_in_dom_def1112 = new BitSet(new long[]{0x0000000800242000L});
    public static final BitSet FOLLOW_domain_element_in_dom_def1121 = new BitSet(new long[]{0x0040400800242000L});
    public static final BitSet FOLLOW_46_in_dom_def1136 = new BitSet(new long[]{0x0000000800242000L});
    public static final BitSet FOLLOW_domain_element_in_dom_def1140 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_domain_element_in_dom_def1159 = new BitSet(new long[]{0x0040000800242000L});
    public static final BitSet FOLLOW_54_in_dom_def1183 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_50_in_constraint1208 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_42_in_constraint1210 = new BitSet(new long[]{0x0000080000040000L});
    public static final BitSet FOLLOW_ID_in_constraint1215 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_49_in_constraint1217 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_STRING_in_constraint1219 = new BitSet(new long[]{0x0000180000040000L});
    public static final BitSet FOLLOW_44_in_constraint1221 = new BitSet(new long[]{0x0000080000040000L});
    public static final BitSet FOLLOW_43_in_constraint1229 = new BitSet(new long[]{0x0000040810247000L});
    public static final BitSet FOLLOW_exp_in_constraint1234 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_41_in_program1260 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_ID_in_program1264 = new BitSet(new long[]{0x0000000000240000L});
    public static final BitSet FOLLOW_ID_in_program1269 = new BitSet(new long[]{0x0000022000000200L});
    public static final BitSet FOLLOW_INT_in_program1273 = new BitSet(new long[]{0x0000022000000200L});
    public static final BitSet FOLLOW_DOM_in_program1283 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_program1285 = new BitSet(new long[]{0x0000002000400020L});
    public static final BitSet FOLLOW_dom_def_in_program1289 = new BitSet(new long[]{0x0001002000400020L});
    public static final BitSet FOLLOW_48_in_program1291 = new BitSet(new long[]{0x0000002000400020L});
    public static final BitSet FOLLOW_VAR_in_program1301 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_program1303 = new BitSet(new long[]{0x0004000000400020L});
    public static final BitSet FOLLOW_var_def_in_program1306 = new BitSet(new long[]{0x00050000004000A0L});
    public static final BitSet FOLLOW_48_in_program1310 = new BitSet(new long[]{0x00040000004000A0L});
    public static final BitSet FOLLOW_CONS_in_program1319 = new BitSet(new long[]{0x0000800000000000L});
    public static final BitSet FOLLOW_47_in_program1321 = new BitSet(new long[]{0x0004040810247000L});
    public static final BitSet FOLLOW_constraint_in_program1329 = new BitSet(new long[]{0x0001000000000000L});
    public static final BitSet FOLLOW_48_in_program1331 = new BitSet(new long[]{0x0004040810247002L});
    public static final BitSet FOLLOW_ID_in_identifier_or_constant1447 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_identifier_or_constant1458 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_INT_in_synpred13_CPL961 = new BitSet(new long[]{0x0000000000000002L});

}